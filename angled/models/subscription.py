from django.contrib.gis.db import models
from django.conf import settings
from datetime import timedelta, datetime

MAX_NOTIFICATION_DAYS = timedelta(
    days=getattr(settings, 'MAX_SUBSCRIPTION_TIME', 7))


class Subscription(models.Model):
    class Meta:
        abstract = True

    id = models.AutoField(primary_key=True)
    created_at = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey("auth.User", on_delete=models.CASCADE)
    active = models.BooleanField(default=False)


class ProjectSubscription(Subscription):
    project = models.ForeignKey('angled.Project', on_delete=models.CASCADE)


class CircleSubscription(Subscription):
    radius = models.IntegerField()
    geom = models.PointField(dim=2, srid=31370)


class NotificationManager(models.Manager):
    def get_queryset(self):
        lower_bound = datetime.now() - MAX_NOTIFICATION_DAYS
        return super().get_queryset().filter(created_at__gt=lower_bound)


class Notification(models.Model):
    class Meta:
        abstract = True

    recent_objects = NotificationManager()
    objects = models.Manager()

    id = models.AutoField(primary_key=True)
    created_at = models.DateTimeField(auto_now_add=True)
    mark_view = models.BooleanField(default=False)

    action = models.CharField(max_length=64)
    unit = models.CharField(max_length=64)
    unit_id = models.IntegerField()


class ProjectNotification(Notification):
    subscription = models.ForeignKey(ProjectSubscription,
                                     on_delete=models.CASCADE,
                                     related_name='notifications')


class CircleNotification(Notification):
    subscription = models.ForeignKey(CircleSubscription,
                                     on_delete=models.CASCADE,
                                     related_name='notifications')


def auto_subscribe(user, project):
    subs = ProjectSubscription.objects.filter(user=user, project=project)
    if subs.count() == 0:
        ProjectSubscription.objects.create(user=user,
                                           project=project,
                                           active=True)
