from rest_framework import serializers
from angled.models.subscription import (
    ProjectSubscription,
    CircleSubscription,
    ProjectNotification,
    CircleNotification,
)


class ConstantField(serializers.Field):
    def __init__(self, constant, *args, **kwargs):
        self.constant = constant
        kwargs['read_only'] = True
        super().__init__(*args, **kwargs)

    def get_attribute(self, instance):
        return self.constant

    def to_representation(self, value):
        if value != self.constant:
            self.fail(value)
        return value


class CreateSerializerMixin:
    def create(self, validated_data):
        user = self.context['request'].user
        instance = ProjectSubscription.objects.create(user=user,
                                                      **validated_data)

        return instance


class ProjectSubscriptionSerializer(CreateSerializerMixin,
                                    serializers.ModelSerializer):
    tag = ConstantField('sub/project')

    class Meta:
        model = ProjectSubscription
        fields = ('tag', 'id', 'created_at', 'active', 'project')


class CircleSubscriptionSerializer(CreateSerializerMixin,
                                   serializers.ModelSerializer):
    tag = ConstantField('sub/circle')

    class Meta:
        model = CircleSubscription
        fields = ('tag', 'id', 'created_at', 'active', 'geom', 'radius')


class ProjectNotificationSerializer(serializers.ModelSerializer):
    tag = ConstantField('notif/project')

    class Meta:
        model = ProjectNotification
        fields = ('tag', 'id', 'created_at', 'mark_view', 'subscription',
                  'action', 'unit', 'unit_id')


class CircleNotificationSerializer(serializers.ModelSerializer):
    tag = ConstantField('notif/circle')

    class Meta:
        model = CircleNotification
        fields = ('tag', 'id', 'created_at', 'mark_view', 'subscription',
                  'action', 'unit', 'unit_id')
