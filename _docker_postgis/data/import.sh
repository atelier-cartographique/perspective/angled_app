#!/bin/bash

# params
pg_host="127.0.0.1"
pg_port="5432"
pg_username="postgres"
pg_dbname="postgres"

# Lim_Pentagone_Couronnes_Bxl
shp2pgsql -I -s 31370 -d Lim_Pentagone_Couronnes_Bxl.shp public.ref_pentagone | psql -h $pg_host -p $pg_port -U $pg_username -d $pg_dbname

# UrbAdm_SHP
refs=("MUNICIPALITY" "REGION" "ADDRESS_POINT" "MONITORING_DISTRICT" "STATISTICAL_DISTRICT" "BLOCK" "BUILDING")
for ref in ${refs[@]}
do
    shp2pgsql -I -s 31370 -d "UrbAdm_SHP/shp/UrbAdm_$ref.shp" "public.ref_urbis_$ref" | psql psql -h $pg_host -p $pg_port -U $pg_username -d $pg_dbname
done

# UrbPab_SHP
shp2pgsql -I -s 31370 -d "UrbPab_SHP/shp/URB_P_CAPA.shp" "public.ref_urbis_capa" | psql -h $pg_host -p $pg_port -U $pg_username -d $pg_dbname

