import json

from rest_framework import serializers
from angled.models import profi
from angled.profile.parser import parse_profile


class JSONField(serializers.RelatedField):
    def to_representation(self, value):
        return json.loads(value)


class ProfileTextField(serializers.Field):
    def to_representation(self, value):
        return parse_profile(value)


class ProfileSerializer(serializers.ModelSerializer):
    # text = ProfileTextField(read_only=True)
    layout = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = profi.Profile
        fields = ['id', 'layout', 'name']

    def get_layout(self, instance):
        return parse_profile(instance.text)