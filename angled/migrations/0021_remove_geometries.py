# Generated by Django 2.1.7 on 2019-06-12 14:32

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('angled', '0020_auto_20190612_1621'),
    ]

    operations = [
 
        migrations.DeleteModel(
            name='info_unit_line',
        ),
        migrations.DeleteModel(
            name='info_unit_point',
        ),
        migrations.DeleteModel(
            name='info_unit_polygon',
        ),
 
        migrations.DeleteModel(
            name='multi_line',
        ),
        migrations.DeleteModel(
            name='multi_point',
        ),
        migrations.DeleteModel(
            name='multi_polygon',
        ),
      
        migrations.DeleteModel(
            name='permission_info_unit_line',
        ),
        migrations.DeleteModel(
            name='permission_info_unit_point',
        ),
        migrations.DeleteModel(
            name='permission_info_unit_polygon',
        ),
        
        migrations.DeleteModel(
            name='Tagged_line',
        ),
        migrations.DeleteModel(
            name='Tagged_point',
        ),
        migrations.DeleteModel(
            name='Tagged_polygon',
        ),
      
    ]
