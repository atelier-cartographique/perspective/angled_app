from rest_framework import serializers
from angled.models import ref
from angled.serializers._utils import TimestampField
from lingua.serializers import LinguaRecordSerializer
from angled.serializers.fields import JSONField


class ContactSerializer(serializers.ModelSerializer):
    """
    Serializer for Contact class
    """
    name = serializers.CharField(min_length=2, max_length=256)
    info = serializers.CharField(allow_blank=True)

    class Meta:
        model = ref.Contact
        fields = (
            'id',
            'name',
            'info',
        )


class SiteSerializer(serializers.ModelSerializer):
    """
    Serializer for Site class
    """
    name = LinguaRecordSerializer()

    class Meta:
        model = ref.Site
        fields = (
            'id',
            'name',
        )


# class NovaSerializer(serializers.ModelSerializer):
#     """
#     Serializer for Nova class

#     Those fields are read-only:

#     - time_of_fetch ;
#     - data ;
#     """
#     ref = serializers.CharField(min_length=2, max_length=24)
#     time_of_fetch = TimestampField()
#     data = JSONField()

#     class Meta:
#         model = ref.Nova
#         fields = (
#             'ref',
#             'time_of_fetch',
#             'data',
#         )
#         read_only_fields = (
#             'time_of_fetch',
#             'data',
#         )


class FundingOrgSerializer(serializers.ModelSerializer):
    """
    Serializer for FundiingOrg class
    """
    name = serializers.CharField(min_length=2, max_length=256)

    class Meta:
        model = ref.FundingOrg
        fields = (
            'id',
            'name',
        )


class LocalitySerializer(serializers.ModelSerializer):
    """
    Serializer for locality

    All fields are read-only
    """
    city = LinguaRecordSerializer()

    class Meta:
        model = ref.Locality
        fields = (
            'id',
            'city',
            'postcode',
            'ins',
        )
        read_only_fields = (
            'id,'
            'city',
            'postcode',
            'ins',
        )


class TeamMemberSerializer(serializers.ModelSerializer):
    """
    Serializer for TeamMember
    """
    class Meta:
        model = ref.TeamMember
        fields = (
            'id',
            'role',
            'member',
        )


class TeamSerializer(serializers.ModelSerializer):
    """
    Serializer for team
    """
    #form = serializers.PrimaryKeyRelatedField(queryset=ref.Term.objects.all())
    members = TeamMemberSerializer(many=True, source='teammember_set')

    class Meta:
        model = ref.Team
        fields = ('id', 'members')

    def create(self, validated_data):
        members_data = validated_data.pop('teammember_set')
        team = ref.Team.objects.create(**validated_data)

        for m in members_data:
            team.teammember_set.create(**m)

        return team

    def update(self, team, validated_data):
        """
        Update team and his members.

        Note that the id of the team members are not kept: the
        teammembers are removed and re-recreated.
        """
        members_data = validated_data.pop('teammember_set')

        # team.form = validated_data.get('form', team.form)
        # team.contact = validated_data.get('contact', team.contact)

        # Remove previous members
        for member in team.teammember_set.all():
            member.delete()

        for m in members_data:
            team.teammember_set.create(**m)

        return team


class TagSerializer(serializers.ModelSerializer):
    label = LinguaRecordSerializer()

    class Meta:
        model = ref.Tag
        fields = ['id', 'label']


class SchoolSiteSerializer(serializers.ModelSerializer):
    """
    Serializer for SchoolSite class
    """
    class Meta:
        model = ref.SchoolSite
        fields = '__all__'
