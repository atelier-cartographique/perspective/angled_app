from datetime import datetime
from urllib.parse import urlencode

from django.shortcuts import get_object_or_404
from django.urls import reverse
from django.utils.text import slugify
from django.contrib.auth.models import User

from basic_wfs.xml import (
    create,
    create_tree,
    append,
    tree_to_string,
    rid_to_typename_ns,
    strip_ns,
)

from angled.ui import find_field
from angled.models.dyn import Project
from angled.models.ref import Term, Team, FundingOrg, Contact
from angled.models.query import Query
from angled.serializers.query import QuerySerializer
from angled.serializers.projects import ProjectSerializer
from angled.query import (
    field_type,
    deser_selects,
    deser_statements,
    get_path,
    get_select_info,
    parse_input,
    parse_select,
)


def ensure_list(x):
    if isinstance(
        x,
        (
            list,
            tuple,
        ),
    ):
        return x
    return [x]


def encode_point(p):
    return "{} {}".format(p[0], p[1])


def encode_line(l):
    return " ".join(list(map(encode_point, l)))


def ns_tag(ns, tag):
    norm_tag = slugify(tag)
    return "{}:{}".format(ns, norm_tag)


def encode_geometry(feature, geom, ns):
    gt = geom["type"]
    coordinates = geom["coordinates"]
    geom = append(feature, ns_tag(ns, "geom"))
    if "MultiPolygon" == gt:
        root = append(geom, "gml:MultiPolygon", dict(srsName="EPSG:31370"))
        for polygon in coordinates:
            member = append(root, "gml:polygonMember")
            poly = append(member, "gml:Polygon")
            outer = append(poly, "gml:outerBoundaryIs")
            ring = append(outer, "gml:LinearRing")
            coords = append(ring, "gml:posList")
            coords.text = encode_line(polygon[0])

    elif "MultiPoint" == gt:
        root = append(geom, "gml:MultiPoint", dict(srsName="EPSG:31370"))
        for point in coordinates:
            member = append(root, "gml:pointMember")
            pt = append(member, "gml:Point")
            pos = append(pt, "gml:pos")
            pos.text = encode_point(point)

    else:
        raise NotImplementedError(
            "support ony polygons and points for now, not {}".format(gt)
        )


def multi_output(select):
    return select.aggregate is not None and select.aggregate.name == "concat"


def make_serializer(fn):
    def inner(feature, ns, select, val, lang):
        tag = ns_tag(ns, select.name)
        list_tag = "{}-list".format(tag)
        if multi_output(select):
            element = append(feature, list_tag)
            for val in ensure_list(val):
                append(element, tag).text = fn(val, select, lang)
        else:
            append(feature, tag).text = fn(val, select, lang)

    return inner


def get_fk(pk, select, lang):
    if pk is None:
        return ""
    field = find_field(select.unit, select.field[0])
    model = field[2]
    try:
        if model == "Contact":
            return Contact.objects.get(pk=pk).name
        elif model == "FundingOrg":
            return FundingOrg.objects.get(pk=pk).name
        elif model == "auth.User":
            u = User.objects.get(pk=pk)
            display_name = u.get_full_name()
            if len(display_name) > 0:
                return display_name
            else:
                return u.username
        elif model == "Team":
            t = Team.objects.get(pk=pk)
            return "; ".join([m.name for m in t.teammember_set.all()])

    except Exception:
        pass

    return "{}({})".format(model, pk)


def get_term(pk, select, lang):
    try:
        return getattr(Term.objects.get(pk=pk).name, lang)
    except Exception:
        """"""


def format_date(timestamp, select, lang):
    try:
        date = datetime.fromtimestamp(timestamp / 1000).date()
        return date.isoformat()
    except Exception:
        return ""


ser_fk = make_serializer(get_fk)
ser_term = make_serializer(get_term)
ser_string = make_serializer(lambda a, b, c: str(a))
ser_date = make_serializer(format_date)


SERIALIZERS = {
    "fk": ser_fk,
    "term": ser_term,
    "label": ser_string,
    "text": ser_string,
    "raw_text": ser_string,
    "number": ser_string,
    "decimal": ser_string,
    "month": ser_string,
    "year": ser_string,
    "date": ser_date,
    "boolean": ser_string,
    "varchar": ser_string,
}


def encode_query(root, data, request, ns, typename, geometry_unit, lang):
    geom_fieldname = "geom"
    wheres = deser_statements(data.get("statements", {}).get("where", []))
    selects = deser_selects(data.get("statements", {}).get("select", []))

    parsed = parse_input(wheres)
    if parsed is None:
        return

    qs = Project.objects.filter(parsed).distinct()
    pids = [p.id for p in qs]
    select_info = get_select_info(list(filter(lambda s: s.name != "geom", selects)))

    if len(pids) > 0:
        pdata = Project.full_loader.get_all_as_dict(pids)
        projects = ProjectSerializer(pdata, many=True, context=dict(request=request))

        for project in projects.data:
            try:
                geom_data = project.get(geometry_unit).get(geom_fieldname)
            except Exception:
                continue

            feature_id = project.get("id")
            feature_member = append(root, "gml:featureMember")
            feature = append(
                feature_member,
                typename,
                {
                    ns_tag("gml", "id"): "ID_" + str(feature_id),
                    # ns_tag(ns, 'id'): str(feature_id),
                },
            )
            append(feature, ns_tag(ns, "id")).text = str(feature_id)

            for select, unit, is_multi, fields in select_info:
                try:
                    ser = SERIALIZERS[field_type(select)]
                    unit_data = project.get(unit, [{}] if is_multi else {})

                    if is_multi:
                        output = parse_select(select, unit_data)
                    else:
                        output = get_path(select.field, unit_data)

                    ser(feature, ns, select, output, lang)
                except Exception:
                    pass

            encode_geometry(feature, geom_data, ns)


def _results_to_gml(request, query_data, lang, rid_url):
    ns = rid_url.scheme
    geometry_unit = rid_url.hostname
    typename = rid_to_typename_ns(rid_url.geturl())

    select = query_data["statements"]["select"]
    select.append(
        dict(name="geom", tag="select", unit=geometry_unit, field=["geom"], filters=[])
    )
    query_data["statements"]["select"] = select

    loc = "{}?{}".format(
        request.build_absolute_uri(reverse("basic-wfs", args=(lang,))),
        urlencode(
            {
                "SERVICE": "WFS",
                "REQUEST": "DescribeFeatureType",
                "TYPENAME": typename,
            }
        ),
    )

    attributes = {
        "xmlns:angled": "http://www.carto-station.com/angled",
        "xmlns:gml": "http://www.opengis.net/gml",
        "xmlns:wfs": "http://www.opengis.net/wfs",
        "xmlns:xsi": "http://www.w3.org/2001/XMLSchema-instance",
        "xsi:schemaLocation": "http://www.carto-station.com/angled {}".format(loc),
    }

    root = create("wfs:FeatureCollection", attributes)
    encode_query(root, query_data, request, ns, typename, geometry_unit, lang)

    return tree_to_string(create_tree(root))


def get_feature(request, lang, rid_url):
    qid = rid_url.path[1:]
    query = get_object_or_404(Query, id=qid)
    return _results_to_gml(request, QuerySerializer(query).data, lang, rid_url)
