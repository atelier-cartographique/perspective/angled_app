import unittest
import django
from angled.query import exec_query
from django.db import connection
from django.test.client import RequestFactory
from django.contrib.auth.models import User
from json import dumps

# TODO FIXME : refactoriser les tests (trop de copier/coller)

class TestQueries(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        django.setup()

    def test_school_seats(self):
        angled_return = self.school_seats_angled_query()
        sql_return = self.school_seats_sql_query()

        self.assertEqual(len(angled_return), len(sql_return))

        for i, ret in enumerate(angled_return):
            self.assertEqual(ret['id'], sql_return[i][0])


    def test_pjt_type_10(self):
        angled_return = self.pjt_type_10_angled_query()
        sql_return = self.pjt_type_10_sql_query()

        self.assertEqual(len(angled_return), len(sql_return))

        for i, ret in enumerate(angled_return):
            self.assertEqual(ret['id'], sql_return[i][0])

    def pjt_type_10_angled_query(self):
        where = [{
            "tag":"statement",
            "unit":"project_type",
            "filters":[{
                "tag":"filter",
                "field":["type"],
                "negate":False,
                "op":"exact",
                "value":10}]
            }]
            
        select = [{
            "tag":"select",
            "unit":"project_type",
            "field":["type"],
            "aggregate":"concat",
            "filters":[]
        },{
            "tag":"select",
            "unit":"name",
            "field":["name"],
            "aggregate":"concat",
            "filters":[]
        }]
        input = {
            'statements':  dict(where=where, select=select)
        }

        rf = RequestFactory()
        request = rf.get('/')
        request.user = User.objects.get(id=1)

        results = exec_query(input, request)
        return results

    def pjt_type_10_sql_query(self):
        cursor = connection.cursor()

        sql = "\
SELECT DISTINCT p.id \
    FROM \
        public.angled_project AS p, \
        public.angled_info_unit_project_type AS pt \
    WHERE \
        p.id = pt.project_id \
        AND pt.type_id = 10 \
"

        cursor.execute(sql)
        return cursor.fetchall()



    def school_seats_angled_query(self):
        where = [
            {
                'tag': 'statement',
                'unit': 'name',
                'filters': [{
                    'field': ['name', 'fr'],
                    'negate': False,
                    'op': 'icontains',
                    'value': 'aaaaaaaa',
                }],
            },
            {
                'tag': 'conj',
                'conj': 'OR',
            },
            {
                'tag':
                'statement',
                'unit':
                'name',
                'filters': [{
                    'field': ['name', 'nl'],
                    'negate': False,
                    'op': None,
                    'value': 'sch',
                }],
            },
        ]

        select = [
            {
                'tag': 'select',
                'unit': 'school_seats',
                'field': ['quantity'],
                'aggregate': 'sum',
                'filters': [
                    {
                        'tag': 'filter',
                        'field': ['opening'],
                        'op': 'gt',
                        'value': 2000,
                        'negate': False,
                    },
                ],
            },
            {
                'tag': 'select',
                'unit': 'name',
                'field': ['name', 'fr'],
                'aggregate': '',
                'filters': [],
            },
            {
                'tag': 'select',
                'unit': 'point',
                'field': ['geom'],
                'aggregate': '',
                'filters': [],
            },
        ]

        input = {
            'statements':  dict(where=where, select=select)
        }

        rf = RequestFactory()
        request = rf.get('/')
        request.user = User.objects.get(id=1)

        results = exec_query(input, request)
        return results

    def school_seats_sql_query(self):
        cursor = connection.cursor()

        sql = "\
SELECT DISTINCT p.id \
    FROM \
        public.angled_project AS p, \
        public.angled_info_unit_school_seats AS uss, \
        public.angled_info_unit_name AS uname \
    WHERE \
        p.id = uss.project_id \
        AND p.id = uname.project_id \
        AND lower(uss.opening) >= date '2000-01-01' \
        AND ( \
            ((uname.name->'fr')::text) LIKE '%aaaaaaaa%' \
            OR ((uname.name->'nl')::text) = 'sch' ) \
"

        cursor.execute(sql)
        return cursor.fetchall()

