import json
from tempfile import TemporaryDirectory
from pathlib import PosixPath
from xlsxwriter import Workbook

from django.http.response import FileResponse, HttpResponseBadRequest
from django.shortcuts import get_object_or_404
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
from django.contrib.gis.geos import GEOSGeometry
from rest_framework.decorators import action
from rest_framework.response import Response as DRFResponse
from api.permissions import ViewSetWithPermissionsAndFilter
from angled.serializers.query import (
    QuerySerializer,
    QueryShareSerializer,
    write_csv_file,
    write_xlsx_file,
)
from angled.models.query import Query, QueryShare
from angled.query import exec_query


class id_generator:
    def __init__(self):
        self.base = 0

    def __call__(self):
        self.base += 1
        return self.base


class QueryViewSet(ViewSetWithPermissionsAndFilter):
    """
    Views for CReate, Update, Delete Query
    """

    serializer_class = QuerySerializer
    queryset = Query.objects.all()

    @action(
        detail=False,
        methods=["post"],
        name="Run Query",
    )
    def exec(self, request):
        projects, _ = exec_query(request.data, request)
        return DRFResponse(projects)


class QueryShareViewSet(ViewSetWithPermissionsAndFilter):
    """
    Views for CReate, Update, Delete QueryShare
    """

    serializer_class = QueryShareSerializer
    queryset = QueryShare.objects.all()


NUMBER = "number"
STRING = "string"
BOOLEAN = "boolean"
DATE = "date"
DATETIME = "datetime"
TERM = "term"

MAPPED_TYPES = {
    # 'fk': NUMBER,
    "fk": STRING,
    "term": TERM,
    "label": STRING,
    "text": STRING,
    "raw_text": STRING,
    "number": NUMBER,
    "decimal": NUMBER,
    "month": NUMBER,
    "year": NUMBER,
    "date": DATE,
    "boolean": BOOLEAN,
    "varchar": STRING,
}


def to_centroid(feature):
    try:
        geometry = feature["geometry"]
        return {
            **feature,
            "geometry": json.loads(GEOSGeometry(json.dumps(geometry)).centroid.json),
        }
    except Exception:
        return feature


GEO_OPS = {"centroid": to_centroid}


class GeoOpNotImplemented(Exception):
    pass


def export_name(lang, query_instance=None):
    if query_instance is not None:
        return getattr(query_instance.name, lang)
    return "query"


def get_csv(request, query_data, lang, query_instance=None):
    with TemporaryDirectory() as dirname:
        root = PosixPath(dirname)
        # TODO: give a better filename (nw)
        filename = f"{export_name(lang, query_instance)}.csv"
        filepath = root.joinpath(filename)
        with filepath.open("w", encoding="utf8") as file:
            write_csv_file(request, file, query_data, lang)
        return FileResponse(filepath.open("rb"), as_attachment=True, filename=filename)


def get_xlsx(request, query_data, lang, query_instance=None):
    with TemporaryDirectory() as dirname:
        root = PosixPath(dirname)
        filename = f"{export_name(lang, query_instance)}.xlsx"
        filepath = root.joinpath(filename)
        with Workbook(filepath.as_posix()) as workbook:
            write_xlsx_file(request, workbook, query_data, lang)
        return FileResponse(filepath.open("rb"), as_attachment=True, filename=filename)


def results_to_geojson(
    request, query_data, geometry_unit, geo_op_name=None, query_instance=None
):
    lang = request.GET.get("lang")
    form = request.GET.get("form")
    if form is not None and lang is not None:
        if form == "csv":
            return get_csv(request, query_data, lang, query_instance)
        elif form == "xlsx":
            return get_xlsx(request, query_data, lang, query_instance)
        return HttpResponseBadRequest(
            f'You asked for a form we can not provide: "{form}"'
        )

    # lets fail early if it has to
    if geo_op_name is not None and geo_op_name not in GEO_OPS:
        raise GeoOpNotImplemented("{} is not a supported operation".format(geo_op_name))

    geo_op = GEO_OPS[geo_op_name] if geo_op_name is not None else None

    select = query_data["statements"]["select"]
    geom_key = "{}.geom".format(geometry_unit)
    select.append(
        dict(
            tag="select", unit=geometry_unit, field=["geom"], filters=[], name=geom_key
        )
    )
    query_data["statements"]["select"] = select
    query_results, fields_info = exec_query(query_data, request)

    fields = [(k, MAPPED_TYPES[t]) for k, t in fields_info]

    # we should not need it, maybe extraneous anxiety - pm
    id_gen = id_generator()

    features = []
    for project in query_results:
        try:
            geom_data = project.pop(geom_key, None)
            if isinstance(geom_data, dict) and "coordinates" in geom_data:
                features.append(
                    dict(
                        type="Feature",
                        id=project.get("id", id_gen()),
                        geometry=geom_data,
                        properties=project,
                    )
                )

        except Exception as ex:
            pass

    if geo_op is not None:
        collection = {
            "type": "FeatureCollection",
            "features": list(map(geo_op, features)),
            "fields": fields,
        }
    else:
        collection = {
            "type": "FeatureCollection",
            "features": features,
            "fields": fields,
        }

    return JsonResponse(collection)


@require_http_methods(["OPTIONS", "GET"])
def query_to_geojson(request, qid, geometry_unit):
    """
    qid A query ID
    geometry_unit one of 'point', 'line', 'polygon'
    """
    query = get_object_or_404(Query, id=qid)

    return results_to_geojson(
        request, QuerySerializer(query).data, geometry_unit, query_instance=query
    )


@require_http_methods(["OPTIONS", "GET"])
def query_to_geojson_op(request, qid, geometry_unit, geo_op_name):
    """
    qid A query ID
    geometry_unit one of 'point', 'line', 'polygon'
    geo_op_name a transformation name 'centroid'
    """
    query = get_object_or_404(Query, id=qid)

    return results_to_geojson(
        request,
        QuerySerializer(query).data,
        geometry_unit,
        geo_op_name=geo_op_name,
        query_instance=query,
    )


@require_http_methods(["POST"])
def query_data_to_geojson(request, geometry_unit):
    """
    The same as `query_to_geojson`, but getting data from POST
    as to let clients get layers from un-saved queries.

    geometry_unit one of 'point', 'line', 'polygon'
    """
    data = json.loads(request.body.decode("utf-8"))
    return results_to_geojson(request, data, geometry_unit, query_instance=None)
