import re
from datetime import date
from django.utils.text import slugify

from angled.models import ref
from angled.models import dyn
from angled.models.source import Source

from angled.management.import_base import (
    ImportCommand,
    UnitError,
    UnitResult,
    UnitSkip,
    MultiResult,
    get_cell_name,
    get_val,
    get_label,
    get_text,
    parse_int,
    parse_money,
    get_contact,
)

_cells = [
    'Secteurs', 'Id', 'Projet ou nom du site par défaut ',
    'Occupant et / ou exploitant / acteurs prospectés',
    'Propriétaire et /ou promoteur', 'Intentions connues', 'Actuel', 'projeté',
    'Horizon  conception Horizon réalisation Horizon installation',
    'Contacts (nom et tel)', 'Contacts (mails)', 'Site: n° rue',
    'dernière MAJ', 'shape_NUM'
]

UNITS = (
    ('', 'Secteurs'),
    ('', 'Id'),
    ('', 'Projet ou nom du site par défaut '),
    ('', 'Occupant et / ou exploitant / acteurs prospectés'),
    ('owner', 'Propriétaire et /ou promoteur'),
    ('intent', 'Intentions connues'),
    ('site_description_current', 'Actuel'),
    ('site_description_projected', 'projeté'),
    ('horizon',
     'Horizon  conception Horizon réalisation Horizon installation'),
    ('contact_name', 'Contacts (nom et tel)'),
    ('contact_email', 'Contacts (mails)'),
    ('site', 'Site: n° rue'),
    ('', 'dernière MAJ'),
    ('num', 'shape_NUM'),
)

thesaurus_intent = (
    'accès',
    'activité',
    'amélioration',
    'bureaux',
    'collecte',
    'commerces',
    'continuité',
    'coworking',
    'création',
    'démolition',
    'densification',
    'développer',
    'école',
    'éducation',
    'élargissement pertuis',
    'elargissement trottoir',
    'espace vert',
    'extension',
    'hôtel',
    'immeuble',
    'implantation',
    'infrastructures',
    'logement',
    'logements sociaux',
    'maison de repos',
    'mise en gestion',
    'nettoiement',
    'parc',
    'parc à  conteneur',
    'parking',
    'permis',
    'PME',
    'PRAS démographique',
    'rampe',
    'réaménagement',
    'reconversion',
    'redéveloppement',
    'relocalisation',
    'renovation',
    'RER',
    'résidence service',
    'résidentiel',
    'sens de circulation',
    'sous-station de traction',
    'tablier du pont',
    'talus',
    'TPE',
    'transformation',
    'vélo',
    'viabilisation',
)


def terms_in(text, thesaurus, domain):
    ret = []
    for t in thesaurus:
        if text.find(t) is not -1:
            ret.append(dyn.DomainReg.term(domain, t))
    return ret


CELL_NAMES = [(cell, model) for model, cell in UNITS]

cell_name = get_cell_name(UNITS)
val = get_val(UNITS)
label = get_label(UNITS)
text = get_text(UNITS)

# def pad_status(row):
#     v = val(row, 'status')
#     if len(v) == 0:
#         return UnitSkip()

#     return MultiResult(UnitResult(status=dyn.DomainReg.term('status_pad', v)))

# def project_type(row):
#     v = val(row, 'project_type')

#     return MultiResult(list(map(lambda x:
#         UnitResult(type=dyn.DomainReg.term('project_type_pad', x.strip())), v.split('/'))))


def owner(row):
    v = val(row, 'owner').replace('?', '').replace('/', '').replace('-', '')

    if len(v) == 0:
        return UnitSkip()

    return UnitResult(contact=get_contact(v))


def intent(row):
    v = val(row, 'intent')
    terms = terms_in(v, thesaurus_intent, 'intent')
    if len(terms) == 0:
        return UnitSkip()

    return MultiResult(map(lambda t: UnitResult(intent=t), terms))


def intent_note(row):
    v = val(row, 'intent')
    if len(v) == 0:
        return UnitSkip()
    return MultiResult(UnitResult(body=v))


def description(row):
    current = val(row, 'site_description_current')
    projected = val(row, 'site_description_projected')
    if len(current) == 0 and len(projected) == 0:
        return UnitSkip()
    ret = []
    if len(current) > 0:
        ret.append(UnitResult(body='Actuel\n========\n\n{}'.format(current)))
    if len(projected) > 0:
        ret.append(
            UnitResult(body='Projeté\n========\n\n{}'.format(projected)))

    return MultiResult(ret)


def poc(row):
    name = val(row, 'contact_name')
    email = val(row, 'contact_email')
    if len(name) == 0:
        return UnitSkip()

    return MultiResult(
        UnitResult(poc=get_contact(name, info='e-mail: {}'.format(email))))


class Command(ImportCommand):
    def get_project(self, row):
        try:
            num = val(row, 'num')
            src = Source.objects.get(source_id=num)
            return dyn.Project.objects.get(id=src.project_id)
        except Exception:  #Source.DoesNotExist:
            return None

    def process_row(self, row, create_unit):

        create_unit('owner', owner(row))
        create_unit('note', intent_note(row))
        create_unit('intent', intent(row))
        create_unit('note', description(row))
        create_unit('point_of_contact', poc(row))
