from django.db import models
from django.core.exceptions import ValidationError
from ruamel.yaml import YAML
from ruamel.yaml.scanner import ScannerError
import json
from angled.profile.parser import (
    parse_profile,
    ProfileValidationError,
    IndentationError,
)


class JSONField(models.TextField):
    def validate(self, value, model_instance):
        super().validate(value, model_instance)
        try:
            json.loads(value)
        except Exception as e:
            raise ValidationError('Erreur : {}'.format(e))


class YAMLField(models.TextField):
    def validate(self, value, model_instance):
        print("yaml validation")
        super().validate(value, model_instance)
        try:
            yaml = YAML(typ="safe")
            yaml.load(value)
        except ScannerError as e:
            raise ValidationError("Error reading yaml: {}".format(e))
        except Exception as e:
            raise ValidationError('Erreur: {}'.format(e))
        except:
            raise ValidationError('Erreur')


def validate_profile(value):
    try:
        parse_profile(value, True)
    except ProfileValidationError as e:
        raise ValidationError("Error parsing profile: {}".format(e))
    except IndentationError as e:
        raise ValidationError('Indentation error: {}'.format(e))
    except Exception as e:
        raise ValidationError('Unknown error: {}'.format(e))


class ProfileTextField(models.TextField):
    default_validators = [validate_profile]
    description = 'A profile description'


class Profile(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=256)
    text = ProfileTextField('Profile Layout')
    layout = JSONField()
    layout_yaml = YAMLField()

    def __str__(self):
        return "Profile #{}".format(self.name)

    # def clean(self):
    #     yaml = YAML(typ="safe")
    #     try:
    #         self.layout = json.dumps(yaml.load(self.layout_yaml))
    #     except ScannerError as e:
    #         raise ValidationError("Error reading Yaml")
