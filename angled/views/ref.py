from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.response import Response

from django.http import HttpResponseBadRequest

from api.permissions import (
    ViewSetWithPermissions,
    ObjectPermissions,
)
from angled.serializers.ref import (
    ContactSerializer,
    SiteSerializer,
    FundingOrgSerializer,
    LocalitySerializer,
    SchoolSiteSerializer,
    TeamSerializer,
    TagSerializer,
)
from angled.serializers.universe import (
    DomainSerializer,
    DomainMappingSerializer,
    TermSerializer,
)
from angled.models import (
    Contact,
    Site,
    SchoolSite,
    Nova,
    FundingOrg,
    Locality,
    Team,
    Domain,
    DomainFieldMapping,
    Term,
    Tag,
)


class ContactViewSet(ViewSetWithPermissions):
    """
    Views for CReate, Update, Delete contacts
    """
    serializer_class = ContactSerializer
    queryset = Contact.objects.all()


class SiteViewSet(ViewSetWithPermissions):
    """
    Views for CReate, Update, Delete sites
    """
    serializer_class = SiteSerializer
    queryset = Site.objects.all()


# class NovaViewSet(ViewSetWithPermissions):
#     """
#     Views for CReate, Update, Delete NOVA code
#     """
#     # todo fixme si dans db data=string sinon =dict
#     serializer_class = NovaSerializer
#     queryset = Nova.objects.all()

#     def retrieve(self, request, pk=None):
#         nova_ref = pk.replace('_', '/')
#         nova = Nova.get_or_create_uptodate(nova_ref)
#         if not nova:
#             return Response({"error": "ref nova not good"}, status=404)
#         else:
#             serializer = NovaSerializer(nova)
#             return Response(serializer.data)

#     def create(self, request, *args, **kwargs):
#         return HttpResponseBadRequest("Create is not allowed")

#     def update(self, request, *args, **kwargs):
#         return HttpResponseBadRequest("Update is not allowed")

#     def update(self, request, *args, **kwargs):
#         return HttpResponseBadRequest("Partial update is not allowed")


class FundingOrgViewSet(ViewSetWithPermissions):
    """
    Views for CReate, Update, Delete FundingOrg
    """
    serializer_class = FundingOrgSerializer
    queryset = FundingOrg.objects.all()


class LocalityViewSet(viewsets.ReadOnlyModelViewSet):
    """
    Views for Locality. 

    Implements only list and retrieve operations. 

    Permissions are "ObjectPermissions"
    """
    permission_classes = (ObjectPermissions, )
    serializer_class = LocalitySerializer
    queryset = Locality.objects.all()


class TeamViewSet(ViewSetWithPermissions):
    """
    Viewset for team
    """
    queryset = Team.objects.all()
    serializer_class = TeamSerializer

    def partial_update(self, request, *args, **kwargs):
        return HttpResponseBadRequest("Partial update is not allowed")


class DomainViewSet(ViewSetWithPermissions):
    """
    API endpoint for domains.
    """
    queryset = Domain.objects
    serializer_class = DomainSerializer

    @action(
        detail=True,
        methods=["post"],
    )
    def swap(self, request, pk):
        source_id= request.data.pop('source')
        target_id=request.data.pop('target')
        domain = Domain.objects.get(pk=pk)
        source = domain.term_set.get(pk=source_id)
        target = domain.term_set.get(pk=target_id)
        tmp_index = source.sort_index
        source.sort_index = target.sort_index
        target.sort_index = tmp_index
        
        source.save()
        target.save()
        # return HttpResponse(status=204)
        return Response(TermSerializer(domain.term_set.all(), many=True).data)


class DomainMappingViewSet(ViewSetWithPermissions):
    """
    API endpoint for mappings.
    """
    queryset = DomainFieldMapping.objects
    serializer_class = DomainMappingSerializer


class TermViewSet(ViewSetWithPermissions):
    """
    API endpoint for terms.
    """
    queryset = Term.objects
    serializer_class = TermSerializer


class TagViewSet(ViewSetWithPermissions):
    """
    API endpoint for tags.
    """
    queryset = Tag.objects
    serializer_class = TagSerializer


class SchoolSiteViewSet(viewsets.ReadOnlyModelViewSet):
    """
    Views for CReate, Update, Delete sites
    """
    permission_classes = (ObjectPermissions, )
    serializer_class = SchoolSiteSerializer
    queryset = SchoolSite.objects.all()
