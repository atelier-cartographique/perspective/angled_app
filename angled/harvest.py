from django.http import JsonResponse
from django.shortcuts import get_object_or_404

from angled.models.query import Query, QueryShare
from angled.serializers.query import QuerySerializer
from angled.query import exec_query, Filter, Statement
from angled.views.query import results_to_geojson


def make_where(geometry_unit, geometry_data):
    filter = Filter(['geom'], 'intersects', geometry_data, False).to_object()
    return Statement(geometry_unit, [filter]).to_object()


def harvest(request, rid, geometry_data):
    """
    ...
    """
    geometry_unit = rid.hostname
    qid = rid.path[1:]
    query = get_object_or_404(Query, id=qid)
    data = QuerySerializer(query).data
    where = data['statements']['where']
    where.append(make_where(geometry_unit, geometry_data))

    return results_to_geojson(request, data, geometry_unit)
