from django.db import migrations
from angled.models.dyn import get_unit_model
from angled.ui import UNITS_NAMES


def set_unit_eol(apps, schema_editor):
    Transaction = apps.get_model("angled", "Transaction")
    for tx in Transaction.objects.all().order_by("created_at"):
        for unit_name in UNITS_NAMES:
            field_name = f"info_unit_{unit_name}_set"
            query_set = getattr(tx, field_name)
            if query_set.count() > 0:
                Unit = apps.get_model("angled", f"info_unit_{unit_name}")
                Unit.objects.filter(
                    project=tx.project, created_at__lt=tx.created_at, eol__isnull=True
                ).update(eol=tx.created_at)


def unset_unit_eol(apps, schema_editor):
    for unit_name in UNITS_NAMES:
        model = apps.get_model("angled", f"info_unit_{unit_name}")
        model.objects.all().update(eol=None)


class Migration(migrations.Migration):

    dependencies = [
        ("angled", "0062_auto_20220323_1417"),
    ]

    operations = [migrations.RunPython(set_unit_eol, unset_unit_eol)]
