# Generated by Django 2.1.7 on 2019-06-11 08:29

import angled.models.profi
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('angled', '0016_listindex_unit'),
    ]

    operations = [
        migrations.AddField(
            model_name='profile',
            name='layout_yaml',
            field=angled.models.profi.YAMLField(default=''),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='listindex',
            name='unit',
            field=models.CharField(max_length=50, null=True),
        ),
    ]
