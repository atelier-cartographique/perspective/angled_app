from django.core.management.base import BaseCommand
from django.apps import apps


class Command(BaseCommand):
    def handle(self, *args, **options):
        config = apps.get_app_config('angled')
        for m in config.models:
            print(m)
