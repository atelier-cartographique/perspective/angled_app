"""The profile parser

It parses lines of the form:
<indentation><object_type><whitespace+><object_name>([<whitespace+><option>=<value>]*)

Indentation is the means of creating scopes

"""
from parsy import (
    regex,
    seq,
    string,
    whitespace,
    generate,
    alt,
    seq,
    letter,
    test_char,
    char_from,
    ParseError,
)
from json import dumps, loads, JSONEncoder

from ..ui import UNITS_NAMES


class Optioned:
    def set_options(self, o):
        self.options = o


class Page(Optioned):
    def __init__(self):
        self.boxes = []


class Box(Optioned):
    def __init__(self, direction):
        self.direction = direction
        self.children = []


class Unit(Optioned):
    def __init__(self, name):
        self.name = name


class UnknowUnit(Optioned):
    def __init__(self, name):
        self.name = name


class View(Optioned):
    def __init__(self, name):
        self.name = name


class Option:
    def __init__(self, key, value):
        self.key = key
        self.value = value


def letter_or_chars(*args):
    char_parsers = char_from(''.join(args))
    return alt(letter, char_parsers)


# We need to sort unit names such as to avoid
# shorter matches to make the parser fails on
# longer ones
ORDERED_UNIT_NAMES = reversed(sorted(UNITS_NAMES))

UNKNOWN_UNIT = '__unknown_unit__'
lexeme = lambda p: p << regex(r'\s*')
unit_name_parsers = [lexeme(string(u)) for u in ORDERED_UNIT_NAMES] + [
    lexeme(letter.at_least(1).concat()).tag(UNKNOWN_UNIT)
]
INDENT = whitespace.times(0, 124)
PAGE_TYPE = lexeme(string('page'))
BOX_TYPE = lexeme(string('box'))
UNIT_TYPE = lexeme(string('unit'))
VIEW_TYPE = lexeme(string('view'))

PAGE_NAME = lexeme(string('new')).desc('Page name')
BOX_NAME = alt(lexeme(string('vertical')),
               lexeme(string('horizontal'))).desc('Box name')
UNIT_NAME = alt(*unit_name_parsers).desc('Unit name')
VIEW_NAME = lexeme(
    alt(letter,
        char_from('_-0123456789')).at_least(1).concat()).desc('View name')


def make_unit(x):
    if isinstance(x, (
            list,
            tuple,
    )) and x[0] == UNKNOWN_UNIT:
        return UnknowUnit(x[1])
    return Unit(x)


PAGE_OBJECT = seq(PAGE_TYPE,
                  PAGE_NAME).map(lambda x: Page()).desc('Page object')
BOX_OBJECT = seq(BOX_TYPE,
                 BOX_NAME).map(lambda x: Box(x[1])).desc('Box object')
UNIT_OBJECT = seq(UNIT_TYPE,
                  UNIT_NAME).map(lambda x: make_unit(x[1])).desc('Unit object')
VIEW_OBJECT = seq(VIEW_TYPE,
                  VIEW_NAME).map(lambda x: View(x[1])).desc('View object')

OBJECT = alt(PAGE_OBJECT, BOX_OBJECT, UNIT_OBJECT, VIEW_OBJECT).desc('Object')

EQUALS = string('=')
OPTION_KEY = seq(letter_or_chars('_', '-').at_least(1).concat(),
                 EQUALS).map(lambda ok: ok[0])
number = lexeme(
    regex(r'-?(0|[1-9][0-9]*)([.][0-9]+)?([eE][+-]?[0-9]+)?')).map(float)
string_part = regex(r'[^"\\]+')
string_esc = string('\\') >> (
    string('\\')
    | string('/')
    | string('"')
    | string('b').result('\b')
    | string('f').result('\f')
    | string('n').result('\n')
    | string('r').result('\r')
    | string('t').result('\t')
    | regex(r'u[0-9a-fA-F]{4}').map(lambda s: chr(int(s[1:], 16))))
quoted = lexeme(
    string('"') >> (string_part | string_esc).many().concat() << string('"'))
OPTION_VALUE = number | quoted


def make_option(o):
    return Option(o[0], o[1])


def make_option_dict(os):
    ret = dict()
    for o in os:
        ret[o.key] = o.value
    return ret


OPTIONS = seq(OPTION_KEY,
              OPTION_VALUE).map(make_option).many().map(make_option_dict)


@generate
def LINE():
    indent = yield INDENT
    object = yield OBJECT
    options = yield OPTIONS
    object.set_options(options)
    if len(indent) > 0:
        return (len(indent[0]) // 2, object, options)

    return (0, object, options)


class ProfileValidationError(Exception):
    pass


class IndentationError(Exception):
    pass


def parse_profile(data, fail_on_warning=False):
    current_indent = 0
    pages = []
    boxes = {}
    for i, l in enumerate(data.split('\n')):
        line_number = i + 1
        stripped = l.lstrip()
        if len(stripped) > 0 and stripped[0] != '%':
            # print('Parsing "{}"'.format(l))
            try:
                parsed = LINE.parse(l)
            except ParseError as pex:
                message = 'Parse error on line {}\n>> {}'.format(
                    line_number, pex)
                raise ProfileValidationError(message)
            indent, obj, options = parsed
            if isinstance(obj, Page):
                pages.append(obj)
                boxes = {}
                current_indent = 0
            else:
                page = pages[len(pages) - 1]
                if len(boxes) == 0:
                    if not isinstance(obj, Box):
                        raise ProfileValidationError(
                            'Line {}: Page does not contains any Box to which attach this object'
                            .format(line_number))
                    boxes[indent] = obj
                    page.boxes.append(obj)

                elif isinstance(obj, UnknowUnit):
                    message = 'Line {}: Unknown unit "{}"'.format(
                        line_number, obj.name)
                    if fail_on_warning:
                        raise ProfileValidationError(message)
                    else:
                        print('[WARNING] {}'.format(message))
                    continue

                else:
                    try:
                        parent_box = boxes[indent - 1]
                    except KeyError:
                        expected = (indent - 1) * 2
                        got = len(l) - len(stripped)
                        raise IndentationError(
                            '\nLine {}: expected an indent of {} whitespaces,  got {}.\n\n{}'
                            .format(i, expected, got, l))

                    parent_box.children.append(obj)
                    if isinstance(obj, Box):
                        boxes[indent] = obj

    return loads(dumps(pages, cls=Encoder, indent=2))


class Encoder(JSONEncoder):
    def default(self, o):

        if isinstance(o, Page):
            return dict(
                type='Page',
                boxes=o.boxes,
                options=o.options,
            )
        elif isinstance(o, Box):
            return dict(
                type='DirectedContainer',
                direction=o.direction.title(),
                children=o.children,
                options=o.options,
            )
        elif isinstance(o, Unit):
            return dict(
                type='UnitWidget',
                name=o.name,
                options=o.options,
            )
        elif isinstance(o, View):
            return dict(
                type='ProjectView',
                name=o.name,
                options=o.options,
            )

        raise TypeError(repr(o) + " is not a valid profile node")
