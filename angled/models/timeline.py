
class EventTimeline(object):
    """
    Define an entry in timeline

    **Note**: currently only in use in timeline for multi model
    """

    def __init__(self, id, unit_name, created_at, uis=[]):
        self.id = id
        self.unit_name = unit_name
        self.created_at = created_at
        self.multi_uis = uis

    def is_multi(self):
        return len(self.multi_uis) > 0
