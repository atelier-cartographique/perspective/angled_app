from django.db import migrations
from angled.query import join_field
from json import loads

def fname(s): 
    comps = [s['unit'], join_field(s['field'])] 
    a = s['aggregate'] 
    if a is not None: 
        comps.append(a['name']) 
        f = a['filter'] 
        if f is not None: 
            comps.append(str(f['termId'])) 
    return '.'.join(comps) 



def add_name_to_query_selects(apps, schema_editor):
    Query = apps.get_model('angled', 'Query')

    for query in Query.objects.all():
        query_data = loads(query.statements)
        selects = query_data['select']
        for select in selects:
            if 'name' not in select or select['name'] is None:
                select['name'] = fname(select)
        query.statements = query_data
        query.save()


class Migration(migrations.Migration):

    dependencies = [
        ('angled', '0057_info_unit_school_take_off_opening'),
    ]

    operations = [
        migrations.RunPython(add_name_to_query_selects),
    ]
