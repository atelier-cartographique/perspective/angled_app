from django.shortcuts import get_object_or_404
from django.db import transaction
from django.http import (
    JsonResponse,
    Http404,
)
from django.db.models import Q
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework import status

from api.permissions import ViewSetWithPermissions, ViewSetWithPermissionsAndFilter

from angled.models import dyn
from angled.models import ref
from angled.models import profi
from angled.models import audience
from angled.models.audience import get_readings
from angled.models.subscription import (
    ProjectNotification,
    ProjectSubscription,
    auto_subscribe,
)
from angled.ui import UNITS_NAMES
from angled.serializers.units import (
    make_unit_serializer,
    make_tag_serializer,
)
from angled.serializers.audience import make_reading_serializer
from angled.serializers.projects import (
    ProjectSerializer,
)
from angled.serializers.profi import (
    ProfileSerializer,
)
from angled.serializers.timeline import EventTimelineSerializer


def make_unit_viewset(unit):
    local_unit = unit
    unit_name = local_unit["name"]

    class UnitViewset(ViewSetWithPermissions):
        queryset = dyn.get_unit_model(unit_name).objects.all()
        serializer_class = make_unit_serializer(local_unit)

        def get_serializer_context(self):
            context = super().get_serializer_context()
            context.update(dict(readings=get_readings()))
            return context

        # @action(
        #     detail=True,
        #     methods=['get'],
        #     name="readings for unit",
        #     description="List all readings for an information unit.")
        # def readings(self, request, pk=None):
        #     unit_model = dyn.get_reading_model(unit_name)
        #     unit_instance = get_object_or_404(unit_model, id=pk)
        #     readings = unit_instance.readings.all()
        #     serializer = self.get_serializer(readings, multi=True)

        #     return Response(serializer.data)

    return UnitViewset


def make_tag_viewset(unit):
    local_unit = unit
    unit_name = local_unit["name"]

    class TagViewSet(ViewSetWithPermissionsAndFilter):
        queryset = dyn.get_tag_model(unit_name).objects.all()
        serializer_class = make_tag_serializer(local_unit)

    return TagViewSet


def on_reading_create(unit_name, reading, user):
    unit_instance = reading.unit
    project_subs = ProjectSubscription.objects.filter(
        ~Q(user=user), active=True, project=unit_instance.project
    )
    for sub in project_subs:
        ProjectNotification.objects.create(
            subscription=sub,
            action="audience",
            unit=unit_name,
            unit_id=unit_instance.id,
        )


def make_reading_viewset(unit):
    local_unit = unit
    unit_name = local_unit["name"]

    class ReadingVieSet(ViewSetWithPermissionsAndFilter):
        queryset = dyn.get_reading_model(unit_name).objects.all()
        serializer_class = make_reading_serializer(local_unit)

        def create(self, request, *args, **kwargs):
            context = dict(
                user=request.user,
            )
            serializer = self.get_serializer(data=request.data, context=context)
            serializer.is_valid(raise_exception=True)
            self.perform_create(serializer)
            on_reading_create(unit_name, serializer.instance, request.user)
            headers = self.get_success_headers(serializer.data)
            return Response(
                serializer.data, status=status.HTTP_201_CREATED, headers=headers
            )

    return ReadingVieSet


def get_audiences_from_meta(meta):
    return audience.Audience.objects.filter(id__in=meta.get("audiences", []))


def update_unit_status(transaction, name):
    """
    moved from signals because the transaction being saved *before* units,
    it failed to update units status.
    """

    field_name = f"info_unit_{name}_set"
    query_set = getattr(transaction, field_name)
    if query_set.count() > 0:
        Unit = dyn.get_unit_model(name)
        current_units = Unit.objects.filter(
            ~Q(transaction=transaction),
            project=transaction.project,
            unit_status=dyn.BaseUnit.STATUS_CURRENT,
        )
        for unit in current_units:
            unit.eol = transaction.created_at
            unit.unit_status = dyn.BaseUnit.STATUS_ARCHIVED
            unit.save(update_fields=["eol", "unit_status"])

        query_set.all().update(unit_status=dyn.BaseUnit.STATUS_CURRENT)


def update_multiunit_status(transaction, deleted_units_qs):
    for unit in deleted_units_qs:
        unit.eol = transaction.created_at
        unit.unit_status = dyn.BaseUnit.STATUS_ARCHIVED
        unit.save(update_fields=["eol", "unit_status"])


class ProjectViewSet(ViewSetWithPermissions):
    """
    API endpoint for Project
    """

    queryset = dyn.Project.objects.all()
    serializer_class = ProjectSerializer

    def list(self, request, *args, **kwargs):
        """overriden method to load list of project"""
        names = request.GET.getlist("unit")
        if names is not None:
            data = dyn.Project.full_loader.get_all_as_dict(names=names)
        else:
            data = dyn.Project.full_loader.get_all_as_dict()

        serializer = self.get_serializer(data, many=True)
        return Response(serializer.data)

    def _handle_unit(self, data, unit_name, tx, audiences, context={}):
        """
        Create a new unit entry associated to a project.

        """

        unit = dyn.find_unit(unit_name)
        context.update(
            dict(
                transaction=tx,
                request=self.request,
                readings={},
            )
        )
        serializer = make_unit_serializer(unit)(
            data=data,
            context=context,
        )
        serializer.is_valid(raise_exception=True)
        new_unit = serializer.save()

        reading = dyn.get_reading_model(unit_name)
        for audience in audiences:
            reading.objects.create(unit=new_unit, audience=audience)

        return new_unit

    def _handle_multi_unit(self, data, unit_name, tx, audiences):
        """
        Handle "multi" unit entries, attached to the transaction, project and user given in tx parameter
        """
        list_model = dyn.get_multi_model(unit_name)
        new_instances = []
        kept_instances = []
        current_instances = list_model.objects.get_units(tx.project)
        list_index = dyn.ListIndex.objects.create(transaction=tx, unit=unit_name)

        for new_instance in data.get("new", []):
            new_instances.append(
                self._handle_unit(new_instance, unit_name, tx, audiences)
            )

        removed_ids = data.get("removed", [])
        for current_instance in current_instances:
            if current_instance.id not in removed_ids:
                kept_instances.append(current_instance)

        for instance in new_instances + kept_instances:
            list_model.objects.create(index=list_index, unit=instance)

        return dyn.get_unit_model(unit_name).objects.filter(id__in=removed_ids)

    @action(
        detail=True,
        methods=["post"],
        name="Update Unit Informations",
        description="Update the informational units associated to the project",
    )
    def inform(self, request, pk=None):

        proj = get_object_or_404(dyn.Project, id=pk)
        req_data = request.data
        unit_data = req_data.get("units", {})
        meta_data = req_data.get("meta", {})
        audiences = get_audiences_from_meta(meta_data)

        with transaction.atomic():
            tx = dyn.Transaction.objects.create(project=proj, user=request.user)

            for unit_name in unit_data:
                if dyn.is_multi(unit_name):
                    deleted_qs = self._handle_multi_unit(
                        unit_data[unit_name], unit_name, tx, audiences
                    )
                    update_multiunit_status(tx, deleted_qs)
                else:
                    self._handle_unit(unit_data[unit_name], unit_name, tx, audiences)
                    update_unit_status(tx, unit_name)

        serializer = self.get_serializer(proj)
        auto_subscribe(request.user, proj)

        return Response(serializer.data)

    @action(
        detail=True,
        methods=["get"],
        name="Timeline",
        description="Project's list of transactions",
    )
    def timeline(self, request, pk=None):
        proj = get_object_or_404(dyn.Project, id=pk)
        return JsonResponse(dyn.Project.timeline.get_transactions(proj), safe=False)

    # @action(
    #     detail=True,
    #     methods=["get"],
    #     name="Unit History",
    #     description="List all states of an information unit",
    # )
    # def unit(self, request, pk=None):
    #     """
    #     FIXME: it does not validate on the frontend
    #     """
    #     proj = get_object_or_404(dyn.Project, id=pk)
    #     unit_name = request.GET.get("name")

    #     context = dict(
    #         request=self.request,
    #         readings=get_readings(),
    #     )

    #     if unit_name not in UNITS_NAMES:
    #         raise Http404('There is not unit with name "{}"'.format(unit_name))
    #     unit = dyn.find_unit(unit_name)

    #     if unit["multi"] == False:
    #         data = dyn.Project.timeline.ui_timeline(proj, unit_name)
    #         serializer = make_unit_serializer(unit)(
    #             data,
    #             many=True,
    #             context=context,
    #         )
    #         for ii in serializer.data:
    #             if "tags" in ii:
    #                 ii.move_to_end("tags")
    #                 ii.popitem()
    #         return Response(serializer.data)
    #     else:
    #         data = dyn.Project.timeline.ui_timeline(proj, unit_name)
    #         serializer = EventTimelineSerializer(data, many=True, context=context)
    #         for ii in serializer.data:
    #             if "data" in ii:
    #                 for jj in ii["data"]:
    #                     if "tags" in jj:
    #                         jj.move_to_end("tags")
    #                         jj.popitem()
    #         return Response(serializer.data)


class ProfileViewSet(ViewSetWithPermissions):
    """
    API endpoint for Project
    """

    queryset = profi.Profile.objects.all()
    serializer_class = ProfileSerializer
