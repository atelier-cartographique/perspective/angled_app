from enum import Enum


class MissingEnumeration(Exception):
    def __init__(self, name):
        super().__init__('Missing Enumeration "{}"'.format(name))


def choices_from_enum(enumeration):
    """From
    https://hackernoon.com/using-enum-as-model-field-choice-in-django-92d8b97aaa63
    """
    return [(tag, tag.value) for tag in enumeration]


# enums


class perm_mod(Enum):
    """CREATE TYPE perm_mod AS ENUM ('read', 'write');
    """
    Read = 'read'
    Write = 'write'


class occupying_status(Enum):
    """CREATE TYPE occupying_status AS ENUM ('actuel' ,'futur' , 'passe');
    """
    Current = 'actuel'
    Future = 'futur'
    Past = 'passe'


class prl_ah(Enum):
    """CREATE TYPE prl_ah AS ENUM ('PRL', 'AH');
    """
    PRL = 'PRL'
    AH = 'AH'


class public_construction_type(Enum):
    """CREATE TYPE public_construction_type AS ENUM ('LS', 'Lmod', 'Lmoy');
    """
    LS = 'LS'
    Lmod = 'Lmod'
    Lmoy = 'Lmoy'


class rent_own(Enum):
    """CREATE TYPE rent_own AS ENUM ('rent', 'own');
    """
    Rent = 'rent'
    Own = 'own'


class housing_class(Enum):
    """CREATE TYPE housing_class AS ENUM ('studio', '1ch', '2ch', '3ch', '4chplus', 'pmr');
    """
    Studio = 'studio'
    One = '1ch'
    Two = '2ch'
    Three = '3ch'
    Four = '4chplus'
    PMR = 'pmr'


class ppas_project_type(Enum):
    """CREATE TYPE ppas_project_type AS ENUM (
    'create',
    'update',
    'delete'
    );
    """
    Create = 'create'
    Update = 'update'
    Delete = 'delete'


class ppas_project_step(Enum):
    """CREATE TYPE ppas_project_step AS ENUM (
    'Demande d’avis', 
    'Décision du conseil communal', 
    'RIE', 
    'Enquêtes publiques', 
    'Commission de concertation', 
    'Adoption par le conseil communal', 
    'Approbation par le gouvernement'
    );
    """
    Request = 'Demande d’avis'
    Decision = 'Décision du conseil communal'
    RIE = 'RIE'
    Survey = 'Enquêtes publiques'
    Concertation = 'Commission de concertation'
    Adoption = 'Adoption par le conseil communal'
    Approval = 'Approbation par le gouvernement'


class school_level(Enum):
    """CREATE TYPE school_level_detail AS ENUM (
    'maternel', 
    'primaire', 
    'secondaire', 
    'secondaire1', 
    'secondaire2',
    'secondaire3'
    );
    """
    Mat = 'maternel'
    Pri = 'primaire'
    Sec = 'secondaire'
    Sec1 = 'secondaire1'
    Sec2 = 'secondaire2'
    Sec3 = 'secondaire3'


class school_level_basic(Enum):
    """CREATE TYPE school_level AS ENUM (
    'fondamental',
    'secondaire'
    );
    """
    Fond = 'fondamental'
    Sec = 'secondaire'