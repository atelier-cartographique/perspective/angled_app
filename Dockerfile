# TODO: préciser la version
FROM python:3

# TODO: nettoyer le cache des paquets
RUN apt update && apt install -y libsqlite3-mod-spatialite python3-gdal

# TODO: indiquer les uid et gid en paramètre
RUN useradd --uid 1000 --create-home user

COPY . /app

WORKDIR /app

RUN pip install -r requirements.txt

