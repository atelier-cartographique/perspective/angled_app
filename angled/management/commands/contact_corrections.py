from json import dumps, loads
from django.core.management.base import BaseCommand
from angled.management.commands.import_bma import actors
from angled.models import audience
from angled.models.dyn import get_unit_model, get_reading_model
from angled.models import ref
from angled.models import dyn
from angled.models.audience import Audience
from django.contrib.auth.models import User
import re
from csv import DictReader, writer as csv_writer

Actor = dyn.get_unit_model("actor")
Bidder = dyn.get_unit_model("bidders")
Winner = dyn.get_unit_model("tender_winner")
MultiActor = dyn.get_multi_model("actor")
ActorReading = dyn.get_reading_model("actor")

# STATUS_CURRENT = dyn.BaseUnit.STATUS_CURRENT


def term_name(tid):
    try:
        t = ref.Term.objects.get(pk=tid)
        return str(t.name)
    except:
        return str(f"<Term {tid}>")


def change_name(row, dry_run, ok, err):
    """
    Homogénéisation et correction des noms des intervenants(majuscules, acronymes…).
    Il a fallu une analyse cas par cas. Une liste est déjà faite.

    exemple: Czveg Rigby (2174), ADT_ATO (831)

    -> Changement nom

    >> id: ID du contact à modifier
    >> name: Nom de remplacement
    """
    cid = row.get("id")
    name = row.get("name")
    try:
        contact = ref.Contact.objects.get(pk=cid)
    except Exception as ex:
        err(f"row>> {row}")
        err(f"Failed to get contact {cid}: {ex}")
        return
    if dry_run:
        ok(f"change_name {cid} {contact.name} -> {name}")
        return
    contact.name = name
    contact.save()


class CSV:
    def __init__(self, name, header) -> None:
        self._inited = False
        self.name = name
        self.header = header

    def init(self):
        if self._inited:
            raise Exception("CSV {self.name} already inited")
        self._inited = True
        self.actor_file = open(f"{self.name}.csv", "w")
        self.actor_csv = csv_writer(self.actor_file)
        self.actor_csv.writerow(self.header)

    def writerow(self, row):
        if self._inited is False:
            self.init()
        self.actor_csv.writerow(row)

    def close(self):
        if self._inited:
            self.actor_file.close()


actor_csv = CSV(
    "missing_info_actors",
    [
        "id",
        "contact name",
        "type",
        "project name",
        "role",
        "managers",
        "encoder",
        "encoding date",
    ],
)


def format_user(u):
    fn = u.get_full_name()
    email = u.email
    return f"{fn} <{email}>"


def format_project(p):
    try:
        return str(p.info_unit_name_set.first().name)
    except:
        return str(p.id)


def managers(p):
    ms = []
    for manager in p.info_unit_manager_set.all():
        u = manager.manager_user
        fn = u.get_full_name()
        email = u.email
        context = term_name(manager.context.id)
        ms.append(f"{fn} ({context}) <{email}>")

    return ", ".join(ms)


def missing_info(row, dry_run, ok, err):
    """
    Des intervenants où il n'y a pas assez d'information pour savoir
    de quel intervenant on parle ou s'il faut des clarifications

    exemple: Mouton (2460), Groupe GEI (2284)

    -> Consulter les chargés de projet

    >> id: ID du contact à modifier
    """
    cid = row.get("id")
    try:
        contact = ref.Contact.objects.get(pk=cid)
    except Exception as ex:
        err(f"row>> {row}")
        err(f"Failed to get contact {cid}: {ex}")
        return
    actors = Actor.objects.filter(contact=contact)
    members = ref.TeamMember.objects.filter(member=contact)

    known_as_actor = set()
    for actor in actors:
        role = str(actor.type.name)
        # project = format_project(actor.project)

        known_as_actor.add(
            (actor.project, actor.created_at, role, format_user(actor.user))
        )

    if len(known_as_actor) > 0:
        for project, date, role, user in known_as_actor:
            if dry_run:
                pname = format_project(actor.project)
                ok(
                    f"Actor [{contact.name} - {cid}] Project: {pname} ({date}); Role {role}  =>  {user}"
                )
            else:
                actor_csv.writerow(
                    [
                        cid,
                        contact.name,
                        "actor",
                        format_project(actor.project),
                        role,
                        managers(project),
                        user,
                        str(date.date()),
                    ]
                )

    for member in members:
        for bidder in Bidder.objects.filter(bidders_team=member.team):
            if dry_run:
                ok(
                    f"Bidder [{contact.name} - {cid}] Project: {format_project(bidder.project)} ({bidder.created_at.date()});  Role {member.role} =>  {format_user(bidder.user)}"
                )
            else:
                actor_csv.writerow(
                    [
                        cid,
                        contact.name,
                        "bidder",
                        format_project(bidder.project),
                        member.role,
                        managers(bidder.project),
                        format_user(bidder.user),
                        str(bidder.created_at.date()),
                    ]
                )

    for member in members:
        for bidder in Winner.objects.filter(winner_team=member.team):
            if dry_run:
                ok(
                    f"Winner [{contact.name} - {cid}] Project: {format_project(bidder.project)} ({bidder.created_at.date()});  Role {member.role} =>  {format_user(bidder.user)}"
                )
            else:
                actor_csv.writerow(
                    [
                        cid,
                        contact.name,
                        "winner",
                        format_project(bidder.project),
                        member.role,
                        managers(bidder.project),
                        format_user(bidder.user),
                        str(bidder.created_at.date()),
                    ]
                )


def duplicate_simple(row, dry_run, ok, err):
    """
    Des intervenants sont répétés. Un a été choisi comme l'officiel,
    ceux qui se répètent doivent être rapatrier vers l'officiel.
    Les répétitions on été repérées dans une liste.

    exemple: Fonds du logement (3) (répetition)/ officiel (1631)

    -> Rapatrier/ merge à voir

    >> source: ID du contact dupliqué
    >> target: ID du contact "officiel"
    """
    source_id = row.get("source")
    target_id = row.get("target")
    try:
        source = ref.Contact.objects.get(pk=source_id)
        target = ref.Contact.objects.get(pk=target_id)
    except Exception as ex:
        err(f"row>> {row}")
        err(f"Failed to get contact of {source_id} or {target_id}: {ex}")
        return
    if dry_run:
        count_actors = Actor.objects.filter(contact=source).count()
        count_members = ref.TeamMember.objects.filter(member=source).count()
        ok(
            f"duplicate_simple {source.name} {target.name} -> actors:{count_actors}; members:{count_members} "
        )
        return
    for actor in Actor.objects.filter(contact=source):
        actor.contact = target
        actor.save()

    for member in ref.TeamMember.objects.filter(member=source):
        member.member = target
        member.save()

    source.delete()


def duplicate_double_impl(target, sources):

    for actor in Actor.objects.filter(contact=target):
        user = actor.user
        project = actor.project
        transaction = actor.transaction
        typ = actor.type
        created_at = actor.created_at
        unit_status = actor.unit_status
        index = actor.transaction.listindex_set.get(unit="actor")
        audiences = [reading.audience for reading in actor.readings.all()]
        for source in sources:
            new_actor = Actor.objects.create(
                user=user,
                project=project,
                transaction=transaction,
                type=typ,
                created_at=created_at,
                contact=source,
                unit_status=unit_status,
            )

            MultiActor.objects.create(index=index, unit=new_actor)
            for audience in audiences:
                ActorReading.objects.create(unit=new_actor, audience=audience)

        actor.delete()

    for member in ref.TeamMember.objects.filter(member=target):
        team = member.team
        role = member.role
        for source in sources:
            ref.TeamMember.objects.create(team=team, member=source, role=role)

        member.delete()


def duplicate_double(row, dry_run, ok, err):
    """
    Des intervenants qui ont été créés comme un seul et
    sont des répetitions sur des existants individuels

    exemple: Cdev et ULB (35)/ officiels (29)(570)

    -> Séparer et rapatrier un par un vers les officiels

    >> source: Liste séparé par des `;` d'IDs des individuels
    >> target: ID du contact à remplacer
    """

    source_ids = row.get("source").split(";")
    target_id = row.get("target")
    sources = [ref.Contact.objects.get(pk=source_id) for source_id in source_ids]
    target = ref.Contact.objects.get(pk=target_id)

    if dry_run:
        names = ", ".join([s.name for s in sources])
        ok(f"duplicate_double  {target.name} # {names} ")
        return

    duplicate_double_impl(target, sources)
    target.delete()


def duplicate_mixed(row, dry_run, ok, err):
    """
    Des intervenants qui ont été crées comme un seul et sont
    des répetitions sur des existants individuels,
    SAUF un ou plusieurs qui sont nouveaux.

    exemple: Arcadis Belgium_Lab705_D'ici là (3029)/ officiels existants (1941)(2748)

    -> Séparer et rapatrier un par un vers les officiels et créer celui qui n'existe pas encore en individuel. Ex. D'ici là

    >> source: Liste séparé par des `;` d'IDs des individuels
    >> new: Liste séparé par des `;` des noms des intervenants
    >> target: ID du contact à remplacer
    """

    source_names = row.get("new").split(";")
    source_ids = row.get("source").split(";")
    target_id = row.get("target")

    target = ref.Contact.objects.get(pk=target_id)

    if dry_run:
        names = ", ".join(source_names)
        ok(f"duplicate_mixed  {target.name} # {names} ")
        return

    sources = [ref.Contact.objects.get(pk=source_id) for source_id in source_ids] + [
        ref.Contact.objects.create(name=name) for name in source_names
    ]
    duplicate_double_impl(target, sources)
    target.delete()


def grouped(row, dry_run, ok, err):
    """
    Des intervenants qui ont  été crées comme un seul
    mais individuellement n'existent pas dans la liste d'intervenants.

    exemple: Roger Diener ; Patrick Berger : Sofia Von Ellrichshausen (266)

    -> Créer individuellement

    >> source: Liste séparé par des `;` des noms des intervenants
    >> target: ID du contact à remplacer
    """
    source_names = row.get("source").split(";")
    target_id = row.get("target")

    try:
        target = ref.Contact.objects.get(pk=target_id)
    except Exception as ex:
        err(f"row>> {row}")
        err(f"Failed to get contact  {target_id}: {ex}")
        return

    if dry_run:
        names = ", ".join(source_names)
        ok(f"grouped  {target.name} # {names} ")
        return

    sources = [ref.Contact.objects.create(name=name) for name in source_names]
    duplicate_double_impl(target, sources)
    target.delete()


def ghost(row, dry_run, ok, err):
    """
    Intervenants qui sont toujours dans la liste excel
    mais introuvables sur geodata car ils font partie de l'historique?

    exemple: EMS (2234)

    -> Vérifier et si possible les effacer complètement
    """

    cid = row.get("id")
    try:
        contact = ref.Contact.objects.get(pk=cid)
    except Exception as ex:
        err(f"row>> {row}")
        err(f"Failed to get contact {cid}: {ex}")
        return
    actors = Actor.objects.filter(contact=contact).count()
    members = ref.TeamMember.objects.filter(member=contact).count()
    if actors > 0 or members > 0:
        err(f"{contact.name} is still in use")
    else:
        if dry_run:
            ok(f"removing {contact.name}")
        else:
            contact.delete()


ACTIONS = [
    "name",
    "missing",
    "simple",
    "double",
    "mixed",
    "group",
    "ghost",
]


class Command(BaseCommand):
    def add_arguments(self, parser):

        parser.add_argument("-i", "--input", type=str, help="A CSV file", default=None)
        parser.add_argument("-d", "--dry-run", action="store_true", dest="dry_run")
        parser.add_argument("-a", "--action", type=str, choices=ACTIONS)

    def with_input(self, input_path, action, dry_run):

        ok = lambda s: self.stdout.write(self.style.SUCCESS(s))
        err = lambda e: self.stdout.write(self.style.ERROR(e))

        with open(input_path) as input_file:
            csv_reader = DictReader(input_file)
            for row in csv_reader:
                if action == "name":
                    change_name(row, dry_run, ok, err)
                elif action == "missing":
                    missing_info(row, dry_run, ok, err)
                elif action == "simple":
                    duplicate_simple(row, dry_run, ok, err)
                elif action == "double":
                    duplicate_double(row, dry_run, ok, err)
                elif action == "mixed":
                    duplicate_mixed(row, dry_run, ok, err)
                elif action == "group":
                    grouped(row, dry_run, ok, err)
                elif action == "ghost":
                    ghost(row, dry_run, ok, err)

        actor_csv.close()

    def handle(self, *args, **options):
        input_path = options.pop("input")
        action = options.pop("action")
        dry_run = options.get("dry_run", False)
        if input_path is not None:
            return self.with_input(input_path, action, dry_run)
