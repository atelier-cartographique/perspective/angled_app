from pathlib import Path
from setuptools import setup, find_packages

version = '1.0.1'

name = 'angled'
description = 'carto.station app for perpective.brussels'
url = 'https://gitlab.com/atelier-cartographique/perspective/angled_app'
author = 'Atelier Cartographique'
author_email = 'contact@atelier-cartographique.be'
license = 'Affero GPL3'

classifiers = [
    'Development Status :: 3 - Alpha',
    'Intended Audience :: Developers',
    'Topic :: Software Development :: Build Tools',
    'License :: OSI Approved :: GNU Affero General Public License v3 or later (AGPLv3+)',
    'Operating System :: POSIX',
    'Programming Language :: Python',
    'Programming Language :: Python :: 3.5',
]

install_requires = ['django', 'pyshp', 'psycopg2-binary']

packages = [
    'angled',
    'angled.views',
    'angled.models',
    'angled.templatetags',
    'angled.serializers',
    'angled.migrations',
    'angled.management',
    'angled.management.commands',
    'angled.profile',
    'angled.wfs',
]

setup(
    name=name,
    version=version,
    url=url,
    license=license,
    description=description,
    author=author,
    author_email=author_email,
    packages=packages,
    install_requires=install_requires,
    classifiers=classifiers,
    include_package_data=True,
)
