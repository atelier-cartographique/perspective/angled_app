from django.contrib.gis.db import models


class Source(models.Model):
    """An ancilary model to store some facts about how data has been imported
    """
    id = models.AutoField(primary_key=True)
    filename = models.CharField(max_length=256)
    source_id = models.IntegerField()
    project_id = models.IntegerField()
    data = models.TextField()
