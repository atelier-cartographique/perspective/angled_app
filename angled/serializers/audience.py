from rest_framework import serializers
from lingua.serializers import LinguaRecordSerializer
from angled.models.audience import Audience
from angled.models import dyn
from api.rules import (
    group_intersects,
)


class AudienceSerializer(serializers.ModelSerializer):
    """Audience Serializer"""

    name = LinguaRecordSerializer()
    description = LinguaRecordSerializer()

    class Meta:
        model = Audience
        fields = ("id", "name", "description", "group")


class NotAReader(Exception):
    pass


def make_reading_serializer(unit):
    unit_name = unit["name"]
    unit_model = dyn.get_unit_model(unit_name)

    class ReadingSerializer(serializers.ModelSerializer):
        """Reading Serializer"""

        def create(self, validated_data):
            user = self.context["user"]
            model = self.Meta.model
            audience = validated_data.pop("audience")
            unit = validated_data.pop("unit")

            others = model.objects.filter(unit=unit)
            can_read = user.id == unit.user.id or group_intersects(
                user.groups.all(), [o.audience.group for o in others]
            )
            if can_read:
                return model.objects.create(audience=audience, unit=unit)

            raise NotAReader()

        class Meta:
            model = dyn.get_reading_model(unit_name)
            fields = ("id", "audience", "unit")

    return ReadingSerializer
