

## writing import commands

A minimal version would look like this:

```python
from angled.management.import_base import  ImportCommand, UnitResult

class Command(ImportCommand):
    def process_row(self, row, create_unit):
        # everything will have the same name
        create_unit('name', UnitResult(name='A Name'))
```

and if saved in ```management/commands/import_xxx.py```, will become usable under ```manage.py``` with:

```sh
./manage.py import_xxx /path/to/data.csv username
```


### create_unit

The function ```create_unit``` that is made available expects the name of the information unit as its first argument, and one of ```UnitResult```, ```UnitError```, ```UnitSkip``` as its second argument. It will take care of creating the unit attached to the current project if given a result or to log the error if given one.


### UNITS

Sort of a convention to have a mappting of unit name to column names in the CSV. An extract from the import_bma command looks like this

```python
UNITS = (
    ('', ''),
    ('name', 'NomProjet\nNaamProject'),
    ('program', 'Programme\nProgramma'),
    ('description', 'Description Fr/Nl\nOmschrijving FR/Nl'),
    ('bidders_with_fee', 'NbreDefr\nAantalVergoeding'),
    # ...
)
```


### helpers

They go into ```angled.management.import_base```.

ATM we've got 

```python
def get_val(UNITS):
    cell_name = get_cell_name(UNITS)

    def inner(row, field):
        return row[cell_name(field)]

    return inner

# To be used like
val = get_val(UNITS)
def my_parser(row):
    v = val(row, 'unit_name')
    # do something with v

```

```python
def get_label(UNITS):
    val = get_val(UNITS)

    def inner(row, field, key):
        v = val(row, field).strip()
        if 0 == len(v):
            return UnitSkip()

        label_func = dyn.unit_uni_label(key)
        kwargs = dict()
        kwargs[key] = label_func(v)
        return UnitResult(**kwargs)

    return inner

# To be used like
label = get_label(UNITS)
class Command(ImportCommand):
    def process_row(self, row, create_unit):
        create_unit('name', label(row, 'name', 'name'))
```