# Generated by Django 2.1.7 on 2019-06-24 13:02

import angled.models.profi
from django.db import migrations
import lingua.fields


class Migration(migrations.Migration):

    dependencies = [
        ('angled', '0028_auto_20190624_1115'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='address',
            name='locality',
        ),
        migrations.RemoveField(
            model_name='info_unit_address',
            name='address',
        ),
        migrations.AddField(
            model_name='info_unit_address',
            name='address',
            field=lingua.fields.LinguaField(verbose_name='address', default={'fr': 'fr', 'nl': 'nl', 'en': 'en'}),
        ),
        migrations.DeleteModel(
            name='Address',
        ),
    ]
