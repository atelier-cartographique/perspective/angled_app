from collections import OrderedDict

from django.apps import apps
from rest_framework import serializers
from rest_framework.fields import empty

from angled.models import dyn, ref
from angled.serializers.fields import (
    UnitField,
    YearField,
    MonthField,
    MessageField,
    TermField,
    UnitCharField,
    UnitIntegerField,
    UnitDecimalField,
    UnitDateField,
    UnitBooleanField,
    UnitGeometryField,
    TagField,
)
from angled.rules import can_read_unit

FieldMap = {
    "fk": UnitIntegerField,
    "term": TermField,
    "label": MessageField,
    "text": MessageField,
    "raw_text": UnitCharField,
    "varchar": UnitCharField,
    "number": UnitIntegerField,
    "decimal": UnitDecimalField,
    "month": MonthField,
    "year": YearField,
    "date": UnitDateField,
    "boolean": UnitBooleanField,
    "geometry": UnitGeometryField,
}


def is_model_instance(instance):
    return isinstance(instance, dyn.BaseUnit)


class Undefined(Exception):
    pass


def field_names(fields):
    return [f[1] for f in fields]


def make_ts_name(base_name, suffix=""):
    """
    Prepare a name compatible with typescript associated to unit
    """
    return "Unit" + base_name.title().replace("_", "") + suffix


def get_unit_readings(readings, unit_name, unit_id):
    if unit_name in readings and unit_id in readings[unit_name]:
        return [
            dict(id=r.id, audience=r.audience, unit=unit_id)
            for r in readings[unit_name][unit_id]
        ]
    return []


def get_instance_tags(name, unit):
    tagged_name = "tagged_{}".format(name)
    return list(
        map(
            lambda t: dict(id=t.id, unit=t.unit_id, tag=t.tag_id),
            getattr(unit, tagged_name).all(),
        )
    )


def get_instance_base(user, readings, unit, name):
    getter = getattr
    unit_id = getter(unit, "id")
    return dict(
        unit=make_ts_name(name),
        id=unit_id,
        created_at=round(getter(unit, "created_at").timestamp() * 1000),
        project=getter(unit, "project_id"),
        user=getter(unit, "user_id"),
        transaction=getter(unit, "transaction_id"),
        tags=get_instance_tags(name, unit),
        readings=get_unit_readings(readings, name, unit_id),
        eol=0,
    )


def get_dict_base(user, readings, unit, name):
    def getter(o, k, d=None):
        return o.get(k, d)

    unit_id = getter(unit, "id")

    return dict(
        unit=make_ts_name(name),
        id=unit_id,
        created_at=round(getter(unit, "created_at").timestamp() * 1000),
        project=getter(unit, "project"),
        user=getter(unit, "user"),
        transaction=getter(unit, "transaction"),
        tags=getter(unit, "tags", []),
        readings=get_unit_readings(readings, name, unit_id),
        eol=0,
    )


def get_base(user, readings, unit, name):
    """
    serialize a single unit to a dictionary

    `unit` may be a unit, or a dictionary representing an unit

    """
    is_dict = isinstance(unit, (dict, OrderedDict))
    is_unit_instance = isinstance(unit, (dyn.BaseUnit,))
    assert is_dict or is_unit_instance, "Got {}".format(type(unit))

    base = (
        get_instance_base(user, readings, unit, name)
        if is_unit_instance
        else get_dict_base(user, readings, unit, name)
    )

    base["visible"] = can_read_unit(user, readings, name, base)

    return OrderedDict(base)


def ser_unit_at(at_time, context, unit_info, project):
    """
    Serialize an unit into a dictionary at time at_time

    """
    name = unit_info["name"]
    multi = unit_info["multi"]
    model = dyn.get_unit_model(name)
    unit_serializer = make_unit_serializer(unit_info)
    if multi:
        multi_model = dyn.get_multi_model(name)
        units = list(multi_model.objects.get_units_at(project, at=at_time))
        # pour les multi, il est possible qu'une liste ait été utilisée dans le
        # futur et soit vide au moment du `at`
        if len(units) == 0:
            raise Undefined
        return [unit_serializer(unit, context=context).data for unit in units]
    else:
        if at_time is None:
            try:
                unit = model.objects.filter(project__id=project.id).get(
                    eol__isnull=True
                )
            except model.DoesNotExist:
                raise Undefined
        else:
            unit = (
                model.objects.filter(project__id=project.id)
                .filter(created_at__lte=at_time)
                .first()
            )
            if unit is None:
                raise Undefined

        return unit_serializer(unit, context=context).data


class UnitSerializerError(Exception):
    pass


UNIT_SERIALIZERS = {}


def _make_unit_serializer_impl(name, unit):
    global UNIT_SERIALIZERS

    tagged_name = "tagged_{}".format(name)
    unit_fields = unit["fields"]

    unit_serializer_fields = dict()
    cached_fields = {
        "id": serializers.IntegerField(required=False),
        "project": serializers.IntegerField(required=False),
        "user": serializers.IntegerField(required=False),
        "transaction": serializers.IntegerField(required=False),
        "created_at": serializers.DateTimeField(required=False),
        "tags": TagField(required=False),
    }

    for f in unit_fields:
        field_type = f[0]
        field_name = f[1]
        unit_serializer_fields[field_name] = FieldMap[field_type]()
        cached_fields[field_name] = FieldMap[field_type]()

    class UnitSerializer(serializers.Serializer):
        id = serializers.IntegerField(required=False)
        project = serializers.IntegerField(required=False)
        user = serializers.IntegerField(required=False)
        transaction = serializers.IntegerField(required=False)
        created_at = serializers.DateTimeField(required=False)
        tags = TagField(required=False)

        def __init__(self, *args, **kwargs):
            super().__init__(*args, **kwargs)
            context = kwargs.get("context", None)
            request = context.get("request", None)
            readings = context.get("readings", None)
            assert request is not None
            assert readings is not None

        def to_internal_value(self, data):
            """We reimplemented it to avoid going through DRF"""
            ret = OrderedDict()
            for name, field in cached_fields.items():
                primitive_value = data.get(name)
                if primitive_value is not None:
                    validated_value = field.run_validation(primitive_value)
                    ret[name] = validated_value

            return ret

        def run_validation(self, data=empty):
            return self.to_internal_value(data)

        def get_user(self):
            try:
                return self.context["request"].user
            except Exception:
                raise UnitSerializerError("Should have a request object on context")

        def get_readings(self):
            try:
                return self.context["readings"]
            except Exception:
                raise UnitSerializerError("Should have a readings object on context")

        def to_representation(self, instance):
            user = self.get_user()
            readings = self.get_readings()

            ret = get_base(user, readings, instance, name)
            fields = filter(lambda f: isinstance(f, UnitField), self.fields.values())

            for field in fields:
                try:
                    attribute = field.get_attribute(instance)
                except serializers.SkipField:
                    continue

                ret[field.field_name] = field.to_representation(
                    attribute, ret.get("visible", False)
                )

            return ret

        def get_tags(self, obj):
            return [t.id for t in getattr(obj, tagged_name).all()]

        def create(self, validated_data):
            # we have to special case term fields and FKs as django wants a model instance
            # rather than an ID, sad.

            term_fields = [f[1] for f in filter(lambda f: f[0] == "term", unit_fields)]

            fk_fields = dict()
            for f in filter(lambda f: f[0] == "fk", unit_fields):
                fk_parts = f[2].split(".")
                if len(fk_parts) == 1:
                    fk_model = apps.get_app_config("angled").get_model(fk_parts[0])
                else:
                    fk_model = apps.get_app_config(fk_parts[0]).get_model(fk_parts[1])
                fk_fields[f[1]] = fk_model

            for k, v in validated_data.items():
                if k in term_fields:
                    validated_data[k] = ref.Term.objects.get(pk=v)
                if k in fk_fields:
                    fk_model = fk_fields[k]
                    validated_data[k] = fk_model.objects.get(pk=v)

            instance = dyn.get_unit_model(unit["name"]).objects.create(
                project=self.context["transaction"].project,
                user=self.context["transaction"].user,
                transaction=self.context["transaction"],
                unit_status=dyn.BaseUnit.STATUS_CURRENT,
                **validated_data
            )

            return instance

    UNIT_SERIALIZERS[name] = type(
        "FieldedUnitSerializer", (UnitSerializer,), unit_serializer_fields
    )


FAST_UNIT_SERIALIZERS = {}


def _get_fast_serializer_impl(name, unit):
    """A serializer factory that tries to mimick DRF without using it
    because of serious performance issues.
    It's intended use case is in serializing the complete list of projects.
    """
    global FAST_UNIT_SERIALIZERS

    import angled.serializers.fast_fields as ff

    FastFieldMap = {
        "fk": ff.UnitIntegerField,
        "term": ff.TermField,
        "label": ff.MessageField,
        "text": ff.MessageField,
        "raw_text": ff.UnitCharField,
        "varchar": ff.UnitCharField,
        "number": ff.UnitIntegerField,
        "decimal": ff.UnitDecimalField,
        "month": ff.MonthField,
        "year": ff.YearField,
        "date": ff.UnitDateField,
        "boolean": ff.UnitBooleanField,
        "geometry": ff.UnitGeometryField,
    }

    tagged_name = "tagged_{}".format(name)
    unit_fields = unit["fields"]
    cached_fields = {}

    for f in unit_fields:
        field_type = f[0]
        field_name = f[1]
        cached_fields[field_name] = FastFieldMap[field_type](field_name)

    class FastUnitSerializer:
        def __init__(self, *args, **kwargs):
            self._input_data = kwargs.pop("data")
            self._data = None
            self.many = kwargs.get("many", False)
            context = kwargs.get("context", None)
            self.request = context.get("request", None)
            self.readings = context.get("readings", None)
            assert self.request is not None
            assert self.readings is not None

        def is_valid(self):
            return True

        @property
        def data(self):
            if self._data is None:
                if self.many is True:
                    self._data = [
                        self.to_representation(data) for data in self._input_data
                    ]
                else:
                    self._data = self.to_representation(self._input_data)

            return self._data

        def get_user(self):
            try:
                return self.request.user
            except Exception:
                raise UnitSerializerError("Should have a request object on context")

        def get_readings(self):
            try:
                return self.readings
            except Exception:
                raise UnitSerializerError("Should have a readings object on context")

        def to_representation(self, instance):
            user = self.get_user()
            readings = self.get_readings()

            ret = get_base(user, readings, instance, name)
            fields = cached_fields.values()

            for field in fields:
                try:
                    attribute = field.get_attribute(instance)
                except serializers.SkipField:
                    continue

                ret[field.field_name] = field.to_representation(
                    attribute, ret.get("visible", False)
                )

            return ret

        def get_tags(self, obj):
            return [t.id for t in getattr(obj, tagged_name).all()]

    FAST_UNIT_SERIALIZERS[name] = FastUnitSerializer


def make_unit_serializer(unit):
    name = unit["name"]
    if name not in UNIT_SERIALIZERS:
        _make_unit_serializer_impl(name, unit)

    return UNIT_SERIALIZERS[name]


def get_fast_serializer(unit):
    name = unit["name"]
    if name not in FAST_UNIT_SERIALIZERS:
        _get_fast_serializer_impl(name, unit)

    return FAST_UNIT_SERIALIZERS[name]


def make_tag_serializer(unit):
    class T(serializers.ModelSerializer):
        class Meta:
            model = dyn.get_tag_model(unit["name"])
            fields = ["id", "tag", "unit"]

    return T
