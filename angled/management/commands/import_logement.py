import datetime
from django.utils.text import slugify
from django.contrib.auth.models import User
from angled.models import ref
from angled.models import dyn
from collections import namedtuple

from angled.management.import_base import (ImportCommand, ReaderType,
                                           UnitError, UnitResult, UnitSkip,
                                           MultiResult, get_cell_name, get_val,
                                           get_nova, get_contact, parse_int,
                                           parse_float, parse_money,map_multiple,
                                           parse_multiple_int)

import logging

logger = logging.getLogger('import')

POSITIONS = {
    'RbL/15/003': [151772.71, 172422.49],  # Phase 1 SLRB
    'RbL/15/004': [151762.3, 172428.99],  # Phase 1 Citydev
    'RbL/15/005': [151758.39, 172415.98],  # Phase 1 Fonds du Logement
    'RbL/15/023': [152225.55, 172756.91],  # Phase 3 SLRB
    'RbL/15/006': [152001.73, 172656.71],  # Phase 2A SLRB
    'RbL/15/007': [152009.54, 172669.73],  # Phase 2A Citydev
    'RbL/15/024': [152215.14, 172759.51],  # Phase 3 Citydev
    'RbL/15/008': [152016.04, 172659.32],  # Phase 2A FdL
    'RbL/15/025': [152124.05, 172907.86],  # Phase 2B FdL
    'RbL/19/001': [156829.09, 168365.57],  # Dames Blanches
    'RbL/01/006': [146633.05, 169006.37],  # Petite-?le
    'RbL/18/007': [154172.5, 171607.18],  # Val d'Or
    'RbL/01/025': [142719.79, 167488.95],  # Erasmus
    'RbL/04/031': [148156.41, 174282.88],  # Steyls Hippodrome
    'RbL/01/027': [144777.58, 169830.58],  # Prins
    'RbL/02/001': [153573.0, 166876.64],  # Demey - Houlette III
    'RbL/06/005': [153020.43, 173590.92],  # Renoir
    'RbL/04/020': [146766.97, 176153.33],  # Cit? Mod?le 18-22
    'RbL/01/013': [144285.26, 167798.27],  # Novacity - Lot 2/3
    'RbL/01/010': [146623.22, 169434.97],  # Citygate 3
    'RbL/07/011': [146189.19, 166124.62],  # Verrerie
    'RbL/04/030': [151943.31, 176329.88],  # Chemin Vert
    'RbL/06/004': [152846.67, 172004.16],  # Destrier
    'RbL/04/015': [148994.69, 173548.28],  # Tivoli Green City
    'RbL/12/028': [147584.39, 171124.23],  # Delaunoy 58-64
    'RbL/01/009': [146626.75, 169084.49],  # Citygate 2
    'RbL/04/024': [147752.67, 170871.17],  # Porte de Ninove
    'RbL/01/008': [146842.94, 169064.27],  # Citygate 1 - Marchandises
    'RbL/01/022': [146867.04, 169125.04],  # Citygate 1 - Kuborn
    'RbL/07/004': [146491.94, 166804.79],  # Barcelone
    'RbL/18/003': [154705.05, 170952.62],  # Brel
    'RbL/12/014': [146079.24, 171756.46],  # BAT-Lavoisier
    'RbL/04/012': [153012.27, 175136.37],  # Verdun
    'RbL/12/017': [146663.31, 171982.91],  # Schols
    'RbL/01/012': [146412.72, 169189.2],  # Citydocks
    'RbL/04/022': [151430.48, 175847.76],  # Faines
    'RbL/08/001': [146143.46, 173518.13],  # Van Overbeke
    'RbL/06/003': [153258.33, 172492.92],  # Artemis
    'RbL/01/017': [146251.82, 170075.23],  # Birmingham -  Malherbe - Ensor
    'RbL/06/006': [153773.86, 172624.77],  # Lauriers roses
    'RbL/16/004': [147956.71, 163446.29],  # Homborchveld
    'RbL/12/026': [147006.55, 171068.46],  # Site Cinoco
    'RbL/12/023': [147029.56, 170846.5],  # Birmingham
    'RbL/15/010': [149720.85, 172837.83],  # Destouvelles - Reine
    'RbL/07/001': [146711.92, 167953.66],  # Luttre
    'RbL/16/008': [147730.93, 165668.68],  # Alsemberg
    'RbL/01/014': [144774.83, 167239.11],  # Citycampus (ex-Gryson)
    'RbL/01/039': [144246.3, 167816.38],  # Novacity - Lot 1
    'RbL/17/008': [153338.86, 166283.17],  # Cailles Logis
    'RbL/01/002': [147332.87, 170369.42],  # Compas
    'RbL/04/010': [153429.23, 174801.63],  # Grenouillette
    'RbL/01/033': [143921.64, 169696.22],  # Dupuis
    'RbL/04/005': [150275.72, 173443.85],  # BridgeCity (Navez)
    'RbL/01/019': [142719.79, 167488.95],  # Erasme
    'RbL/12/007': [147894.64, 172360.34],  # Sucrerie
    'RbL/03/004': [144630.05, 171769.95],  # Dormont
    'RbL/09/002': [151249.26, 167894.13],  # G?n?ral Jacques
    'RbL/01/003': [147187.61, 170434.85],  # CRU 5 Heyvaert-Poincar?
    'RbL/01/016': [147332.87, 170369.42],  # Compas
    'RbL/01/029': [144879.27, 170121.53],  # Peterbos Bois
    'RbL/07/005': [146047.95, 166321.3],  # Huileries
    'RbL/17/003': [153355.86, 166545.12],  # Archiducs Sud
    'RbL/07/019': [146650.96, 166183.15],  # Bambou
    'RbL/12/018': [145583.42, 171344.63],  # Condor
    'RbL/04/019': [148536.22, 173953.58],  # Vandergoten
    'RbL/17/006': [153527.0, 166659.22],  # Tritomas
    'RbL/04/029': [148433.6, 170129.35],  # Roue
    'RbL/07/020': [146180.08, 166465.78],  # Rue du Dries
    'RbL/03/006': [144004.34, 173141.98],  # Hunderenveld
    'RbL/06/007': [152068.5, 174323.68],  # Picardie
    'RbL/18/005': [155613.46, 170934.49],  # Chapelle aux Champs
    'RbL/01/037': [147432.7, 170705.44],  # Heyvaert
    'RbL/18/001': [153490.24, 171543.43],  # Pl?iades
    'RbL/01/020': [144900.22, 170110.57],  # Groeninckx-De May
    'RbL/16/003': [149638.93, 164514.25],  # Avijl
    'RbL/04/023': [151126.99, 176667.75],  # Val Maria
    'RbL/12/013': [146046.09, 172111.62],  # M-Square
    'RbL/15/011': [151332.21, 173237.24],  # CSA
    'RbL/12/021': [144668.3, 171566.44],  # Vieillesse Heureuse/Gronkel
    'RbL/18/002': [155671.84, 170821.43],  # Vandervelde
    'RbL/12/002': [147092.5, 170524.74],  # Nautica
    'RbL/12/009': [147017.17, 171772.59],  # Vandepeereboom - De Gunst
    'RbL/04/032': [150868.14, 175592.64],  # Biebuyck
    'RbL/15/019': [151191.48, 174320.93],  # Zenobe Gramme
    'RbL/01/034': [146779.15, 169884.33],  # Albert 1er
    'RbL/04/007': [150265.64, 173406.05],  # Navez
    'RbL/17/004': [153364.95, 166665.13],  # Archiducs Nord/Gerfauts
    'RbL/07/018': [146863.01, 167421.13],  # Delta (Les Sources
    'RbL/07/017': [146616.26, 167019.48],  # Bervoets
    'RbL/07/006': [146835.53, 166326.41],  # Grappe
    'RbL/16/005': [147257.83, 164496.84],  # Vervloet
    'RbL/01/007': [146773.38, 169245.96],  # Citygate 1  - Goujons
    # 'RbL/01/023': [143781.04, 169385.16],  # Itterbeek
    'RbL/01/026': [144765.1, 167225.35],  # Rauter
    'RbL/07/014': [146967.52, 168082.74],  # Ducuroir
    'RbL/12/008': [146997.56, 171667.13],  # Vandepeereboom
    'RbL/12/006': [146549.9, 170938.14],  # EKLA
    'RbL/12/016': [145671.91, 171787.6],  # Lemaire
    'RbL/12/029': [147328.22, 170514.85],  # Liverpool
    'RbL/03/007': [144511.37, 172586.72],  # Fleuristes
    'RbL/06/001': [153093.84, 171484.1],  # Grosjean (B-House)
    'RbL/12/001': [147073.04, 170462.37],  # CRU 5 Heyvaert-Poincar?
    'RbL/13/005': [147713.68, 169267.39],  # Midi A1
    'RbL/18/008': [156222.28, 170459.72],  # Dumont
    'RbL/18/009': [153774.08, 171095.75],  # Charrette
    'RbL/19/002': [154122.16, 169677.28],  # Gay
    'RbL/12/022': [147077.91, 171496.17],  # Campine
    'RbL/04/027': [148179.87, 170471.14],  # Verdure/Potiers
    'RbL/01/011': [145835.15, 168322.27],  # Nautilus
    'RbL/03/008': [144236.94, 173059.13],  # Azur
    'RbL/06/002': [153020.43, 173590.92],  # Leger
    'RbL/12/015': [145589.42, 171737.08],  # Candries
    'RbL/17/001': [153292.85, 166345.35],  # Cailles
    'RbL/03/002': [144970.06, 172681.71],  # Place Schweizer
    'RbL/11/001': [147072.98, 171899.51],  # Neep
    'RbL/17/005': [153296.88, 166145.11],  # Tritons - Nymphes
    'RbL/09/006': [151822.9, 167042.65],  # Volta III
    'RbL/04/016': [148885.94, 173495.5],  # Tivoli Green City
    'RbL/04/021': [146153.53, 175896.5],  # Cit? Mod?le Bois
    'RbL/01/049': [147183.48, 170092.75],  # Lidl
    'RbL/03/005': [145002.86, 172846.33],  # Openveld
    'RbL/12/024': [147445.59, 171306.61],  # Delaunoy 69
    'RbL/12/027': [145885.07, 171914.48],  # B?guines
    'RbL/01/024': [143320.77, 167268.45],  # Lennik Nord
    'RbL/01/032': [146620.78, 169759.54],  # Transvaal
    'RbL/09/007': [149411.13, 167662.7],  # Waterloo
    'RbL/07/009': [147368.07, 168452.63],  # Fierlant
    'RbL/01/023': [144111.92, 168595.74],  # La Braise
    'RbL/04/041': [148558.03, 174104.32],  # CQD Bockstael
    'RbL/09/004': [149600.14, 169653.89],  # Wavre
    'RbL/09/005': [150484.9, 169854.4],  # Wiertz
    'RbL/15/017': [150710.42, 173378.84],  # Waelhem
    'RbL/01/043': [146321.54, 169434.85],  # CQD Biestebroeck
    'RbL/04/001': [149099.52, 172326.3],  # Anvers
    'RbL/04/028': [148270.73, 169323.43],  # Philanthropie
    'RbL/04/036': [148695.48, 169800.23],  # CQD Marolles
    'RbL/09/014': [150625.75, 169102.72],  # CQD Maelbeek
    'RbL/10/001': [146594.95, 174184.25],  # Saule
    'RbL/10/003': [148227.55, 169012.98],  # Vandenschrieck
    'RbL/16/002': [147137.81, 163750.22],  # Silence
    'RbL/01/001': [147562.7, 170648.12],  # CRU 5 Heyvaert-Poincar?
    'RbL/13/002': [149879.66, 167248.02],  # CQD Parvis Morichar
    'RbL/16/001': [147454.36, 164335.43],  # St Job
    'RbL/01/028': [147177.29, 169973.22],  # Rapha?l
    'RbL/01/040': [145512.9, 170001.17],  # D?mosth?ne
    'RbL/01/036': [147187.61, 170434.85],  # Bougie
    'RbL/07/007': [148107.3, 167850.92],  # Albert
    'RbL/07/012': [147282.71, 168720.24],  # Belgrade
    'RbL/07/013': [146929.57, 167606.91],  # Van Volxem - CRV
    'RbL/07/022': [146183.9, 166239.73],  # CQD Abbaye
    'RbL/13/001': [147638.36, 169109.2],  # Danemark
    'RbL/18/004': [153983.45, 170594.46],  # Activit?
    'RbL/19/003': [156311.81, 168125.3],  # Or?e
    'RbL/07/008': [146473.25, 167127.19],  # St Denis
    'RbL/13/004': [147512.48, 168885.72],  # Rue de M?rode
    'RbL/01/030': [145786.75, 168271.65],  # Industriel Van Kalken (Nautilus)
    'RbL/05/002': [151002.92, 169428.62],  # CQD Chasse-Gray
    'RbL/08/002': [146143.46, 173518.13],  # Commissariat
    'RbL/13/003': [148563.59, 168888.15],  # Rue H?tel des Monnaies
    'RbL/15/022': [150641.12, 172848.28],  # CQD Pogge
    'RbL/16/007': [146813.54, 164324.82],  # Ugeux
    'RbL/01/018': [145671.97, 169622.86],  # Delcourt Aumale
    'RbL/01/041': [147635.71, 170626.15],  # Abb? Cuylits
    'RbL/05/001': [151875.58, 169625.52],  # Hap
    'RbL/07/016': [146825.22, 168170.31],  # CQD Wiels
    'RbL/15/013': [151476.15, 173841.26],  # Corbeau
    'RbL/01/045': [146136.38, 169526.52],  # CQD Biestebroeck
    'RbL/02/002': [153666.44, 167750.0],  # Kouter
    'RbL/03/003': [144276.29, 172057.76],  # Cognassier
    'RbL/05/003': [150799.66, 169361.55],  # CQD Chasse-Gray
    'RbL/09/009': [149833.88, 169383.2],  # CQD Ath?n?e
    'RbL/12/032': [147329.87, 170798.69],  # CQD Petite Senne
    'RbL/15/012': [152044.16, 172865.93],  # Gilisquet
    'RbL/15/014': [151621.58, 173269.04],  # Marbotin
    'RbL/01/031': [146230.36, 170549.11],  # Verheyden
    'RbL/12/035': [147349.44, 170790.7],  # CQD Petite Senne
    'RbL/16/006': [147586.05, 166114.05],  # Asselbergs
    'RbL/04/026': [148264.02, 170498.82],  # Soignies
    'RbL/05/005': [151370.51, 169262.04],  # CQD Chasse-Gray
    'RbL/07/010': [146947.24, 167659.31],  # Van Volxem
    'RbL/09/012': [150625.75, 169102.72],  # CQD Maelbeek
    'RbL/10/008': [147564.58, 174050.96],  # CQD Magritte
    'RbL/10/010': [147840.42, 174078.58],  # CQD Magritte
    'RbL/15/016': [149796.09, 172658.03],  # Liedts
    'RbL/04/040': [148741.29, 169982.18],  # CQD Jonction
    'RbL/10/006': [147754.48, 174298.73],  # CQD Magritte
    'RbL/13/006': [148274.61, 168875.81],  # CQD Parvis Morichar
    'RbL/15/021': [151478.79, 173301.49],  # CQD Pogge
    'RbL/01/048': [147524.34, 170435.63],  # CQD Compas
    'RbL/04/035': [148695.48, 169800.23],  # CQD Marolles
    'RbL/05/006': [151392.7, 169217.66],  # CQD Chasse-Gray
    'RbL/09/011': [150243.66, 169419.16],  # CQD Ath?n?e
    'RbL/09/013': [150690.86, 169165.95],  # CQD Maelbeek
    'RbL/10/002': [147232.92, 173069.31],  # Moranville II
    'RbL/12/033': [147367.1, 170670.8],  # CQD Petite Senne
    'RbL/12/034': [147308.73, 170698.83],  # CQD Petite Senne
    'RbL/09/010': [149983.29, 169680.28],  # CQD Ath?n?e
    'RbL/10/009': [147564.58, 174050.96],  # CQD Magritte
    'RbL/14/001': [150160.35, 171053.33],  # CQD Axe louvain
    'RbL/05/004': [151096.67, 169325.17],  # CQD Chasse-Gray
    'RbL/09/015': [150493.61, 169407.53],  # CQD Maelbeek
    'RbL/15/015': [150197.97, 173436.37],  # Jacquet
    'RbL/01/044': [146187.42, 169534.71],  # CQD Biestebroeck
    'RbL/01/047': [147416.97, 170565.92],  # CQD Compas
    'RbL/01/015': [144782.37, 167250.58],  # Citycampus
    'Rbl/04/037': [148695.48, 169800.23],  # CQD Marolles
    'RbL/04/038': [148446.82, 169772.29],  # CQD Marolles
    'RbL/10/005': [146928.21, 173929.77],  # Dewez
    'RbL/13/007': [148243.1, 168583.57],  # CQD Parvis Morichar
    'RbL/15/020': [152774.16, 171630.46],  # Evenepoel
    'Rbl/03/009': [145390.72, 172924.04],  # Hpital franais - Clos Bourgeois
    'Rbl/04/017': [148807.65, 173442.02],  # Tivoli Green City
    'RbL/01/021': [144246.09, 167890.76],  # Trèfles I
    'RbL/01/050': [144297.49, 167953.54],  # Trèfles II
    'RbL/01/051': [144351.82, 167821.14],  # Trèfles III
    'RbL/09/001': [151306.74, 167922.49],  # Logements familiaux intra
    'RbL/10/007': [147620.38, 174324.8],  # CQD Magritte
    'RbL/11/002': [147661.84, 172265.96],  # Montagne aux Anges
    'Rbl/11/003': [147433.42, 171891.39],  # Schmitz - Piers
    'RbL/12/005': [146770.84, 171108.1],  # None
    'RbL/12/036': [146578.39, 170872.04],  # Emaillerie
    'RbL/12/039': [148062.36, 173318.7],  # Dubrucq - Escaut
    'Rbl/12/041': [147938.0, 171550.35],  # Vandermaelen
    'RbL/15/001': [152316.51, 171397.44],  # MediaPark
    'RbL/15/002': [152316.51, 171397.44],  # MediaPark
    'RbL/17/007': [154156.21, 170795.78],  # Dries
    'RbL/17/004b': [153342.18, 166733.99],  # Archiducs Nord/Gerfauts
    'RbL/04/039': [148694.55, 171526.88],  # Pacheco
    'RbL/01/042': [144333.05, 168013.47],  # Trfles 2
}

UNITS = (
    ('rbl', 'Code RbL'),
    ('', 'Zone stratégique'),
    ('zone', 'Zone'),
    ('', 'Code commune INS'),
    ('post_code', 'Code Postal'),
    ('city', 'Commune'),
    ('', 'Code Quartier'),
    ('', 'Quartier (monitoring)'),
    ('', 'Code Secteur Statistique'),
    ('', 'Secteur statistique'),
    ('projet', 'Projets'),
    ('sous_proj', 'Sous-projets'),
    ('prl_ah', 'PRL/AH'),
    ('street_name', 'Rue/Avenue Clos'),
    ('street_number', 'Numéro'),
    ('', 'Adresse'),
    ('new_adress', 'Nvl Adresse'),
    ('', 'Parcelle'),
    ('proprietaire', 'Propriétaire'),
    ('operateur', 'Opérateur public'),
    ('construction_type', 'Type'),
    ('by_date_2020', '< 2020'),
    ('by_date_2020-2024', '2020 < x < 2025'),
    ('by_date_2024', '> 2024'),
    ('by_ls', 'LS'),
    ('by_lmod', 'Lmod'),
    ('by_lmoy', 'Lmoy'),
    ('is_locatif', 'Locatif'),
    ('is_acquisitif', 'Acquisitif'),
    ('total_logements', 'Total Logement'),
    ('area_over', 'Hors Sol'),
    ('area_under', 'Sous-Sol'),
    ('parking', 'Parkings'),
    ('equipments', 'Equipements joints'),
    ('equipment_quantity', 'Surface équipements joints'),
    ('step', 'Etape de la procédure'),
    ('', 'Date_GRBC'),
    ('', 'Date_Avis_Marché'),
    ('', 'Date_marché_Service'),
    ('nova', 'Code NOVA'),
    ('', 'Date dépôt PU/PE'),
    ('', 'Dossier incomplet'),
    ('', 'Date complétude'),
    ('', 'Date CC'),
    ('', 'Date délivrance PU'),
    ('', 'Date DBA_CA'),
    ('cost', 'Coût HTVA'),
    ('', 'Coût/logement'),
    ('', 'coût/m²'),
    ('studio', 'Studio'),
    ('1ch', '1Ch'),
    ('2ch', '2Ch'),
    ('3ch', '3Ch'),
    ('4ch+', '4Ch et +'),
    ('pmr', 'PMR'),
    ('remarques', 'Remarques'),
)

CELL_NAMES = [(cell, model) for model, cell in UNITS]

cell_name = get_cell_name(UNITS)
val = get_val(UNITS)
map_units = map_multiple(UNITS)


def programme(row):
    v = val(row, 'program')
    if 0 == len(v.strip()):
        return UnitSkip()

    try:
        term = dyn.DomainReg.term('common', v)
        return UnitResult(program=term)
    except Exception:
        return UnitError(cell_name('program'),
                         'Could not create a term for "{}"'.format(v))


def parking_space(row):
    v = val(row, 'parking_space').strip()
    if 0 == len(v):
        return UnitSkip()
    parkings = parse_int(v)
    if parkings == 0:
        return UnitSkip()
    return UnitResult(value=parkings)


def housing_area(row):
    v = val(row, 'housing_area').strip()
    surface = parse_int(v)
    if surface == 0:
        return UnitSkip()
    return UnitResult(value=surface)


cost_general = ref.Term.objects.get(pk=174)


def cost(row):
    v = val(row, 'cost')
    c = parse_money(v.strip())
    if len(v) > 0 and c == 0.0:
        return UnitError(cell_name('cost'),
        """Error parsing cost "{}"
        """.format(v))
    if c == 0.0:
        return UnitSkip()
    return MultiResult(UnitResult(type=cost_general, cost=c))


def check(a, g, n=1):
    if len(a) <= n:
        return g
    if len(a) > n and g:
        raise Exception('oops')
    return True


HousingRecord = namedtuple('HR', ['type', 'quantity'])


def fst(a, default=None):
    try:
        return a[0]
    except IndexError:
        return default


unknown_type = HousingRecord(ref.Term.objects.get(pk=335), 0)
unknown_rent = HousingRecord(ref.Term.objects.get(pk=334), 0)
unknown_compo = HousingRecord(ref.Term.objects.get(pk=333), 0)


def make_housings_per_type(release, types, rent):
    result = []
    for rec in types:
        result.append(
            UnitResult(
                planned_release=release,
                quantity=rec.quantity,
                disposal=rec.type,
                rental_level=rent.type,
            ))
    return MultiResult(result)


def make_housings_per_rent(release, type, rents):
    result = []
    for rec in rents:
        result.append(
            UnitResult(
                planned_release=release,
                quantity=rec.quantity,
                disposal=type.type,
                rental_level=rec.type,
            ))
    return MultiResult(result)



def make_housings(total, release, types, rents):
    try:
        if len(types) > 1:
            return make_housings_per_type(release, types,
                                          fst(rents, unknown_rent))
        elif len(rents) > 1:
            return make_housings_per_rent(release, fst(types, unknown_type),
                                          rents)


        return MultiResult(
            UnitResult(
                planned_release=release,
                quantity=total,
                disposal=fst(types, unknown_type).type,
                rental_level=fst(rents, unknown_rent).type,
            ))
    except Exception as ex:
        return UnitError('housing', 'Failed to build a housing description: {}'.format(ex))


def check_housing(releases, types, rents):

    if len(releases) == 0:
        raise Exception('We need a release date')
    if len(releases) > 1:
        raise Exception('We can parse only one release per line')


    try:
        guard = check(types, False)
        guard = check(rents, guard)
    except Exception:
        raise Exception(
            'There is too many of types or rents to make sense of it'
        )


def legislature(date):
    if date == 'by_date_2020':
        return ref.Term.objects.get(pk=316)
    elif date == 'by_date_2020-2024':
        return ref.Term.objects.get(pk=317)
    else:
        return ref.Term.objects.get(pk=318)


def housings(row):
    declared_total = parse_int(val(row, 'total_logements'))
    if declared_total == 0:
        return UnitSkip()

    releases = list(filter(lambda x: x.quantity > 0, map_units(row,
                         ['by_date_2020', 'by_date_2020-2024', 'by_date_2024'],
                         lambda x: HousingRecord(legislature(x[0]), parse_int(x[1])))))

    types = list(
        filter(
            lambda x: x.quantity > 0,
            map_units(
                row, ['is_locatif', 'is_acquisitif'], lambda x: HousingRecord(
                    dyn.DomainReg.term('Typologie du logement', x[0]),
                    parse_int(x[1])))))

    rents = list(
        filter(
            lambda x: x.quantity > 0,
            map_units(
                row, ['by_ls', 'by_lmod', 'by_lmoy'], lambda x: HousingRecord(
                    dyn.DomainReg.term('Type de logement public', x[0]),
                    parse_int(x[1])))))

    # compos = list(
    #     filter(
    #         lambda x: x.quantity > 0,
    #         map_units(
    #             row, ['studio', '1ch', '2ch', '3ch', '4ch+'],
    #             lambda x: HousingRecord(
    #                 dyn.DomainReg.term('Composition du logement', x[0]),
    #                 parse_int(x[1])))))

    # pmr = parse_int(val(row, 'pmr'))

    try:
        check_housing(releases, types, rents)
    except Exception as ex:
        return UnitError('housing', str(ex))

    return make_housings(declared_total, fst(releases).type, types, rents)

def housing_info(row):
    studio = parse_int(val(row, 'studio'))
    one = parse_int(val(row, '1ch'))
    two = parse_int(val(row, '2ch'))
    three = parse_int(val(row, '3ch'))
    four_more = parse_int(val(row, '4ch+'))
    pmr = parse_int(val(row, 'pmr'))

    return UnitResult(
        studio=studio,
        two=two,
        three=three,
        one=one,
        four_more=four_more,
        pmr=pmr,
    )


def prl_ah(row):
    v = val(row, 'prl_ah')
    if 0 == len(v) or v == '-':
        return UnitSkip()
    else:
        try:
            return MultiResult([
                UnitResult(
                    name=dyn.DomainReg.term('Programme de financement', v))
            ])
        except Exception:
            return UnitError(
                cell_name('prl_ah'),
                'Could not determine a funding term from "{}"'.format(v))


def construction_type(row):
    v = val(row, 'construction_type')
    if 0 == len(v):
        return UnitSkip()
    else:
        types = [t.strip() for t in v.split('+')]
        try:
            return MultiResult([
                UnitResult(construction_type=dyn.DomainReg.term(
                    'Type d\'opération', name)) for name in types
            ])
        except Exception:
            return UnitError(cell_name('construction_type'),
                             'Could not make a term of "{}"'.format(v))


def zone(row):
    v = val(row, 'zone')
    if 0 == len(v) or v == 'à définir':
        return UnitSkip()
    else:
        return UnitResult(
            name=dyn.DomainReg.term('Nom de la zone stratégique', v))


def projet(row):
    name = val(row, 'projet')
    return UnitResult(name=dict(fr=name, nl=name))


def nova(row):
    v = val(row, 'nova')
    if 0 == len(v):
        return UnitSkip()
    else:
        return MultiResult([UnitResult(nova=v)])


def proprietaire(row):
    v = val(row, 'proprietaire')
    if 0 == len(v):
        return UnitSkip()
    else:
        contact = get_contact(v)
        ty = ref.Term.objects.get(pk=124)  # Proprietaire
        return UnitResult(contact=contact, type=ty)


def operator(row):
    v = val(row, 'operateur')
    if 0 == len(v):
        return UnitSkip()
    else:
        contact = get_contact(v)
        ty = ref.Term.objects.get(pk=304)  # maiter d'ouvrage
        return UnitResult(contact=contact, type=ty)


def equipment(row):
    results = []

    try:
        p = int(val(row, 'parking'))
        results.append(
            UnitResult(
                aux_equipment=ref.Term.objects.get(pk=135),
                amount=p,
                unite=ref.Term.objects.get(pk=257),
            ))
    except ValueError:
        pass

    e = [x.strip() for x in val(row, 'equipments').split('+')]
    q = []
    for x in val(row, 'equipment_quantity').split('+'):
        if len(x) > 0:
            try:
                q.append(int(x))
            except Exception:
                return UnitSkip(), UnitError(
                    cell_name('equipment_quantity'),
                    'Could not extract a quantity from "{}"'.format(x))

    if len(e) != len(q):
        q += [0 for _ in range(len(e) - len(q))]

    equipments = zip(e, q)

    errored = []
    for e, q in equipments:
        if len(e) == 0:
            continue
        try:
            t = dyn.DomainReg.term('Equipement accessoire aux logements', e)
            results.append(
                UnitResult(
                    amount=q,
                    aux_equipment=t,
                    unite=ref.Term.objects.get(pk=256),
                ))
        except Exception:
            errored.append(e)

    error = UnitError(
        cell_name('equipments'),
        'Failed to get terms for {}'.format(errored)) if len(errored) else None
    if len(results):
        return MultiResult(results), error

    return UnitSkip(), error


note_type = ref.Term.objects.get(pk=49)


def remarque(row):
    rem = val(row, 'remarques')
    if len(rem) == 0:
        return UnitSkip()
    return MultiResult([UnitResult(type=note_type, body=rem)])


def point(row):
    rbl = val(row, 'rbl')
    if rbl in POSITIONS:
        coord = POSITIONS[rbl]
        return UnitResult(
            geom='SRID=31370;MULTIPOINT (({} {}))'.format(*coord))
    return UnitSkip()


manager_type = ref.Term.objects.get(pk=325)
yves = User.objects.get(username='yvandecasteele')


def manager(row):
    return MultiResult(UnitResult(context=manager_type, manager_user=yves))


def step(row):
    name = val(row, 'step')
    if len(name) == 0:
        return UnitSkip()

    try:
        return UnitResult(step=dyn.DomainReg.term('Etapes d\'avancement du projet', name))
    except Exception:
        return UnitError(cell_name('step'), 'Failed to get a term for "{}"'.format(name))


type_over = ref.Term.objects.get(pk=200)
type_under = ref.Term.objects.get(pk=201)


def area(row):
    over = parse_int(val(row, 'area_over'))
    under = parse_int(val(row, 'area_under'))
    results = []
    if over > 0:
        results.append(UnitResult(type=type_over, value=over))
    if under > 0:
        results.append(UnitResult(type=type_under, value=under))

    if len(results) > 0:
        return MultiResult(results)

    return UnitSkip()


pt = ref.Term.objects.get(pk=198)


def project_type():
    return MultiResult(UnitResult(type=pt))


prog_comp = ref.Term.objects.get(pk=236)


def program():
    return MultiResult(UnitResult(component=prog_comp))


def address(row):
    pc = val(row, 'post_code')
    c = val(row, 'city')
    sna = val(row, 'street_name')
    snu = val(row, 'street_number')
    na = val(row, 'new_adress')
    text_fr = []
    if len(na):
        text_fr.append(na)
        text_fr.append('{} {}'.format(pc, c))
    else:
        text_fr.append('{} {}'.format(sna, snu))
        text_fr.append('{} {}'.format(pc, c))

    fr = '\n'.join(text_fr)
    return UnitResult(address=dict(fr=fr, nl=''))


def description(row):
    d = val(row, 'sous_proj')
    if len(d) > 0:
        return UnitResult(body=dict(fr=d, nl=''))

    return UnitSkip()

class Command(ImportCommand):
    reader_type = ReaderType.CSV

    def process_row(self, row, create_unit):
        create_unit('housing_batch', housings(row))
        create_unit('housing_info', housing_info(row))
        create_unit('program_name', prl_ah(row))
        create_unit('construction_type', construction_type(row))
        create_unit('zone', zone(row))
        create_unit('name', projet(row))
        create_unit('nova', nova(row))
        create_unit('actor', MultiResult([proprietaire(row), operator(row)]))
        create_unit('point', point(row))
        create_unit('note', remarque(row))
        create_unit('description', description(row))
        create_unit('manager', manager(row))
        create_unit('cost', cost(row))
        create_unit('step', step(row))
        create_unit('area', area(row))
        create_unit('project_type', project_type())
        create_unit('program', program())
        create_unit('address', address(row))
        equipments, eq_error = equipment(row)
        create_unit('auxiliary_equipments', equipments)
        if eq_error is not None:
            create_unit('auxiliary_equipments', eq_error)
