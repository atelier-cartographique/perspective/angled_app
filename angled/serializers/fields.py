# from django.contrib.postgres.fields import ranges
from datetime import datetime, date
from decimal import Decimal
import json
import time
from django.db import models
from rest_framework import serializers
from rest_framework_gis.serializers import GeometryField
from psycopg2.extras import Range, DateRange

from lingua.fields import LinguaRecord
from angled.models import ref
from angled.serializers._utils import year_to_range, month_to_range


class JSONField(serializers.JSONField):
    """
    FIXME
    It happens that somewhere, my array is turned into a string
    So this is a quick fix to get my array back
    """

    def to_representation(self, value):
        if isinstance(value, str):
            return json.loads(value)
        return super().to_representation(value)


class TagField(serializers.Field):
    def to_representation(self, value):
        return value

    def to_internal_value(self, data):
        return data


class UnitField(serializers.Field):
    """ A base class for unit fields
    """

    def to_representation(self, value, visible):
        if visible:
            return self.to_visible_representation(value)
        return self.to_obfuscated_representation(value)

    def to_visible_representation(self, value):
        raise NotImplementedError()

    def to_obfuscated_representation(self, value):
        raise NotImplementedError()


def isrange(data):
    return isinstance(data, Range)


class YearField(UnitField):
    field_name = 'YearField'

    def to_internal_value(self, data):
        if isinstance(data, DateRange):
            return year_to_range(data.lower.year)
        return year_to_range(data)

    def to_visible_representation(self, value):
        low = value.lower

        return low.year

    def to_obfuscated_representation(self, value):
        return 0


class MonthField(UnitField):
    field_name = 'MonthField'

    def to_internal_value(self, data):
        if isinstance(data, DateRange):
            year = data.lower.year
            month = data.lower.month
        else:
            year, month = list(map(int, data.split('-')))

        return month_to_range(year, month)

    def to_visible_representation(self, value):
        low = value.lower

        return '{}-{:02d}'.format(low.year, low.month)

    def to_obfuscated_representation(self, value):
        return '0000-00'


class MessageField(UnitField, serializers.JSONField):
    def to_visible_representation(self, value):
        if type(value) is str:
            return json.loads(value)
        elif isinstance(value, LinguaRecord):
            return value.to_dict()

        return serializers.JSONField.to_representation(self, value)

    def to_obfuscated_representation(self, value):
        return {'fr': '', 'nl': '', 'en': ''}


class TermField(UnitField, serializers.PrimaryKeyRelatedField):

    pk_field = serializers.IntegerField()

    def __init__(self):
        super().__init__(queryset=ref.Term.objects)

    def to_internal_value(self, data):
        if self.pk_field is not None:
            data = self.pk_field.to_internal_value(data)
        return data

    def to_visible_representation(self, value):
        if type(value) is int:
            return value
        return serializers.PrimaryKeyRelatedField.to_representation(
            self, value)

    def to_obfuscated_representation(self, value):
        return 0


class UnitCharField(UnitField, serializers.CharField):
    def to_visible_representation(self, value):
        return serializers.CharField.to_representation(self, value)

    def to_obfuscated_representation(self, value):
        return ''


class UnitIntegerField(UnitField, serializers.IntegerField):
    def to_visible_representation(self, value):
        # we'ge got foreign keys that use this serializer as well, so watch out
        if isinstance(value, models.Model):
            return serializers.IntegerField.to_representation(self, value.pk)
        return serializers.IntegerField.to_representation(self, value)

    def to_obfuscated_representation(self, value):
        return 0


class UnitDecimalField(UnitField, serializers.DecimalField):
    def __init__(self):
        super().__init__(16, 2, write_only=False)

    def to_visible_representation(self, value):
        return float(value)

    def to_obfuscated_representation(self, value):
        return 0.0


class UnitDateField(UnitField, serializers.DateField):
    def to_internal_value(self, data):
        if isinstance(data, int):
            return super().to_internal_value(
                datetime.fromtimestamp(data / 1000).date())
        elif isinstance(data, float):
            return super().to_internal_value(
                datetime.fromtimestamp(data).date())

        return super().to_internal_value(data)

    def to_visible_representation(self, value):
        data = value
        if type(data) == date:
            return data.isoformat()
        elif isinstance(data, (int, float)):
            return round(data * 1000)
        return data

    def to_obfuscated_representation(self, value):
        # return int(datetime.now().timestamp()) * 1000
        return '0000-00-00'


class UnitBooleanField(UnitField, serializers.BooleanField):
    def to_visible_representation(self, value):
        return serializers.BooleanField.to_representation(self, value)

    def to_obfuscated_representation(self, value):
        return False


class UnitGeometryField(UnitField, GeometryField):
    def to_visible_representation(self, value):
        return GeometryField.to_representation(self, value)

    def to_obfuscated_representation(self, value):
        initial = GeometryField.to_representation(self, value)
        return dict(type=initial['type'], coordinates=[], properties={})
