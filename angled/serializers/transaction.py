from math import floor
from angled.models import dyn
from angled.ui import UNITS_NAMES


def serialize_transaction(tx):
    if not isinstance(tx, dyn.Transaction):
        raise TypeError("Expected Transaction, got a {}".format(type(tx)))

    # the following has high potential for slowness,
    # let's see how it behaves before going for better options.
    # get = dyn.get_unit_model
    units = []
    for name in UNITS_NAMES:
        #     if get(name).objects.filter(transaction=tx).count() > 0:
        #         units.append(name)
        if getattr(tx, f"info_unit_{name}_set").count() > 0:
            units.append(name)

    return dict(
        id=tx.id,
        created_at=floor(tx.created_at.timestamp() * 1000),
        project=tx.project.id,
        user=tx.user.id,
        units=units,
    )


def serialize_transaction_qs(qs):
    return [serialize_transaction(tx) for tx in qs]
