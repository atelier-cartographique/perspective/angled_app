# placeholders to be replaced with proper definitions

import itertools


def fk(target, name=None):
    name = target if name is None else name
    return 'fk', name.lower(), target


def term(target, domain=['none']):
    # fixme todo le term pour l'instant ne sert plus à rien
    return 'term', target, domain


def label(target):
    return 'label', target


def text(target):
    return 'text', target


def raw_text(target):
    return 'raw_text', target


def number(target):
    """Type for integer"""
    return 'number', target


def decimal(target, precision=2):
    """Type for decimal"""
    return 'decimal', target, precision


def month(target):
    return 'month', target


def year(target):
    return 'year', target


def date(target):
    return 'date', target


def boolean(target):
    return 'boolean', target


def varchar(target, length=128):
    return 'varchar', target, length


def geometry(geometry_type):
    return 'geometry', 'geom', geometry_type


NAME = 'name'
FIELDS = 'fields'
MULTI = 'multi'

UNITS_COMMON = [
    {
        'name': 'name',
        'fields': [label('name')],
        'multi': False,
    },
    {
        'name': 'document',
        'fields': [varchar('url', 512), label('caption')],
        'multi': True,
    },
    {
        'name': 'image',
        'fields': [varchar('url', 512), label('caption')],
        'multi': False,
    },
    {
        'name': 'actor',
        'fields': [
            term('type', ['actor_type']),
            fk('Contact', 'contact'),
        ],
        'multi': True,
    },
    {
        'name': 'note',
        'fields': [
            term('type', ['note_type']),
            raw_text('body'),
        ],
        'multi': True,
    },
    {
        'name': 'nova',
        'fields': [
            varchar('nova', 32),
        ],
        'multi': True,
    },
    {
        'name': 'capakey',
        'fields': [varchar('value', 32)],
        'multi': True,
    },
    {
        'name':
        'status',
        'fields': [
            term('status', [
                'status_bma', 'status_logement', 'in_out', 'status_pad',
                'status_ecole'
            ])
        ],
        'multi':
        True,
    },
    {
        'name': 'address',
        'fields': [text('address')],
        'multi': False,
    },
    {
        'name': 'cost',
        'fields': [
            term('type', 'cost'),
            decimal('cost'),
        ],
        'multi': True,
    },
    {
        'name': 'description',
        'fields': [text('body')],
        'multi': False,
    },
    {
        'name': 'program',
        'fields': [term('component', ['project_program', 'equipment'])],
        'multi': True,
    },
    {
        'name': 'step',
        'fields': [
            term('step', ['step_pad']),
        ],
        'multi': False,
    },
    {
        'name':
        'public_survey',
        'fields': [
            term('type', 'survey'),
            date('start'),
            date('end'),
            raw_text('comment'),
        ],
        'multi':
        True,
    },
    {
        'name': 'program_name',
        'fields': [term('name', ['program', 'housing'])],
        'multi': True,
    },
    {
        'name': 'point',
        'fields': [
            geometry('point'),
        ],
        'multi': False,
    },
    {
        'name': 'line',
        'fields': [
            geometry('line'),
        ],
        'multi': False,
    },
    {
        'name': 'polygon',
        'fields': [
            geometry('polygon'),
        ],
        'multi': False,
    },
    {
        'name':
        'project_type',
        'fields': [
            term('type', [
                'project_type_internal',
                'project_type_bma',
                'project_type_pad',
                'project_type_ecole',
            ])
        ],
        'multi':
        True,
    },
    {
        'name': 'area',
        'fields': [
            term('type', ['area_function']),
            number('value'),
        ],
        'multi': True,
    },
    {
        'name': 'zone',
        'fields': [
            term('name', ['zone']),
        ],
        'multi': False,
    },
    {
        'name': 'construction_type',
        'fields': [
            term('construction_type', ['construction_type']),
        ],
        'multi': True
    },
    {
        'name':
        'address_struct',
        'fields': [
            text('street_name'),
            raw_text('street_number'),
            number('postal_code'),
            term('town', 'address_struct'),
        ],
        'multi':
        False,
    },
    {
        'name': 'town',
        'fields': [
            term('name', 'town'),
        ],
        'multi': False,
    },
    {
        'name': 'district',
        'fields': [
            term('name', 'district'),
        ],
        'multi': False,
    },
    {
        'name': 'stat_sector',
        'fields': [
            term('name', 'stat_sector'),
        ],
        'multi': False,
    },
]

# strategie
UNITS_STRAT = [
    {
        'name': 'site',
        'fields': [fk('Site')],
        'multi': False,
    },
    {
        'name': 'horizon',
        'fields': [
            month('concept'),
            month('build'),
            month('install'),
        ],
        'multi': False,
    },
    {
        'name': 'site_description_current',
        'fields': [
            term('site_current', 'site_description'),
            number('quantity'),
        ],
        'multi': True,
    },
]

# logement
UNITS_LOG = [
    {
        'name':
        'auxiliary_equipments',
        'fields': [
            term('aux_equipment', 'logement'),
            term('unite', ['unite']),
            number('amount'),
        ],
        'multi':
        True,
    },
    {
        # DEPRECATED
        'name':
        'housing_description',
        'fields': [
            term('planned_release', 'legislature'),
            number('quantity'),
            # niveau du loyer
            term('rental_level', 'rental_level'),
            # indique si le logement est acquisif, locatif, etc. voir aussi https://gitlab.com/atelier-cartographique/perspective/shared/plate-forme-veille-projets-urbains/issues/12#note_133698706
            term('disposal', 'housing_disposal'),
            # composition du logement: nbre de chambre, si pmr, etc.
            term('housing_composition', 'housing_composition'),
            boolean('is_pmr_accessible')
        ],
        'multi':
        True,
    },
    {
        'name':
        'housing_batch',
        'fields': [
            term('planned_release', 'housing_batch'),
            number('quantity'),
            # niveau du loyer
            term('rental_level', 'housing_batch'),
            # indique si le logement est acquisif, locatif, etc. voir aussi https://gitlab.com/atelier-cartographique/perspective/shared/plate-forme-veille-projets-urbains/issues/12#note_133698706
            term('disposal', 'housing_batch'),
        ],
        'multi':
        True,
    },
    {
        'name':
        'housing_info',
        'fields': [
            number('studio'),
            number('one'),
            number('two'),
            number('three'),
            number('four_more'),
            number('pmr'),
        ],
        'multi':
        False,
    },
]

UNITS_ECOL = [
    {
        'name':
        'school_seats',
        'fields': [
            number('quantity'),
            term('school_level_basic', ['school_level']),
            year('opening'),
        ],
        'multi':
        True,
    },
    {
        'name': 'funding',
        'fields': [
            fk('FundingOrg', 'org'),
            year('grant_year'),
            decimal('amount'),
        ],
        'multi': True,
    },
    {
        'name': 'temporary',
        'fields': [boolean('value')],
        'multi': False,
    },
    {
        'name': 'school_site',
        'fields': [number('site')
                   ],  # the relationship to a proxy model is tricky, at least
        'multi': False,
    },
    {
        'name': 'school_creation',
        'fields': [boolean('creation')],
        'multi': False,
    },
    {
        'name': 'school_take_off',
        'fields': [
            number('amount'),
            year('opening'),
        ],
        'multi': False,
    },
    {
        'name':
        'school_info',
        'fields': [
            term('lang'),  # langue d'enseignement
            term('level'),  # niveau d'enseignement
            term('type'),  # type d'enseignement (ordinaire / spécialisés)
            term('network'),  # réseau
        ],
        'multi':
        False,
    },
    {
        # filière  (dans le secondaire)
        'name': 'school_track',
        'fields': [
            term('track'),
        ],
        'multi': True,
    }
]

UNITS_BMA = [
    {
        'name': 'date_opening',
        'fields': [date('start_file')],
        'multi': False,
    },
    {
        'name': 'public_procurement',
        'fields': [term('type', 'pp')],
        'multi': False,
    },
    {
        'name':
        'manager',
        'fields': [
            term('context', 'manager_context'),
            fk('auth.User', name='manager_user'),
        ],
        'multi':
        True,
    },
    # {
    #     'name': 'client',  # maitre d'ouvrage / pilot
    #     'fields': [fk('Contact', 'client_contact')],
    #     'multi': True,
    # },
    {
        'name': 'procedure',
        'fields': [term('term', 'procedure')],
        'multi': False,
    },
    {
        'name': 'fee',
        'fields': [
            term('type', 'fee'),  ## fee type
            text('comment'),  ## fee is often not expressed as a number
        ],
        'multi': False,
    },
    {
        'name': 'date_notice',
        'fields': [date('value')],
        'multi': False,
    },
    {
        'name': 'date_applications',
        'fields': [date('value')],
        'multi': False,
    },
    {
        'name': 'date_offers',
        'fields': [date('value')],
        'multi': False,
    },
    {
        'name': 'date_committee',
        'fields': [date('value')],
        'multi': True,
    },
    {
        'name': 'applicants',  # todo supprimer? (doublon avec candidates)
        'fields': [number('value')],
        'multi': False,
    },
    {
        'name': 'bidders_with_fee',
        'fields': [number('value')],
        'multi': False,
    },
    {
        'name': 'candidates',
        'fields': [number('value')],
        'multi': False,
    },
    {
        'name': 'bidders',
        'fields': [fk('Team', 'bidders_team')],
        'multi': True,
    },
    {
        'name': 'tender_winner',
        'fields': [fk('Team', 'winner_team')],
        'multi': False,
    },
    {
        'name': 'city_type',
        'fields': [term('type', 'city_type')],
        'multi': True,
    },
]

UNITS_PPAS = [
    # {
    #     'name': 'ppas_outer_ref',
    #     'fields': [
    #         raw_text('municipality'),
    #         raw_text('region'),
    #     ],
    #     'multi': False,
    # },

    {
        'name': 'nova_subsidy',
        'fields': [varchar('reference', 64)],
        'multi': False,
    },
    {
        'name': 'visa_subsidy',
        'fields': [varchar('reference', 64)],
        'multi': False,
    },
    {
        'name': 'total_subsidy',
        'fields': [number('amount')],
        'multi': False,
    },
]


UNITS = {
    'common': UNITS_COMMON,
    'strategie': UNITS_STRAT,
    'logement': UNITS_LOG,
    'ecole': UNITS_ECOL,
    'bma': UNITS_BMA,
    'ppas': UNITS_PPAS,
}

UNITS_KEYS = [
    'common',
    'strategie',
    'logement',
    'ecole',
    'bma',
    'ppas',
    'pad',
]


def extract_name(u):
    return u['name']


UNITS_NAMES = list(
    itertools.chain.from_iterable(
        map(lambda U: [extract_name(u) for u in U], UNITS.values())))


def get_unit_fields(unit_name):
    for block in UNITS.values():
        for u in block:
            if u['name'] == unit_name:
                return u['fields']
    raise Exception('Unit Not Found "{}"'.format(unit_name))


def find_field(unit_name, field_name):
    for f in get_unit_fields(unit_name):
        if f[1] == field_name:
            return f
    raise Exception('Field Not Found "{}.{}"'.format(unit_name, field_name))


def get_field_type(unit_name, field_name):
    return find_field(unit_name, field_name)[0]