from urllib.parse import quote
from json import loads
from urllib.request import urlopen
from django.conf import settings
from django.http import JsonResponse, HttpResponse, HttpResponseServerError, Http404

NOVA_URL_BASE = getattr(settings, 'NOVA_URL_BASE',
                        'http://geoservices-others.irisnet.be/geoserver/ows')

NOVA_URL_PARAMS = getattr(
    settings, 'NOVA_URL_PARAMS', {
        'service': 'WFS',
        'version': '2.0.0',
        'request': 'GetFeature',
        'typeName': 'Nova:vmnovaurbanview',
        'outputFormat': 'application/json',
        'srsName': 'EPSG:31370',
    })


def make_filter(key, val):
    template = ('<Filter><PropertyIsEqualTo>'
                '<PropertyName>{key}</PropertyName>'
                '<Literal>{val}</Literal>'
                '</PropertyIsEqualTo></Filter>')

    return template.format(key=key, val=val)


def encode_query(q):
    parts = ['{}={}'.format(k, quote(v)) for k, v in q.items()]
    return '&'.join(parts)


def extract_props(features):
    first = features['features'][0]
    props = first['properties']
    return props


def nova_proxy(request, key, val):
    """
    Get from the WFS the last version of data.
    """

    url_base = NOVA_URL_BASE
    params = NOVA_URL_PARAMS.copy()
    params["FILTER"] = make_filter(key, val)

    url_with_params = '{}?{}'.format(url_base, encode_query(params))

    with urlopen(url_with_params) as url:
        status = url.getcode()
        if status == 200:
            body = url.read()
            data = loads(body.decode())
            try:
                return JsonResponse(extract_props(data))
            except Exception as ex:
                return HttpResponseServerError(content=str(ex))
        else:
            return HttpResponse(content=url.reason, status=status)

    raise Http404()