from django.contrib import admin
from django import forms
import json

from angled.ui import UNITS_NAMES
from angled.models import dyn
from angled.models import ref
from angled.models import profi
from angled.models import query
from angled.models import audience
from angled.models import subscription


class ProjectAdmin(admin.ModelAdmin):
    search_fields = ('id', )

class TransactionAdmin(admin.ModelAdmin):
    search_fields = ('id', )


class ProfileTextWidget(forms.Textarea):
    template_name = 'angled/forms/widgets/profile/textarea.html'




class ProfileForm(forms.ModelForm):
    class Meta:
        model = profi.Profile
        fields = (
            'name',
            'text',
        )
        widgets = {
            'text':
            ProfileTextWidget(attrs={
                'cols': 40,
                'rows': 32,
                'units': UNITS_NAMES
            }),
        }




class ProfileAdmin(admin.ModelAdmin):
    form = ProfileForm


class KindAdmin(admin.ModelAdmin):
    list_display = ('name',)

class ContactAdmin(admin.ModelAdmin):
    list_display = ('name',)


class DomainAdmin(admin.ModelAdmin):
    list_display = ('name',)


class TermAdmin(admin.ModelAdmin):
    list_display = ('name',)


class DomainFieldMappingAdmin(admin.ModelAdmin):
    list_display = ('domain', 'unit', 'field',)

class TagAdmin(admin.ModelAdmin):
    list_display = ('label',)

class AudienceAdmin(admin.ModelAdmin):
    pass


class JSONWidget(forms.widgets.Textarea):

    def format_value(self, value):
        try:
            print('a')
            print(value)
            value = json.dumps(json.loads(value), indent=2, sort_keys=True)

            print('b')
            print(value)
            # these lines will try to adjust size of TextArea to fit to content
            row_lengths = [len(r) for r in value.split('\n')]
            self.attrs['rows'] = min(max(len(row_lengths) + 2, 10), 30)
            self.attrs['cols'] = min(max(max(row_lengths) + 2, 40), 120)
            return value
        except Exception:
            return super(JSONWidget, self).format_value(value)

    def value_from_datadict(self, data, files, name):
        text_data= data[name]
        data = json.loads(text_data)
        return data

    

class QueryForm(forms.ModelForm):
    class Meta:
        model = query.Query
        fields = (
            'user',
            'name',
            'statements',
        )
        widgets = {
            'statements': JSONWidget(attrs={}),
        }



class QueryAdmin(admin.ModelAdmin):
    form = QueryForm

class ProjectSubscriptionAdmin(admin.ModelAdmin):
    pass

class CircleSubscriptionAdmin(admin.ModelAdmin):
    pass

class ProjectNotificationAdmin(admin.ModelAdmin):
    pass

class CircleNotificationAdmin(admin.ModelAdmin):
    pass


admin.site.register(dyn.Project, ProjectAdmin)
admin.site.register(dyn.Transaction, TransactionAdmin)

admin.site.register(ref.Kind, KindAdmin)
admin.site.register(ref.Contact, ContactAdmin)
admin.site.register(ref.Domain, DomainAdmin)
admin.site.register(ref.Term, TermAdmin)
admin.site.register(ref.DomainFieldMapping, DomainFieldMappingAdmin)
admin.site.register(ref.Tag, TagAdmin)
admin.site.register(audience.Audience, AudienceAdmin)

admin.site.register(profi.Profile, ProfileAdmin)

admin.site.register(query.Query, QueryAdmin)

admin.site.register(subscription.ProjectSubscription, ProjectSubscriptionAdmin)
admin.site.register(subscription.CircleSubscription, CircleSubscriptionAdmin)
admin.site.register(subscription.ProjectNotification, ProjectNotificationAdmin)
admin.site.register(subscription.CircleNotification, CircleNotificationAdmin)
