from copy import deepcopy
from datetime import datetime
from rest_framework import serializers

from django.contrib.auth.models import User
from django.conf import settings
from django.contrib.auth.models import Group
from django.apps import apps
from django.contrib.gis.geos import GEOSGeometry

from lingua.serializers import LinguaRecordSerializer
from angled.models.query import Query, QueryShare
from angled.query import deser_statements, deser_selects
from angled.serializers.fields import JSONField
from angled.serializers.projects import ProjectSerializer
from angled.models.dyn import Project
from angled.models.ref import (Term, Team, FundingOrg, Contact)
from angled.query import (
    field_type,
    deser_selects,
    deser_statements,
    get_path,
    get_select_info,
    parse_input,
    parse_select,
)
from angled.ui import find_field
from json import dumps



ADMIN_QUERY_NAME = getattr(settings, 'ADMIN_QUERY_GROUP', None)
ADMIN_QUERY = Group.objects.get(
    name=ADMIN_QUERY_NAME) if ADMIN_QUERY_NAME is not None else None


def is_query_admin(user):
    is_admin = False
    if ADMIN_QUERY is not None:
        for g in user.groups.values_list():
            if g[0] == ADMIN_QUERY.id:
                is_admin = True
    return is_admin


class QuerySerializer(serializers.ModelSerializer):
    """
    Serializer for Query class
    """
    name = LinguaRecordSerializer()
    statements = JSONField()

    class Meta:
        model = Query
        fields = ('id', 'name', 'user', 'statements')

    def validate_statements(self, value):
        # we need this as deser_* might mutate value
        test = deepcopy(value)
        where = test.get('where')
        select = test.get('select')
        try:
            deser_statements(where)
            deser_selects(select)
        except Exception as ex:
            raise serializers.ValidationError(
                'Failed to parse statements: {}'.format(ex))

        return value

    def validate_user(self, value):
        request = self.context.get('request')
        if request is not None and (request.user.id == value.id
                                    or is_query_admin(request.user)):
            return value

        raise serializers.ValidationError(
            'User ({}) is not the query author ({}), and is no admin for query'
            .format(value.id, request.user.id))


class QueryShareSerializer(serializers.ModelSerializer):
    """
    Serializer for QueryShare class
    """
    description = LinguaRecordSerializer()

    class Meta:
        model = QueryShare
        fields = ('id', 'description', 'query')


#########################################
########## tabular serializers ##########
#########################################


def alias_name(column, lang):
    Alias = apps.get_model(app_label='api', model_name='Alias')
    try:
        a = Alias.objects.get(select=column)
        return a.replace.get(lang)
    except Alias.DoesNotExist:
        return column


def multi_output(select):
    return select.aggregate is not None and select.aggregate.name == 'concat'


def ensure_list(x):
    if isinstance(x, (
            list,
            tuple,
    )):
        return x
    return [x]


def make_serializer(fn):
    def inner(select, val, lang):
        if multi_output(select):
            output = []
            for val in ensure_list(val):
                ser_val = fn(val, select, lang)
                if type(ser_val) == str:
                    output.append(ser_val)
            return ', '.join(output)
        else:
            ser_val = fn(val, select, lang)
            if ser_val is not None:
                return ser_val
            return ''

    return inner


def get_fk(pk, select, lang):
    if pk is None:
        return ''
    field = find_field(select.unit, select.field[0])
    model = field[2]
    try:
        if model == 'Contact':
            return Contact.objects.get(pk=pk).name
        elif model == 'FundingOrg':
            return FundingOrg.objects.get(pk=pk).name
        elif model == 'auth.User':
            u = User.objects.get(pk=pk)
            display_name = u.get_full_name()
            if len(display_name) > 0:
                return display_name
            else:
                return u.username
        elif model == 'Team':
            t = Team.objects.get(pk=pk)
            return '; '.join([m.member.name for m in t.teammember_set.all()])

    except Exception as ex:
        print('get_fk error', ex)

    return '{}({})'.format(model, pk)


def get_term(pk, select, lang):
    try:
        return getattr(Term.objects.get(pk=pk).name, lang)
    except Exception:
        ''

def format_coord(coord):
    return round(coord)

def format_coords(*coords):
    return list(map(format_coord, coords))

def format_geom(unit, select, lang):
    if unit is not None:
        geom = GEOSGeometry(dumps(unit))
        geom.srid = 31370
        geomtype = geom.geom_type
        label = [geomtype]
        cc = geom.centroid.coords
        label.append(f'centroid: {format_coords(*cc)}')

        if geomtype == 'MultiPolygon':
            label.append(f'area: {format_coord(geom.area)}m²')
        elif geomtype == 'MultiLine':
            label.append(f'length: {format_coord(geom.length)}m')

        return "; ".join(label)
    else: 
        return '-'

def format_date(timestamp, select, lang):
    if type(timestamp) == str:
        return timestamp
    try:
        date = datetime.fromtimestamp(timestamp / 1000).date()
        return date.isoformat()
    except Exception as ex:
        # print(f"format_date error: '{timestamp}' -> '{ex}'")
        return ''


ser_fk = make_serializer(get_fk)
ser_term = make_serializer(get_term)
ser_string = make_serializer(lambda a, b, c: str(a))
ser_date = make_serializer(format_date)
ser_geom = make_serializer(format_geom)

SERIALIZERS = {
    'fk': ser_fk,
    'term': ser_term,
    'label': ser_string,
    'text': ser_string,
    'raw_text': ser_string,
    'number': ser_string,
    'decimal': ser_string,
    'month': ser_string,
    'year': ser_string,
    'date': ser_date,
    'boolean': ser_string,
    'varchar': ser_string,
    'geometry': ser_geom,
}


def serialize_query(request, data, lang):
    wheres = deser_statements(data.get('statements', {}).get('where', []))
    selects = deser_selects(data.get('statements', {}).get('select', []))

    parsed = parse_input(wheres)
    select_info = get_select_info(
        list(filter(lambda s: s.name != 'geom', selects)))

    field_names = ['id']+[alias_name(s.name, lang) for s in selects]
    table = []

    if parsed is None:
        return [field_names], table

    queryset = Project.objects.filter(parsed).distinct()
    pids = [p.id for p in queryset]

    if len(pids) == 0:
        return [field_names], table

    pdata = Project.full_loader.get_all_as_dict(pids)
    projects = ProjectSerializer(pdata,
                                 many=True,
                                 context=dict(request=request))

    for project in projects.data:
        row = [project['id']]
        for select, unit, is_multi, fields in select_info:
            ser = SERIALIZERS[field_type(select)]
            unit_data = project.get(unit, [{}] if is_multi else {})
            if is_multi:
                output = parse_select(select, unit_data)
            else:
                output = get_path(select.field, unit_data)
            row.append(ser(select, output, lang))
        table.append(row)

    return field_names, table


def write_csv_file(request, file, query_data, lang):
    from csv import writer

    field_names, table = serialize_query(request, query_data, lang)

    # to force Excel to use utf8 encoding, bom:'ufeff'
    file.write('\uFEFF')
    csv_writer = writer(file)
    csv_writer.writerow(field_names)
    csv_writer.writerows(table)


def write_xlsx_file(request, workbook, query_data, lang):

    field_names, table = serialize_query(request, query_data, lang)

    worksheet = workbook.add_worksheet()
    worksheet.write_row(0, 0, field_names)
    for index, entry in enumerate(table):
        worksheet.write_row(index + 1, 0, entry)
