from datetime import datetime
from pathlib import Path
from django.core.management.base import BaseCommand, CommandError

from angled.ui import UNITS

# UNIT_FIELDS = [
#     '"{unit}.id"[label="id",shape=box]',
#     '"{unit}.project"[label="project",shape=box]\n    "{unit}.project" -> "project.id"',
#     '"{unit}.user"[label="user",shape=box]\n    "{unit}.user" -> "user.id"',
#     '"{unit}.transaction"[label="transaction",shape=box]\n    "{unit}.transaction" -> "transaction.id"',
#     '"{unit}.eol"[label="end_of_life",shape=box]',

#     # '"{unit}.id" -> "{unit}.project" -> "{unit}.user" -> "{unit}.transaction" -> "{unit}.eol" [style=invis]',
# ]

UNIT_FIELDS = ['id', 'project', 'user', 'transaction', 'eol']

UNIT_RELS = [
    '',
    '"{unit}":project -> "Project":id',
    '"{unit}":user -> "auth.User":id',
    '"{unit}":transaction -> "Transaction":id',
]

# UNIT_FIELDS_TEMPLATE = [
#     '"{unit}.id"[label="id"]',
#     '"{unit}.project"[label="project"]\n    "{unit}.project" -> "project.id"',
#     '"{unit}.user"[label="user"]\n    "{unit}.user" -> "user.id"',
#     '"{unit}.transaction"[label="transaction"]\n    "{unit}.transaction" -> "transaction.id"',
#     '"{unit}.eol"[label="end_of_life"]',
# ]

TEMPLATE_LABEL = """
//subgraph  label_{name} {{
    "label_{name}"[label="label_{name}|<id> id| fr | nl| en", color="black"];
//}}
"""

TEMPLATE_TEXT = """
//subgraph  text_{name} {{
    "text_{name}"[label="text_{name}|<id> id| fr | nl| en", color="black"];
//}}
"""


def mk_record_label0(name, field_list):
    fields = '|'.join(map(lambda f: '<{f}> {f}'.format(f=f), field_list))
    return '{name} |{fields}'.format(name=name, fields=fields)


def mk_record_label1(name, field_list):
    fields = []  # '|'.join(map(lambda f: '<{f}> {f}'.format(f=f), field_list))
    for f in field_list:
        fields.append('<TR><TD PORT="{f}">{f}</TD></TR>'.format(f=f))

    return """<<TABLE BORDER="0" CELLBORDER="0" CELLSPACING="0">
                    <TR><TD PORT="f0"><B>{name}</B></TD></TR>
                    {fields}
                </TABLE>>""".format(
        name=name, fields=fields)


TEMPLATE_UNIT = """
subgraph info_unit_{name} {{
    // label = "info_unit_{name}"
   node[shape = "record", color="white"];
    {fields}


    "permission_{name}.node"[label="permission_{name} |<id> id|<unit> unit|<group> group", color="black"]
    "permission_{name}.node":unit -> "info_unit_{name}":id
    "permission_{name}.node":group -> "auth.Group":id
}}
"""

MULTI_FIELDS = [
    '"{name}.id"[label="id"]',
    '"{name}.ts"[label="ts"]',
]

TEMPLATE_MULTI = """
subgraph  list_{name} {{
    label="list_{name}"
    "list_{name}"[label="list_{name} |<id> id |<index> index|<unit> unit"]
    "list_{name}":index -> "ListIndex":id
    "list_{name}":unit -> "info_unit_{name}":id
}}
"""


def fk(source, target):
    return '{source} -> "{target}":id;'.format(source=source, target=target)


def label(target):
    return fk('label_' + target, target)


def text(target):
    return fk('text_' + target, target)


def term(field_name, domain):
    return fk(field_name, 'term')


def geometry(target, geometry_type):
    return '"{{unit}}.{name}"[label="{name}"]'.format(name=target)


def base(name, fields, labels, texts):

    unit = 'info_unit_' + name
    label = mk_record_label1(name,
                             UNIT_FIELDS + list(map(lambda x: x[1], fields)))

    def fp(f):
        return '"{}":{}'.format(unit, f)

    rels = list(map(lambda r: r.format(unit=unit), UNIT_RELS))
    for field in fields:
        fn = field[0]
        fieldname = field[1]
        # ['fk', 'term', 'label', 'text']
        if 'fk' == fn:
            rels.append(fk(fp(fieldname), field[2]))
        elif 'term' == fn:
            rels.append(fk(fp(fieldname), 'Term'))
        elif 'label' == fn:
            if fieldname not in labels:
                labels.append(fieldname)
            rels.append(fk(fp(fieldname), 'label_' + fieldname))
        elif 'text' == fn:
            if fieldname not in texts:
                texts.append(fieldname)
            rels.append(fk(fp(fieldname), 'text_' + fieldname))

    a = '"{}"[label={}, shape="none", fontsize = 12];'.format(unit, label)
    b = '    \n'.join(rels)

    return TEMPLATE_UNIT.format(name=name, fields=a + '\n' + b)


def multi(name):
    return TEMPLATE_MULTI.format(name=name)


NAME = 'name'
FIELDS = 'fields'
MULTI = 'multi'

SOURCE_TEMPLATE = """
//subgraph cluster_source_{source} {{
//    source_node_{source} [label="{source}", shape=plaintext]
"""

BASE_FIELDS = {
    'Project': ['id'],
    'auth.User': ['id'],
    'auth.Group': ['id'],
    'Contact': ['id', 'name', 'info'],
    'Transaction': ['id', 'created_at', 'project', 'user'],
    'ListIndex': ['id', 'ts'],
    'LabelRecord': ['id', 'fr', 'nl', 'en'],
    'TextRecord': ['id', 'fr', 'nl', 'en'],
    'Kind': ['id', 'name'],
    'Domain': ['id', 'kind', 'name'],
    'Term': ['id', 'domain', 'name', 'description'],
    'Site': ['id', 'name'],
    'Nova': ['ref', 'time_of_fetch', 'data'],
    'SchoolSite': ['id', '...'],
    'FundingOrg': ['id', 'name'],
    'Team': ['id', 'form', 'contact'],
}

BASE_RELS = {
    'Transaction': [('project', 'Project'), ('user', 'auth.User')],
    'Kind': [('name', 'LabelRecord')],
    'Domain': [('name', 'LabelRecord'), ('kind', 'Kind')],
    'Term': [('name', 'LabelRecord'), ('description', 'TextRecord'),
             ('domain', 'Domain')],
    'Site': [('name', 'LabelRecord')],
    'Team': [('contact', 'Contact')],
}


def make_base():
    nodes = []
    edges = []
    for n, fs in BASE_FIELDS.items():
        nodes.append('"{}"[label="{}"];'.format(n, mk_record_label0(n, fs)))

    for n, rels in BASE_RELS.items():
        for rel in rels:
            edges.append('"{}":{} -> "{}":id;'.format(n, rel[0], rel[1]))

    return """
    subgraph cluster_common {{
        {nodes}

        {edges}
    }}
    """.format(
        nodes='\n'.join(nodes), edges='\n'.join(edges))


def main(output):

    sql_units = [
        """
digraph base {
    node [
                fontname = "Source Code Pro"
                fontsize = 8
                shape = "record"
                // penwidth = 0.1
                // style="setlinewidth(0.1)"
                // shape=plaintext
        ]

    // compound=true
    // concentrate=true
    // ratio=fill
    rankdir=LR
""",
        make_base(),
    ]

    labels = []
    texts = []

    for source, units in UNITS.items():
        sql_units.append('\n\n// ******************')
        sql_units.append('// **  {} '.format(source))
        sql_units.append('// ******************\n\n')
        sql_units.append(SOURCE_TEMPLATE.format(source=source))
        for U in units:
            sql_units.append('// ' + U[NAME] + '  -  ' +
                             ('multi' if U[MULTI] else 'single'))

            # print('{}#{} "{}"'.format(source, U[NAME], U[FIELDS]))
            sql_units.append(base(U[NAME], U[FIELDS], labels, texts))

            if U[MULTI]:
                sql_units.append(multi(U[NAME]))

        sql_units.append('//}')

    for l in labels:
        sql_units.append(TEMPLATE_LABEL.format(name=l))
    for t in texts:
        sql_units.append(TEMPLATE_TEXT.format(name=t))

    with open(output.as_posix(), 'w') as out:
        out.write('// generated by gen_dot on {}\n\n'.format(datetime.now()))
        out.write('\n'.join(sql_units))
        out.write('}')


class Command(BaseCommand):
    help = """Generate a dot file of al of information units
    """

    output_transaction = True

    def add_arguments(self, parser):
        parser.add_argument('output')

    def handle(self, *args, **options):
        output = Path(options['output'])

        main(output)
