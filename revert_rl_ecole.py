from django.contrib.auth.models import User
from angled.models.dyn import get_unit_model
from angled.ui import UNITS_NAMES

yves = User.objects.get(username='adujardin')

for name in UNITS_NAMES:
    model = get_unit_model(name)
    model.objects.filter(user=yves).delete()
