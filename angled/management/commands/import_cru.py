from angled.management.import_base import (
    ImportCommand,
    ReaderType,
    get_cell_name,
    get_val,
    get_label,
    get_text,
    UnitResult,
    UnitSkip,
    MultiResult,
)
from angled.models import dyn
from django.contrib.auth.models import User

import logging

# CRU = Contracts de renovations urbaines


logger = logging.getLogger('import')
logger.debug('test')

UNITS = (
    ('dru_id', 'DRU_ID'),
    # ('serie') todo que mette ?
    ('name_fr', 'NOM_FR'),
    ('name_nl', 'NOM_NL'),
    ('manager', 'ProjRespon'),
    ('timing', 'Timing'),
    ('geom', '__geometry__'),
)


CELL_NAMES = [(cell, model) for model, cell in UNITS]

cell_name = get_cell_name(UNITS)
val = get_val(UNITS)
label = get_label(UNITS)
text = get_text(UNITS)


class Command(ImportCommand):
    reader_type = ReaderType.SHP
    file_encoding = 'WINDOWS-1252'
    ZONES = []

    def process_row(self, row, create_unit):
        label_fr = val(row, 'name_fr')
        label_nl = val(row, 'name_nl')
        managers_list = val(row, 'manager')

        label_func = dyn.unit_all_label('name')
        kwargs = dict()
        kwargs['name'] = label_func({'fr': label_fr, 'nl': label_nl})
        create_unit('name', UnitResult(**kwargs))

        manager_users_list = []

        for manager_username in managers_list.split(','):
            manager_username = manager_username.strip()

            if manager_username:
                try:
                    manager_user = User.objects.get(username=manager_username)
                except User.DoesNotExist:
                    manager_user = User.objects.create_user(manager_username)
                    manager_user.save()
                    logger.debug("Création de l'utilisateur '{}'".format(
                        manager_username))
                manager_users_list.append(UnitResult(manager_user=manager_user))
        create_unit('manager', MultiResult(manager_users_list))
