from django.db import models
from django.contrib.auth.models import User
from django.contrib.postgres.fields import JSONField
from lingua.fields import label_field, text_field


class Query(models.Model):
    id = models.AutoField(primary_key=True)
    user = models.ForeignKey(
        User, related_name='angled_query', on_delete=models.CASCADE)
    statements = JSONField()
    name = label_field('query_name')

    def __str__(self):
        return str(self.name)


class QueryShare(models.Model):
    id = models.AutoField(primary_key=True)
    query = models.ForeignKey(
        Query, on_delete=models.CASCADE, related_name='share')
    description = text_field('query_share')

    def __str__(self):
        return 'share -> {}'.format(self.query.name)