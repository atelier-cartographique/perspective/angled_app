from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('angled', '0063_auto_20220324_1013'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='term',
            options={'ordering': ['sort_index']},
        ),
        migrations.AddField(
            model_name='term',
            name='sort_index',
            field=models.IntegerField(default=1),
            preserve_default=False,
        ),
    ]
