from datetime import datetime
from pathlib import PosixPath
from django.core.management.base import BaseCommand, CommandError

from angled.profile.parser import parse_profile
from angled.models.profi import Profile
from json import dumps


class Command(BaseCommand):
    help = """Install a profile file in the system
    """

    output_transaction = True

    def add_arguments(self, parser):
        parser.add_argument(
            '-r',
            '--raise',
            action='store_true',
            help='Raise on warnings',
            default=True)
        parser.add_argument(
            '-d',
            '--dry-run',
            action='store_true',
            help='Do not update or create model instance',
            default=False)
        parser.add_argument(
            '-p',
            '--print',
            action='store_true',
            help='Print compiled prrofile',
            default=False)
        parser.add_argument('name')
        parser.add_argument('file')

    def handle(self, *args, **options):
        # print(options)
        f = PosixPath(options['file'])
        name = options['name']
        raise_on_warning = options['raise']
        dry_run = options['dry_run']
        print = options['print']

        with f.open() as p:
            text = p.read()
            try:
                profile_data = parse_profile(text, raise_on_warning)
            except Exception as ex:
                self.stdout.write(
                    self.style.ERROR(
                        "Failed to parse profile \"{}\":\n{}".format(name,
                                                                     ex)))
                return

            if print:
                self.stdout.write(dumps(profile_data, indent=2))

            if not dry_run:
                try:
                    instance = Profile.objects.get(name=name)
                    instance.text = text
                    instance.save()
                    self.stdout.write(
                        self.style.SUCCESS(
                            'Updated profile "{}"'.format(name)))
                except Profile.DoesNotExist:
                    Profile.objects.create(name=name, text=text)
                    self.stdout.write(
                        self.style.SUCCESS(
                            'Created profile "{}"'.format(name)))
                except Exception as ex:
                    self.stdout.write(
                        self.style.ERROR(
                            'Failed to create or update profile "{}":\n{}'.
                            format(name, ex)))
