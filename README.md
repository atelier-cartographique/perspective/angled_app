# Angled

## Settings

### Administrateur Query

Pour qu'un utilisateur ai les droits de modification sur une requête dont il n'est pas l'auteur, il doit faire partie d'un groupe ayant de tels droits. Pour qu'un groupe ai ces droits, il faut le renseigner dans les settings avec la clé "ADMIN_QUERY_GROUP". Par exemple, si le groupe se nomme 'admin', on indiquera:  
```ADMIN_QUERY_GROUP = 'admin'```


## imports

See [commands](./angled/management/commands/README.md)


## Documentation

### Units and list

#### Historique des unités d'un projet

Note: à refactoriser, voir https://gitlab.com/atelier-cartographique/perspective/backlog-dev/issues/19

#### Multi lists

Sur les instances multi, le manager est customisé pour ajouter des méthodes spécifiques.

##### Entrées actuelles d'une liste: `objects.get_current(project)`

La méthode `get_current` permet de récupérer les instances courantes (= les dernières introduites) des UI multi.

Exemple:

```python
from datetime import datetime
from angled.models import dyn
project = dyn.Project.objects.all()[0]
note_model = dyn.get_multi_model('note')

# renvoie un RawQuerySet avec la liste des "instances" multi ajoutées en dernier lieu pour ce projet:
note_model.objects.get_current(project)

# qui peuvent être itérées:
for multi_instance in note_model.objects.get_current(project):
  # on récupère les unités d'informations associées
  print(multi_instance.unit)

# il est également possible de récupérer la liste à une date précise:
note_model.objects.get_current(project, at=datetime.fromtimestamp(1235))
```

##### Historique des entrées pour une liste: `objects.get_last(project)`

La méthode `get_last` permet de récupérer une liste des entités multi associée aux 50 dernières modifications de la liste (donc pour les 50 derniers listindex).

```python
from angled.models import dyn
project = dyn.Project.objects.all()[0]
note_model = dyn.get_multi_model('note')

# renvoie un RawQuerySet avec la liste des "instances" multi associées aux 50 derniers `listIndex`
note_model.objects.get_last(project)
```


### API

Dans les exmples ici, on utilise httpie qui est un wrapper à curl écrit en python: https://github.com/jakubroztocil/httpie

Pour utiliser `http` en cli, on assumera ici qu'il existe un utilisateur admin, mot de passe admin (à créer auparavant avec `python manager.py createsuperuser`).

#### Liste des projets

Une liste des projets et des UI qui y sont associées peut être récupérée par le endpoint suivant:

```
GET /api/geodata/angled/p/project/
```

Paramètres:

- `at` (timestamp * 1000): permet de récupérer les unités d'informations dans l'état où elles étaient au moment spécifié en paramètre. Attention, les projets qui n'existaient pas encore à ce moment sont également listés: le filtre n'agit que sur les unités d'informations, pas sur la date de création du projet.


#### Récupération d'un projet et de ses UI

Un projet et les UI qui y sont associées peut être récupérée par le endpoint suivant:

```
GET /api/geodata/angled/p/project/{id du projet}
```

Exemple:

```
GET /api/geodata/angled/p/project/1
```

Paramètres:

- `at` (timestamp * 1000): permet de récupérer les unités d'informations dans l'état où elles étaient au moment spécifié en paramètre. Attention, les projets qui n'existaient pas encore à ce moment ne donnent pas lieu à une erreur: le filtre n'agit que sur les unités d'informations, pas sur la date de création du projet.


#### Créer un nouveau projet

Envoyer une requête POST sur le endpoint `/api/geodata/angled/p/project/`:

```bash
http --json -a admin:admin --verbose POST :8000/api/geodata/angled/p/project/
```

Et la requête va renvoyer l'identifiant du projet créé:

```
HTTP/1.1 201 Created
Allow: GET, POST, HEAD, OPTIONS
Content-Length: 52
Content-Type: application/json
Date: Thu, 25 Apr 2019 20:27:59 GMT
Server: WSGIServer/0.2 CPython/3.5.2
Vary: Accept, Origin
X-Frame-Options: SAMEORIGIN

{
    "created_at": "2019-04-25T20:27:59.779246Z",
    "id": 11
}
```



#### Modifier les unités d'information via l'API


Imaginons que:

- il existe un projet numéro 1 (à créer dans la bdd ou via une requête POST ci-dessus) ;

##### Endpoint HTTP


Il n'y qu'un seul endpoint: `/api/geodata/angled/p/project/{project id}/ui/`

Pour modifier les données du projet numéro 1, voici la requête:

```bash
http --json -a admin:admin --verbose POST :8000/api/geodata/angled/p/project/1/ui/ < data.json
```

Le contenu de data.json change en fonction de ce qu'on veut mettre à jour.

##### Ajouter / modifier une UI unique (donc `multi: False`)

On lui ajoute un nom. il s'agit de l'unité d'information qui a la clé `name`, et un champ également appelé `name`:

Le fichier json a la syntaxe suivante:

```json
{"name":{"name": {"fr": "Updated name"}}}
```

Il est tout à fait possible de modifier plusieurs unités d'informations en une fois.

##### Ajouter / modifier une UI multiple (donc `multi: True`)

1. Il faut créer les termes adéquats dans la BDD. On peut le faire en utilisant `./manage.py shell`

```python
from angled.models import ref
d = ref.Domain()
d.name = dict(fr="Type de note")
d.save()
t = ref.Term()
t.name = dict(fr="Nouveau type")
t.domain = d
t.save()
t.id # renvoie "3"
```

Ici, le terme a l'id 3.

2. Requête pour ajouter des nouveaux éléments à une liste. Pour rappel, une note a deux caractéristiques: un type de note (qui est un terme), sous la propriété `type` et un contenu, sous la propriété `body`. Le json est donc:

```bash
{
    "note": {
        "new": [
            {
                "body": "This is a short note.",
                "type": 3
            },
            {
                "body": "This is another short note",
                "type": 3
            }
        ]
    }
}
```

3. On modifie les éléments d'une note: on ajoute un nouveau et on met à jour un autre:

```json
{
    "note": {
        "new": [
            {
                "body": "New short note",
                "type": 3
            }
        ],
        "updated": [
            {
                "body": "Updated short note",
                "source_id": 5,
                "type": 3
            }
        ]
    }
}
```

4. Maintenant, on souhaite supprimer un élément:

```json
{
    "note": {
        "removed": [
            {
                "source_id": 9
            }
        ]
    }
}
```

**Note**: L'action "update" est identique à l'action "new" + "remove": une nouvelle unité d'information est créée avec les caractéristiques renvoyées, et celle indiquée par `source_id` est enlevée de la nouvelle liste.

#### Historique - timeline

Nouveau endpoint : `/api/geodata/angled/p/project/{project_id}/timeline/`

Le endpoint renvoie les 50 entrées mises à jour parmi les unités d'information:

```json
[
    {"name": "cost", "id": 1, "created_at": 1556222733105.033},
    {"name": "note", "id": 22, "created_at": 1556222733094.962},
    {"name": "note", "id": 21, "created_at": 1556222733089.517},
    {"name": "note", "id": 17, "created_at": 1556220269924.925}
]
```

Les éléments:

- "name": le nom de l'UI ;
- "id": l'id de l'UI ;
- "created_at": la date de création de l'UI, en timestamp * 1000 (donc en millisecondes)

Le endpoint peut également renvoyer un seul type d'unité d'information, si celui-ci est précisé en paramètre:

```
GET /api/geodata/angled/p/project/{project_id}/timeline/?ui=name
```

Dans le cas où les UI sont unique, le json généré aura la forme suivante:

```json
[
    {
        "name": {
            "fr": "Updated name"
        },
        "id": 6,
        "project": 1,
        "user": 1,
        "created_at": 1556106644646.897
    },
    {
        "name": {
            "fr": "Updated name"
        },
        "id": 5,
        "project": 1,
        "user": 1,
        "created_at": 1556099663845.604
    },
    {
        "name": {
            "fr": "Updated name"
        },
        "id": 4,
        "project": 1,
        "user": 1,
        "created_at": 1556096609424.3801
    }
]
```

Si l'UI est multiple, alors les éléments sont listés à l'intérieur de chaque entrée:

```json
[
    {
        "id": 23,
        "data": [
            {
                "type": 2,
                "body": "New short note added during removed",
                "id": 15,
                "project": 1,
                "user": 1,
                "created_at": 1556219820934.118
            },
            {
                "type": 2,
                "body": "This is another short note",
                "id": 22,
                "project": 1,
                "user": 1,
                "created_at": 1556222733094.962
            },
            {
                "type": 2,
                "body": "Updated short note",
                "id": 10,
                "project": 1,
                "user": 1,
                "created_at": 1556111979204.077
            },
            {
                "type": 2,
                "body": "New short note 2 added during removed",
                "id": 16,
                "project": 1,
                "user": 1,
                "created_at": 1556220208668.911
            },
            {
                "type": 2,
                "body": "New short note 2 added during removed",
                "id": 17,
                "project": 1,
                "user": 1,
                "created_at": 1556220269924.925
            },
            {
                "type": 2,
                "body": "This is a short note.",
                "id": 21,
                "project": 1,
                "user": 1,
                "created_at": 1556222733089.517
            }
        ],
        "created_at": 1556220269921.186,
        "unit_name": "note"
    },
    {
        "id": 22,
        "data": [
            {
                "type": 2,
                "body": "New short note 2 added during removed",
                "id": 17,
                "project": 1,
                "user": 1,
                "created_at": 1556220269924.925
            },
            {
                "type": 2,
                "body": "New short note 2 added during removed",
                "id": 16,
                "project": 1,
                "user": 1,
                "created_at": 1556220208668.911
            },
            {
                "type": 2,
                "body": "Updated short note",
                "id": 10,
                "project": 1,
                "user": 1,
                "created_at": 1556111979204.077
            },
            {
                "type": 2,
                "body": "New short note added during removed",
                "id": 15,
                "project": 1,
                "user": 1,
                "created_at": 1556219820934.118
            }
        ],
        "created_at": 1556220208665.291,
        "unit_name": "note"
    }
]
```

Dans ce dernier cas, l'identifiant donné est celui du `listindex`.

Pour chaque entrée de listindex, l'état de la liste des UI est indiqué.

#### Entités externes (Unités d'information `FK`)

Endpoints pour créer des unités d'information FK

##### Contact

**Point d'entrée** : `/api/geodata/angled/ref/contact/`

**Modèle**:

```json

{
    "id": 2,
    "name": "Champs-Libres - Atelier-Cartographique",
    "info": "Fait un tres bon boulot"
}
```

Pour créer une entrée:

`POST /api/geodata/angled/ref/contact/` avec le json suivant:

```json
{
    "name": "Champs-Libres - Atelier-Cartographique",
    "info": "Fait un tres bon boulot"
}
```

##### Site

Endpoint: `/api/geodata/angled/ref/site/`


Modèle:

```json
{
    "id": 1,
    "name": {
        "fr": "Plaine du Bassin du Canal"
    }
}
```

Pour créer une entrée:

`POST /api/geodata/angled/ref/site/` avec le json suivant:


```json
{
    "name": {"fr": "Plaine du Bassin du Canal"}
}
```

##### Nova

Attention besoin des paramètres suivants :

```python
NOVA_URL_BASE = "http://geoservices-others.irisnet.be/geoserver/ows?service=WFS"
NOVA_URL_PARAMS = {
    "version": "2.0.0",
    "request": "GetFeature",
    "typeName": "Nova:vmnovaurbanview",
    "outputFormat": "application/json",
    "srsName": "EPSG:31370",
    "count": "1",
}
NOVA_PROPERTIES = [
    "s_iddossier", "s_idaddress", "coordinate_x", "coordinate_y",
    "municipalityownerfr", "municipalityownernl", "catdossier",
    "typedossier", "streetnamefr", "streetnamenl", "zipcode"]
```
Endpoint: `/api/geodata/angled/ref/nova/`

Modèle:

```json
{
    "ref": "04/PFD/40156-01",
    "time_of_fetch": 1562345849405.693,
    "data": {
        "s_iddossier": 1000607,
        "s_idaddress": 60080,
        "coordinate_x": null,
        "coordinate_y": null,
        "municipalityownerfr": "Bruxelles",
        "municipalityownernl": "Brussel",
        "catdossier": "U",
        "typedossier": "PFD",
        "streetnamefr": "Rue au Beurre",
        "streetnamenl": "Boterstraat",
        "zipcode": "1000"
    }
}
```

Pour récupérer une donnée NOVA :

`GET /api/geodata/angled/ref/nova/{ref_nova}/` où `ref_nova` est la référence nova dont les `/` ont été remplacés par `_` (ex: `04_PFD_40156-01` pour `04/PFD/40156-01`)

Si cette référence nova n'existe pas, renvoie une page 404.


Attention pas de `POST`

##### FundingOrg (Organisme financeur)

Endpoint: `/api/geodata/angled/ref/funding_org/`

Modèle:

```json
{
    "id": 1,
    "name": "Communaute francaise"
}
```

Pour créer une entrée:

`POST /api/geodata/angled/ref/funding_org/` avec le json suivant:


```json
{
    "name": "Communaute francaise"
}
```

#### Locality (Read-Only)

Les locality sont Read-Only.

Requête SQL pour insérer quelques localites:

```sql
INSERT INTO angled_locality
(id, city, postcode, ins)
VALUES
(nextval('angled_locality_id_seq'), '{"fr":"Bruxelles", "nl": "Brussels"}', '1000', '21004'),
(nextval('angled_locality_id_seq'), '{"fr":"Laeken", "nl": "Laeken"}', '1020', '21004'),
(nextval('angled_locality_id_seq'), '{"fr":"Schaerbeek", "nl": "Schaerbeek"}', '1030', '21015'),
(nextval('angled_locality_id_seq'), '{"fr":"Evere", "nl": "Evere"}', '1140', '21006')
```

Endpoint: `/api/geodata/angled/ref/locality/`

```json
{
    "id": 1,
    "city": {
        "nl": "Brussels",
        "fr": "Bruxelles"
    },
    "postcode": "1000",
    "ins": "21004"
}
```

#### SchoolSite (Read-Only)

Les school site sont Read-Only.

Lien pour importer la liste des écoles (vavlable jusqu'au 20/11/2019): 

https://nextcloud.champs-libres.be/index.php/s/sgqFnRsHydzKrTe

Endpoint: `/api/geodata/angled/ref/school_site/`


##### Address

Endpoint: `/api/geodata/angled/ref/address/`

Modèle:

```json
{
    "id": 3,
    "locality": 1,
    "street":  {
        "nl": "Namenstraat",
        "fr": "Rue de Namur"
    },
    "number": "50"
}
```

Pour créer une entrée:

`POST /api/geodata/angled/ref/address/` avec le json suivant:

```json
{
    "locality": 1,
    "street":  {
        "nl": "Namenstraat",
        "fr": "Rue de Namur"
    },
    "number": "50"
}
```

##### Team & Teammembers

Permet de créer des team et leurs members (`TeamMember`).

Endpoint: `/apli/geodata/angled/ref/team/`

**Attention**: lors des mises à jour, la liste des membres est entièrement recréée (jusqu'à ce qu'on décide qu'on doit faire autrement :-)). Donc, les requêtes `PATCH` (Partial update) ne peuvent pas être utilisées.

Modèle:

```json
{
    "id": 1,
    "form": 3,
    "contact": null,
    "members": [
        {
            "id": 1,
            "role": null,
            "member": 2
        },
        {
            "id": 2,
            "role": null,
            "member": 1
        }
    ]
}
```

Requete de mise à jour:

`POST /api/geodata/angled/ref/team/`

Requete de création (pour l'équipe avec l'id 1):

`POST /api/geodata/angled/ref/team/1`


```json
{
    "form": 3,
    "contact": null,
    "members": [
        {
            "role": null,
            "member": 2
        },
        {
            "role": null,
            "member": 1
        }
    ]
}
```

## Serializers

### `serializers._utils.TimestampSerializer`

Permet de sérializer des dates python en timestamp * 1000
