from rest_framework import viewsets
from api.permissions import ViewSetWithPermissionsAndFilter
from angled.serializers.subscription import (
    ProjectSubscriptionSerializer,
    CircleSubscriptionSerializer,
    ProjectNotificationSerializer,
    CircleNotificationSerializer,
)
from angled.models.subscription import (
    ProjectSubscription,
    CircleSubscription,
    ProjectNotification,
    CircleNotification,
)


class ProjectSubscriptionViewSet(ViewSetWithPermissionsAndFilter):
    """
    Views for ProjectSubscription
    """
    serializer_class = ProjectSubscriptionSerializer
    queryset = ProjectSubscription.objects.all()


class CircleSubscriptionViewSet(ViewSetWithPermissionsAndFilter):
    """
    Views for CircleSubscription
    """
    serializer_class = CircleSubscriptionSerializer
    queryset = CircleSubscription.objects.all()


class ProjectNotificationViewSet(ViewSetWithPermissionsAndFilter):
    """
    Views for ProjectNotification
    """
    serializer_class = ProjectNotificationSerializer
    queryset = ProjectNotification.recent_objects.all()


class CircleNotificationViewSet(ViewSetWithPermissionsAndFilter):
    """
    Views for CircleNotification
    """
    serializer_class = CircleNotificationSerializer
    queryset = CircleNotification.recent_objects.all()
