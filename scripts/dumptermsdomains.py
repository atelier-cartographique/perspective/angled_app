from angled.models import ref
from angled.serializers.universe import TermSerializer, DomainSerializer
from rest_framework.renderers import JSONRenderer

terms = [TermSerializer(t).data for t in ref.Term.objects.all()]
domains = [DomainSerializer(d).data for d in ref.Domain.objects.all()]

with open('domains.json', 'w') as f:
    f.write(JSONRenderer().render(domains).decode('utf-8'))

with open('terms.json', 'w') as f:
    f.write(JSONRenderer().render(terms).decode('utf-8'))
