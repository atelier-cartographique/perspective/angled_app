from datetime import datetime
from rest_framework import serializers
from angled.models import dyn
import json
from angled.serializers.units import get_fast_serializer, Undefined, ser_unit_at
from angled.models.audience import get_readings
from angled.models import dyn
from angled.ui import UNITS_NAMES


class ProjectSerializer(serializers.Serializer):
    """
    serialize a project and his units.

    The data may be a `angled.models.dyn.Project` instance **or** a dict
    where keys are the unit names and values are the unit as dictionaries,
    **plus** an `id` key with the project's id as value.

    This allow to load data directly from the db, bypassing the ORM for
    performance reason (using raw sql)

    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        context = kwargs.get("context")
        self.request = context.get("request", None)
        if self.request is None:
            raise Exception("ProjectSerializer needs to be initialised with a request")
        self.readings = get_readings()

    def to_representation(self, instance):
        if isinstance(instance, dict):
            return self._to_representation_from_dict(instance)
        else:
            return self._to_representation_from_object(instance)

    def _to_representation_from_dict(self, instance):
        result = dict(id=instance["id"])
        # we pop items out of the dict, to allow garbage collector to clean memory :-)
        # so, we loop on the dict using the dict length as condition and use popitem()

        context = dict(request=self.request, readings=self.readings)

        while len(instance) > 0:
            key, content = instance.popitem()
            if key == "id":
                continue
            unit_serializer = get_fast_serializer(dyn.find_unit(key))
            is_multi = isinstance(content, list)
            u = unit_serializer(data=content, many=is_multi, context=context)
            if u.is_valid():
                try:
                    result[key] = u.data
                except Exception:
                    pass

        return result

    def _to_representation_from_object(self, instance):
        context = dict(request=self.request, readings=self.readings)
        pid = instance.id
        result = dict(id=pid)

        try:
            at_time = datetime.fromtimestamp(
                ((int(self.context.get("request").GET.get("at")) / 1000))
                + 10  # adding 10 seconds to account for the possible difference of recording time between transaction and units
            )
        except Exception:
            at_time = None

        for unit_name in UNITS_NAMES:
            unit = dyn.find_unit(unit_name)
            try:
                data = ser_unit_at(
                    at_time,
                    context,
                    unit,
                    instance,
                )
                result[unit_name] = data
            except Undefined:
                pass

        return result

    def create(self, validated_data):
        return dyn.Project.objects.create()


Point = dyn.get_unit_model("point")
Line = dyn.get_unit_model("line")
Polygon = dyn.get_unit_model("polygon")


class MissingGeometry(Exception):
    pass


def merge_geometries(geoms, gt):
    coordinates = [json.loads(g.unit.geom.geojson)["coordinates"] for g in geoms]
    return {"type": gt, "coordinates": coordinates}


# def project_to_geojson(instance, geometry_unit):
#     if "point" == geometry_unit:
#         geometry_type = "MultiPoint"
#         geometry_qs = list(Point.objects.get_current(instance))
#     elif "line" == geometry_unit:
#         geometry_type = "MultiLineString"
#         geometry_qs = list(Line.objects.get_current(instance))
#     elif "polygon" == geometry_unit:
#         geometry_type = "MultiPolygon"
#         geometry_qs = list(Polygon.objects.get_current(instance))
#     else:
#         raise TypeError(
#             "geometry_unit is point, line or polygon. Got {}".format(geometry_unit)
#         )

#     if len(geometry_qs) == 0:
#         raise MissingGeometry()

#     geometry = merge_geometries(geometry_qs, geometry_type)

#     properties = dict(id=instance.id)
#     for unit_name in instance.get_active_units():
#         if unit_name not in ["point", "line", "polygon"]:
#             unit = dyn.find_unit(unit_name)
#             try:
#                 properties[unit["name"]] = ser_unit(unit, datetime.now(), instance)
#             except Undefined:
#                 pass

#     return {
#         "type": "Feature",
#         "geometry": geometry,
#         "properties": properties,
#         "id": instance.id,
#     }
