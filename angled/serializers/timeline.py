from rest_framework import serializers
from angled.models import timeline
from angled.models import dyn
from angled.serializers import (
    _utils,
    units,
)


class EventTimelineSerializer(serializers.Serializer):
    """
    Serialize an EventTimeline
    """

    def to_representation(self, event):
        event_repr = {
            'id': event.id,
            'unit_name': event.unit_name,
            'created_at': _utils.TimestampField().to_representation(event.created_at)
        }
        if event.is_multi():
            data = []
            unit_serializer = units.make_unit_serializer(
                dyn.find_unit(event.unit_name))(
                    many=True, context=self.context)
            event_repr['data'] = unit_serializer.to_representation(
                event.multi_uis)
        return event_repr
