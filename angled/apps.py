from django.apps import AppConfig


class AngledConfig(AppConfig):
    name = 'angled'
    label = 'angled'
    verbose_name = 'AngledPerspective'
    geodata = True

    def ready(self):
        from .signals import connect
        connect()
