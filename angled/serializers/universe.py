from rest_framework import serializers

from lingua.serializers import LinguaRecordSerializer
from lingua.fields import message

from ..models import dyn
from ..models import ref
from ..models import Term


class KindSerializer(serializers.ModelSerializer):
    name = LinguaRecordSerializer()

    class Meta:
        model = ref.Kind
        fields = ['id', 'name']


class DomainSerializer(serializers.ModelSerializer):
    name = LinguaRecordSerializer()
    description = LinguaRecordSerializer(required=False, allow_null=True)

    class Meta:
        model = ref.Domain
        fields = ['id', 'name', 'kind', 'description']


class TermSerializer(serializers.ModelSerializer):
    name = LinguaRecordSerializer()
    description = LinguaRecordSerializer(required=False, allow_null=True)

    class Meta:
        model = ref.Term
        fields = ['id', 'name', 'domain', 'description', 'sort_index']

    def create(self, validated_data): 
        domain = validated_data.get('domain')
        name = validated_data.get('name')
        description = validated_data.get('description')
        termlist = Term.objects.filter(domain=domain)
        lastTerm = termlist[len(termlist)-1] 
        sort_index = lastTerm.sort_index
        sort_index += 1
        return Term.objects.create(name=name, domain=domain, description=description, sort_index=sort_index)




class DomainMappingSerializer(serializers.ModelSerializer):
    class Meta:
        model = ref.DomainFieldMapping
        fields = ['id', 'domain', 'unit', 'field']
        read_only_fields = [
            'id',
        ]
