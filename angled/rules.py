from itertools import product
from collections import namedtuple
from django.conf import settings
from django.db.models import Q
from rules import (
    add_perm,
    predicate,
    is_authenticated,
    always_allow,
)
from angled.models.audience import Audience
from api.rules import (
    view,
    change,
    add,
    delete,
    group_intersects,
)
from angled.models import (
    dyn,
    ref,
    query,
    subscription,
)
from angled.ui import UNITS

try:
    from django.contrib.auth.models import Group

    public_group_name = getattr(settings, "PUBLIC_GROUP")
    public_group = Group.objects.get(name="public")
    PUBLIC_AUDIENCE_ID = Audience.objects.get(group=public_group).id
except Exception:
    PUBLIC_AUDIENCE_ID = None


def get_unit_user(unit_instance):
    return unit_instance.user


@predicate()
def is_query_author(user, query):
    if user is not None:
        return user.id == query.user.id
    return False


@predicate()
def is_query_shared(_user, query):
    if query is not None:
        return query.share.count() > 0
    return False


@predicate()
def is_queryshare_author(user, queryshare):
    if user is not None:
        return user.id == queryshare.query.user.id
    return False


@predicate()
def is_subscriber(user, sub):
    if user is not None:
        return user.id == sub.user.id
    return False


@predicate()
def is_notification_subscriber(user, notif):
    if user is not None:
        return user.id == notif.subscription.user.id
    return False


def is_a_reader(reading_model):
    @predicate()
    def inner(user, reading):
        print("is_a_reader", user, reading)
        if user is not None and reading is not None:
            readings = reading_model.objects.filter(unit=reading.unit)
            it = group_intersects(
                user.groups.all(), [o.audience.group for o in readings]
            )
            print("==> ", it)
            return it
        print("==> ", False)
        return False

    return inner


def hook():
    add_perm(view(dyn.Project), always_allow)
    add_perm(add(dyn.Project), is_authenticated)

    add_perm(view(ref.Term), always_allow)
    add_perm(change(ref.Term), is_authenticated)
    add_perm(add(ref.Term), is_authenticated)
    add_perm(delete(ref.Term), is_authenticated)

    add_perm(view(ref.Domain), always_allow)
    add_perm(change(ref.Domain), is_authenticated)
    add_perm(add(ref.Domain), is_authenticated)
    add_perm(delete(ref.Domain), is_authenticated)

    add_perm(view(ref.Contact), always_allow)
    add_perm(change(ref.Contact), is_authenticated)
    add_perm(add(ref.Contact), is_authenticated)
    add_perm(delete(ref.Contact), is_authenticated)

    add_perm(view(ref.Team), always_allow)
    add_perm(change(ref.Team), is_authenticated)
    add_perm(add(ref.Team), is_authenticated)
    add_perm(delete(ref.Team), is_authenticated)

    add_perm(view(ref.DomainFieldMapping), always_allow)
    add_perm(change(ref.DomainFieldMapping), is_authenticated)
    add_perm(add(ref.DomainFieldMapping), is_authenticated)
    add_perm(delete(ref.DomainFieldMapping), is_authenticated)

    add_perm(view(query.Query), is_query_author | is_query_shared)
    add_perm(change(query.Query), is_query_author)
    add_perm(add(query.Query), is_authenticated)
    add_perm(delete(query.Query), is_query_author)

    add_perm(view(query.QueryShare), always_allow)
    add_perm(change(query.QueryShare), is_queryshare_author)
    add_perm(add(query.QueryShare), is_authenticated)
    add_perm(delete(query.QueryShare), is_queryshare_author)

    add_perm(view(subscription.ProjectSubscription), is_subscriber)
    add_perm(change(subscription.ProjectSubscription), is_subscriber)
    add_perm(add(subscription.ProjectSubscription), is_authenticated)
    add_perm(delete(subscription.ProjectSubscription), is_subscriber)

    add_perm(view(subscription.CircleSubscription), is_subscriber)
    add_perm(change(subscription.CircleSubscription), is_subscriber)
    add_perm(add(subscription.CircleSubscription), is_authenticated)
    add_perm(delete(subscription.CircleSubscription), is_subscriber)

    add_perm(view(subscription.ProjectNotification), is_notification_subscriber)
    add_perm(change(subscription.ProjectNotification), is_notification_subscriber)
    add_perm(delete(subscription.ProjectNotification), is_notification_subscriber)

    add_perm(view(subscription.CircleNotification), is_notification_subscriber)
    add_perm(change(subscription.CircleNotification), is_notification_subscriber)
    add_perm(delete(subscription.CircleNotification), is_notification_subscriber)

    for _, units in UNITS.items():
        for unit in units:
            unit_model = dyn.get_unit_model(unit["name"])
            add_perm(add(unit_model), is_authenticated)

            reading_model = dyn.get_reading_model(unit["name"])
            add_perm(add(reading_model), is_authenticated)
            add_perm(delete(reading_model), is_a_reader(reading_model))

            tag_model = dyn.get_tag_model(unit["name"])
            add_perm(add(tag_model), is_authenticated)
            add_perm(delete(tag_model), is_authenticated)


# << UNIT PERMISSIONS
# Even though we're not going to plug into
# the main mechanism of the SDI, we're still
# going to use `rules` for convenience and homogeneity

from rules import predicate

PredicateArgs = namedtuple("PredicateArgs", ["readings", "unit_name", "unit"])


def ids_intersect(a, b):
    for _ in filter(lambda t: t[0] == t[1], product(a, b)):
        return True
    return False


def get_user_group_ids(user):
    # At some point, Django kind of regressed in terms of caching queries,
    # or we did it somewhere, who knows...
    # But here we are, doing some caching to avoid as many DB requests
    # as we have units to check
    try:
        return user.__cached_groups
    except Exception:
        groups = [g.id for g in user.groups.all()]
        setattr(user, "__cached_groups", groups)
        return groups


def get_unit_groups(args):
    readings = args.readings
    unit_name = args.unit_name
    unit_id = args.unit("id")
    if unit_name in readings and unit_id in readings[unit_name]:
        return [r.group for r in readings[unit_name][unit_id]]
    return []


@predicate
def user_is_unit_audience(user, args):
    return ids_intersect(get_user_group_ids(user), get_unit_groups(args))


@predicate()
def user_is_unit_author(user, args):
    if user is not None:
        return user.id == args.unit("user")
    return False


@predicate()
def audience_is_public(user, args):
    if PUBLIC_AUDIENCE_ID is not None:
        readings = args.readings
        unit_name = args.unit_name
        unit_id = args.unit("id")
        if unit_name in readings and unit_id in readings[unit_name]:
            for reading in readings[unit_name][unit_id]:
                if reading.audience == PUBLIC_AUDIENCE_ID:
                    return True

    return False


unit_predicate = user_is_unit_author | user_is_unit_audience | audience_is_public


def can_read_unit(user, readings, unit_name, unit):
    """
    user       a User model

    readings   a dict of dicts, primary key is a unit name,
    secondary key is unit value and the leaf value is a list of ReadingTuple (see models.audience.get_readings)

    unit_name  an information unit name

    unit       an information unit dict or model instance
    """

    def unit_instance_getter(key, dflt=None):
        return getattr(unit, key, dflt)

    def unit_dict_getter(key, dflt=None):
        return unit.get(key, dflt)

    getter = unit_dict_getter if isinstance(unit, dict) else unit_instance_getter

    args = PredicateArgs(readings, unit_name, getter)
    result = unit_predicate(user, args)

    return result


# >> UNIT PERMISSIONS
