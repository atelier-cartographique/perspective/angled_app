import sys
from pathlib import PosixPath
from datetime import datetime
from angled.ui import UNITS

# générer le ui, par contre le ref est écrit à la main
# - MessageRecordIO -> un chaine de carac qui est multi langue
# integer -> pour les id

# def basic_field_datas():
#     # les données communes à chaque champ de db
#     return [
#         "id: io.Integer,",
#         "project: io.Integer,",
#         "user: IUserIO,",
#         "transaction: io.Integer,",
#         "eol: io.Integer,",
#         "created_at: io.Integer,",
#         r"tags: io.array(io.interface({id:io.Integer, tag:io.Integer})),",
#     ]

START_I = "io.interface({"
END_I = "}),"
START_A = "elems: io.array(" + START_I
END_A = "})),"

field_type_map = {
    'boolean': 'boolean',
    'date': 'string',
    'decimal': 'number',
    'fk': 'number',
    'label': 'MessageRecord',
    'month': 'string',
    'number': 'number',
    'raw_text': 'string',
    'term': 'number',
    'text': 'MessageRecord',
    'varchar': 'string',
    'year': 'number',
    'geometry': 'MultiPoint | MultiLineString | MultiPolygon',
}


def is_multi(name):
    for block in UNITS.values():
        for unit in block:
            if name == unit['name']:
                return unit['multi']


def get_fields(name):
    for block in UNITS.values():
        for unit in block:
            if name == unit['name']:
                return unit['fields']


def make_ts_name(base_name, suffix=''):
    return 'Unit' + base_name.title().replace('_', '') + suffix


class Generator:
    def __init__(self):
        self.tab = "    "
        self.references = set()  # write all the references tha we use
        self.header = []
        self.body = []
        self.tail = []
        self.generated_types = []
        self.generated_data_types = []

    @property
    def output_lines(self):
        return self.header + self.body + self.tail

    def get_tab(self, i=1):
        return self.tab * i

    def get_line(self, str, tab_level):
        """Get the line in a good format (good intenation + line return a the end)"""
        return "{}{}\n".format(self.get_tab(tab_level), str)

    def add_line(self, new_line, tab_level=0):
        self.body.append(self.get_line(new_line, tab_level))

    def add_lines(self, new_lines, tab_level=0):
        for nl in new_lines:
            self.add_line(nl, tab_level)

    def add_topline(self, new_line, tab_level=0):
        self.header.append(self.get_line(new_line, tab_level))

    def add_toplines(self, new_lines, tab_level=0):
        for nl in new_lines:
            self.add_topline(nl, tab_level=tab_level)

    def generate_import_definiton(self):
        """ Attention tjs le faire après ui2ts"""
        assert (len(self.generated_types))

        lines = [
            "import * as io from 'io-ts';",
            "import { fromNullable, Option } from 'fp-ts/lib/Option';",
            "import { MultiPointIO, MultiLineStringIO, MultiPolygonIO, MultiPoint, MultiLineString, MultiPolygon } from 'sdi/source/io/geojson';",
            "import { MessageRecordIO, MessageRecord } from 'sdi/source/io/io';",
            "// import { IUserIO } from 'sdi/source';",
        ]

        if self.references:
            lines.append(
                "import {{\n    {},\n}} from 'angled-core/ref';".format(
                    ',\n    '.join(self.references)))
            lines.append("")

        self.add_toplines(lines)

    def generate_meta_io(self):
        lines = [
            "const MetaIO = {}".format(START_I),
            self.tab + "id: io.Integer,",
            self.tab + "project: io.Integer,",
            self.tab + "user: io.Integer,",
            self.tab + "transaction: io.Integer,",
            self.tab + "eol: io.Integer,",
            self.tab + "created_at: io.Integer,",
            self.tab + "visible: io.boolean,",
            self.tab +
            r"readings: io.array(io.interface({id:io.Integer, unit:io.Integer,audience:io.Integer})),",
            self.tab +
            r"tags: io.array(io.interface({id:io.Integer, unit:io.Integer, tag:io.Integer})),",
            "});",
            "",
        ]

        self.add_lines(lines)

    def generate_global_type(self):
        tab_level = 0
        # all of them
        self.add_line("export const UnitIO = io.union([", tab_level)
        tab_level = 1
        for _, _, io_name in self.generated_types:
            self.add_line("{},".format(io_name), tab_level)
        self.add_line("]);")
        self.add_line("export type Unit = io.TypeOf<typeof UnitIO>;")
        self.add_line('')

        tab_level = 0
        # all of them (data)
        self.add_line("export const UnitDataIO = io.union([", tab_level)
        tab_level = 1
        for _, _, io_name in self.generated_data_types:
            self.add_line("{},".format(io_name), tab_level)
        self.add_line("]);")
        self.add_line("export type UnitData = io.TypeOf<typeof UnitDataIO>;")
        self.add_line('')

        # multi helper
        self.add_line(
            "export const isMulti = (name: InformationUnitName) => {")
        self.add_line("switch (name) {", 1)
        for ui_name, _, _ in self.generated_data_types:
            self.add_line(
                "case '{}': return {};".format(
                    ui_name, 'true' if is_multi(ui_name) else 'false'), 2)
        self.add_line("}", 1)
        self.add_line("};")

        self.add_line("export const makeDefaultData = ")
        self.add_line("(unitName: InformationUnitName) => {", 1)
        self.add_line("switch (unitName) {", 2)
        for ui_name, data_type_name, _ in self.generated_data_types:
            self.add_line(
                "case '{}': return default{}();".format(
                    ui_name, data_type_name), 3)
        self.add_line("}", 2)
        self.add_line("}", 1)

        self.add_line("export const updateFieldData = ")
        self.add_line(
            "(unitName: InformationUnitName, fieldName: string, unitData: Partial<UnitData> | null | undefined) => {",
            1)
        self.add_line(
            "const u = (unitData === null || unitData === undefined) ? makeDefaultData(unitName) : unitData",
            2)
        self.add_line("switch (unitName) {", 2)
        for ui_name, data_type_name, _ in self.generated_data_types:
            self.add_line("case '{}': {{".format(ui_name), 3)
            self.add_line(
                'if (\'{}\' === u.unit) {{'.format(make_ts_name(ui_name)), 4)
            for field in get_fields(ui_name):
                ft = field[0]
                fn = field[1]
                mapped_type = field_type_map[ft]
                if ft == 'geometry':
                    if 'point' == ui_name:
                        mapped_type = 'MultiPoint'
                    if 'line' == ui_name:
                        mapped_type = 'MultiLineString'
                    if 'polygon' == ui_name:
                        mapped_type = 'MultiPolygon'
                self.add_line(
                    'if (\'{0}\' === fieldName) {{ return (value: {2}) => ({{ ...u, {0}: value }});}}'
                    .format(fn, data_type_name, mapped_type), 5)
                # self.add_line(
                #     'if (\'{0}\' === fieldName) {{ return (f: (g: ((u:Partial{1} | null) => Partial{1})) => void) => (value: {2}) => f(u => u === null ? {{ ...(default{1}()), {0}: value }} : {{ ...u, {0}: value }});}}'
                #     .format(fn, data_type_name, mapped_type), 4)
            # self.add_line('return null;', 4)
            # self.add_line(
            #     'return (f: (g: ((u:Partial{0} | null) => Partial{0})) => void) => (_value: any) => f(u => u);'
            #     .format(data_type_name), 4)
            self.add_line("}", 4)
            self.add_line('return (_value: any) => u;', 4)
            self.add_line("}", 3)
        self.add_line("}", 2)
        self.add_line("}", 1)

        # all of them fashioned for API
        self.add_line('export const UnitDataPostIO = io.partial({')
        for ui_name, _, _ in self.generated_types:
            data_io = make_ts_name(ui_name, 'DataIO')
            if is_multi(ui_name):
                self.add_line(
                    "{}: io.interface({{new:io.array({}), removed: io.array(io.Integer)}}),"
                    .format(ui_name, data_io), 2)
            else:
                self.add_line("{}: {},".format(ui_name, data_io), 2)
        self.add_line("}, 'UnitDataPostIO');")
        self.add_line(
            "export type UnitDataPost = io.TypeOf<typeof UnitDataPostIO>;")

        self.add_line(
            r"export const UnitMetaPostIO = io.interface({audiences:io.array(io.number)});"
        )
        self.add_line(
            "export type UnitMetaPost = io.TypeOf<typeof UnitMetaPostIO>;")
        self.add_line(
            r"export const UnitPostIO = io.interface({units: UnitDataPostIO, meta: UnitMetaPostIO});"
        )
        self.add_line(
            "export type UnitPost = io.TypeOf<typeof UnitPostIO>;")

        # all of them in a project interface
        self.add_line('export const ProjectMembersIO = io.partial({')
        for ui_name, _, io_name in self.generated_types:
            if is_multi(ui_name):
                self.add_line("{}: io.array({}),".format(ui_name, io_name), 2)
            else:
                self.add_line("{}: {},".format(ui_name, io_name), 2)
        self.add_line("}, 'ProjectMembersIO');")

        self.add_line('export const ProjectIO = io.intersection([')
        self.add_line('io.readonly(io.interface({ id: io.number })),', 1)
        self.add_line('io.readonly(ProjectMembersIO),', 1)
        self.add_line("], 'ProjectIO');")
        self.add_line("export type Project = io.TypeOf<typeof ProjectIO>;")

        self.add_line(
            'export const getUnitbyName = (name: InformationUnitName, project: Project): Option<Unit| Unit[]> => {'
        )
        self.add_line('switch (name) {', 1)
        for ui_name, _, _ in self.generated_types:
            self.add_line(
                "case '{}': return fromNullable(project.{});".format(
                    ui_name, ui_name), 2)
        self.add_line('};', 1)
        self.add_line('};')

        # all names
        self.add_line('export const InformationUnitNameIO = io.union([')
        for ui_name, _, _ in self.generated_types:
            self.add_line("io.literal('{}'),".format(ui_name), 1)
        self.add_line(']);')
        self.add_line(
            'export type InformationUnitName = io.TypeOf<typeof InformationUnitNameIO>'
        )

        # for ui_name, _, _ in self.generated_types:
        #     self.add_line("| '{}'".format(ui_name), 1)
        # self.add_line(';', 1)

        # and finally all fields
        def ser_field(f):
            name = f[1]
            proc = f[0]
            args = ''  # list(map(lambda x: '{}'.format(x), f[1:]))
            if 'term' == proc:
                domains = f[2] if isinstance(f[2], list) else [f[2]]
                args = ", [{}]".format(', '.join(
                    list(map(lambda x: "'{}'".format(x), domains))))
            elif 'varchar' == proc:
                args = ', 128' if len(f) < 3 else ', {}'.format(f[2])
            elif 'fk' == proc:
                args = ", '{}'".format(
                    f[2]) if len(f) > 2 else ", '{}'".format(name)

            return "proc.{proc}('{name}'{args})".format(
                proc=proc, name=name, args=args)

        def ser(unit):
            return """
        {{
            name: '{name}' as InformationUnitName,
            multi: {multi},
            fields: [{fields}],
        }},""".format(
                name=unit['name'],
                multi='true' if unit['multi'] else 'false',
                fields=', '.join(list(map(ser_field, unit['fields']))))

        self.add_line("""
export interface UnitFieldProcessor<T> {
    fk(target:string, name:string): T;
    term(target:string, domains: string[]): T;
    label(target:string): T;
    text(target:string): T;
    raw_text(target:string): T;
    number(target:string): T;
    decimal(target:string): T;
    month(target:string): T;
    year(target:string): T;
    date(target:string): T;
    boolean(target:string): T;
    varchar(target:string, length:number): T;
    geometry(target:string): T;
}

export const fields = 
    <T>(proc: UnitFieldProcessor<T>) => [
        """)
        # self.add_line('const units = [{}]'.format(', '.join(map())))
        for block in UNITS.values():
            for unit in block:
                self.add_line(ser(unit))
        self.add_line('];')

        self.add_line('export type UnitFieldType = ')
        for v in field_type_map.keys():
            self.add_line('| \'{}\''.format(v), 1)
        self.add_line(';', 1)

        for a, b in field_type_map.items():
            self.add_line(
                'interface UnitField_{0} {{ kind: \'{0}\'; value: {1}; name: string; }}'
                .format(a, b))

        self.add_line('export type UnitField = ')
        for v in field_type_map.keys():
            self.add_line('| UnitField_{}'.format(v), 1)
        self.add_line(';', 1)

        def make_case(unit):
            unit_name = unit['name']
            uu_name = make_ts_name(unit_name)
            self.add_line("case '{}': {{".format(unit_name), 3)
            self.add_line("if('{}' === unit.unit){{".format(uu_name), 4)
            self.add_line('switch(fieldName){', 5)
            for f in unit['fields']:
                t = f[0]
                fname = f[1]
                self.add_line(
                    "case '{fname}': return unit.{fname} === undefined ? null : {{ value: unit.{fname}, kind: '{kind}', name: '{fname}' }};"
                    .format(fname=fname, kind=t, m=field_type_map[t]), 6)
                # self.add_line(
                #     "case '{fname}': return some(unit.{fname});".format(
                #         fname=fname), 6)
            self.add_line('default: return null;', 6)
            self.add_line('}', 5)
            self.add_line('} else { return null;}', 4)
            self.add_line('}', 3)

        self.add_line("""
export const getFieldValue = 
    (unitName: InformationUnitName, fieldName:string, unit:Partial<Unit| UnitData>): UnitField | null => {
        switch(unitName) {""")

        for block in UNITS.values():
            for unit in block:
                make_case(unit)

        self.add_line("""
        }
    }
""")
        # map unit names to unit type names
        self.add_line('export const nameToTypeName = {')
        for ui_name, ui_type_name, _ in self.generated_types:
            self.add_line("{}: '{}',".format(ui_name, ui_type_name), 1)
        self.add_line('};\n')

        # map unit type names to unit  names
        self.add_line('export const typeNameToName = {')
        for ui_name, ui_type_name, _ in self.generated_types:
            self.add_line(
                "{}: '{}' as InformationUnitName,".format(
                    ui_type_name, ui_name), 1)
        self.add_line('};\n')

    def get_reference_name(self, ref_name):
        # if ref_name == 'Nova':
        #     return 'io.string'
        if ref_name == 'auth.User':
            return 'IUserIO'
        else:
            ref_name = ref_name + "IO"
            self.references.add(ref_name)
            return ref_name

    def get_field_def(self, f):
        # conversion d'un champ f
        var_name = f[1]
        if f[0] == 'fk':
            type_name = 'io.number'  # self.get_reference_name(f[2])
        elif f[0] in ('label', 'text'):
            type_name = "MessageRecordIO"
        elif f[0] in ('raw_text', 'varchar'):
            type_name = "io.string"
        elif f[0] == 'term':
            type_name = "io.number"
            # type_name = self.get_reference_name('Term')
        elif f[0] in ('decimal', ):
            type_name = "io.number"
        elif f[0] == 'date':
            type_name = "io.string"
        elif f[0] == 'year':
            type_name = "io.Integer"
        elif f[0] == 'month':
            type_name = "io.string"
        elif f[0] == 'geometry':
            if f[2] == 'point':
                type_name = "MultiPointIO"
            elif f[2] == 'line':
                type_name = "MultiLineStringIO"
            if f[2] == 'polygon':
                type_name = "MultiPolygonIO"
        # todo ici geom
        else:
            type_name = "io." + f[0]
        return "{}: {},".format(var_name, type_name)

    def generate_unit(self, units):
        """ Generation de la declaration des UI"""
        for u in units:
            tab_level = 0
            ui_name = u['name']
            ui_type_name = make_ts_name(ui_name)
            ui_io_name = make_ts_name(ui_name, 'IO')
            data_type_name = make_ts_name(ui_name, 'Data')
            data_io_name = make_ts_name(ui_name, 'DataIO')

            self.add_line("export const {} =".format(data_io_name), 0)
            self.add_line("io.interface({", 1)
            self.add_line("unit: io.literal('{}'),".format(ui_type_name), 2)
            for f in u['fields']:
                self.add_line(self.get_field_def(f), 2)
            self.add_line("}}, '{}');".format(data_io_name), 1)
            self.add_line('export type {} = io.TypeOf<typeof {}>;'.format(
                data_type_name, data_io_name))
            self.add_line(
                "export type Partial{0} = Pick<{0}, 'unit'> & Partial<Pick<{0}, Exclude<keyof {0}, 'unit'>>>;"
                .format(data_type_name))
            self.add_line(
                "export const default{0} = ():Partial{0} => ({{ unit: '{1}' }});"
                .format(data_type_name, ui_type_name))

            self.add_line("export const {} =".format(ui_io_name), 0)
            tab_level += 1
            self.add_line("io.intersection([", tab_level)
            tab_level += 1
            self.add_line("MetaIO,", tab_level)
            self.add_line('{},'.format(data_io_name), tab_level)

            tab_level -= 1
            self.add_line("], '{}');".format(ui_io_name), tab_level)
            tab_level -= 1

            self.add_line("", tab_level)
            self.add_line(
                "export type {} = io.TypeOf<typeof {}>;".format(
                    ui_type_name, ui_io_name), tab_level)
            self.add_line("", tab_level)

            self.generated_types.append((ui_name, ui_type_name, ui_io_name))
            self.generated_data_types.append((ui_name, data_type_name,
                                              data_io_name))


# NOT USED
# def ref2ts(writer):
#     ret = ""
#     for ref in writer.references:
#         ret += ("export const {0} = i({{\n\
#     id: io.number,\n\
#     name: MessageRecordIO,\n\
# }});\n\
# \n\
# export type {1} = io.TypeOf<typeof {0}>;\n\
# \n").format("{}IO".format(ref.capitalize()), ref.capitalize())
#     return ret

if __name__ == '__main__':
    """ pour utiliser : python ui2ts.py path_to_type_script_files (where to generate)"""
    if len(sys.argv) < 2:
        print('Need a target directory, exiting')
        sys.exit(1)

    target = PosixPath(sys.argv[1]).joinpath('ui.ts')
    gen_comment = """/**
 *
 * Generated by `{generator}`
 * On  {date}
 * Might worth not editing
 *
 */
 
 """.format(
        generator=' '.join(sys.argv),
        date=datetime.now(),
    )

    g = Generator()
    g.add_topline(gen_comment)
    g.add_topline('// tslint:disable: variable-name')
    g.generate_meta_io()
    for u_name in UNITS:
        u = UNITS[u_name]
        g.generate_unit(u)
    g.generate_global_type()
    g.generate_import_definiton()
    g.add_line('')

    with target.open('w') as f:
        f.writelines(g.output_lines)
