# from django.contrib.postgres.fields import ranges
from datetime import datetime, date
from decimal import Decimal
import json
import time
from django.db import models
from django.contrib.gis.geos import GEOSGeometry, GEOSException
from django.contrib.gis.gdal import GDALException
from psycopg2.extras import Range, DateRange

from lingua.fields import LinguaRecord
from angled.models import ref
from angled.serializers._utils import year_to_range, month_to_range


class UnitField():
    """ A base class for unit fields
    """

    def __init__(self, name):
        self.field_name = name

    def get_attribute(self, instance):
        return instance.get(self.field_name)

    def to_internal_value(self, data):
        return data

    def to_representation(self, value, visible):
        value = self.to_internal_value(value)
        if visible:
            return self.to_visible_representation(value)
        return self.to_obfuscated_representation(value)

    def to_visible_representation(self, value):
        raise NotImplementedError()

    def to_obfuscated_representation(self, value):
        raise NotImplementedError()


def isrange(data):
    return isinstance(data, Range)


class YearField(UnitField):
    field_name = 'YearField'

    def to_internal_value(self, data):
        if isinstance(data, DateRange):
            return year_to_range(data.lower.year)
        return year_to_range(data)

    def to_visible_representation(self, value):
        low = value.lower

        return low.year

    def to_obfuscated_representation(self, value):
        return 0


class MonthField(UnitField):
    field_name = 'MonthField'

    def to_internal_value(self, data):
        if isinstance(data, DateRange):
            year = data.lower.year
            month = data.lower.month
        else:
            year, month = list(map(int, data.split('-')))

        return month_to_range(year, month)

    def to_visible_representation(self, value):
        low = value.lower

        return '{}-{:02d}'.format(low.year, low.month)

    def to_obfuscated_representation(self, value):
        return '0000-00'


class MessageField(UnitField):
    def to_visible_representation(self, value):
        if type(value) is str:
            return json.loads(value)
        elif isinstance(value, LinguaRecord):
            return value.to_dict()

        return value

    def to_obfuscated_representation(self, value):
        return {'fr': '', 'nl': '', 'en': ''}


class TermField(UnitField):

    def to_internal_value(self, data):
        return data

    def to_visible_representation(self, value):
        if type(value) is int:
            return value
        return value.id

    def to_obfuscated_representation(self, value):
        return 0


class UnitCharField(UnitField,):
    def to_visible_representation(self, value):
        return value

    def to_obfuscated_representation(self, value):
        return ''


class UnitIntegerField(UnitField):
    def to_visible_representation(self, value):
        # we'ge got foreign keys that use this serializer as well, so watch out
        if isinstance(value, models.Model):
            return value.pk
        return value

    def to_obfuscated_representation(self, value):
        return 0


class UnitDecimalField(UnitField):

    def to_visible_representation(self, value):
        return float(value)

    def to_obfuscated_representation(self, value):
        return 0.0


class UnitDateField(UnitField):
    def to_internal_value(self, data):
        if isinstance(data, int):
            return super().to_internal_value(
                datetime.fromtimestamp(data / 1000).date())
        elif isinstance(data, float):
            return super().to_internal_value(
                datetime.fromtimestamp(data).date())

        return super().to_internal_value(data)

    def to_visible_representation(self, value):
        data = value
        if type(data) == date:
            return data.isoformat()
        elif isinstance(data, (int, float)):
            return round(data * 1000)
        return data

    def to_obfuscated_representation(self, value):
        # return int(datetime.now().timestamp()) * 1000
        return '0000-00-00'


class UnitBooleanField(UnitField):
    def to_visible_representation(self, value):
        return value

    def to_obfuscated_representation(self, value):
        return False


class UnitGeometryField(UnitField):
    def to_internal_value(self, value):
        if value == '' or value is None:
            return value
        if isinstance(value, GEOSGeometry):
            # value already has the correct representation
            return value
        if isinstance(value, dict):
            value = json.dumps(value)

        return GEOSGeometry(value)

    def to_visible_representation(self, value):
        return json.loads(value.geojson)

    def to_obfuscated_representation(self, value):
        initial = json.loads(value.geojson)
        return dict(type=initial['type'], coordinates=[], properties={})
