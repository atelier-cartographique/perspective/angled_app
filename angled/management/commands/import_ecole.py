import re
from datetime import date
from django.utils.text import slugify
from django.contrib.auth.models import User
from django.contrib.gis.geos import GEOSGeometry
from angled.models import ref
from angled.models import dyn

from angled.management.import_base import (
    ImportCommand,
    ReaderType,
    UnitError,
    UnitResult,
    UnitSkip,
    MultiResult,
    get_cell_name,
    get_val,
    get_label,
    get_text,
    parse_int,
    parse_money,
    get_contact,
    make_year,
)

# [(d[0].lower(), d[0]) for d in cur.description]
UNITS = (
    ('adpnid', 'adpnid'),
    ('adptid', 'adptid'),
    ('commune_spw', 'commune_spw'),
    ('dat_modif', 'dat_modif'),
    ('date_creat', 'date_creat'),
    ('debut_hors_temp', 'debut_hors_temp'),
    ('decompte_proj', 'decompte_proj'),
    ('delta_spw', 'delta_spw'),
    ('extens_creat', 'extens_creat'),
    ('fin_autres', 'fin_autres'),
    ('fin_montant', 'fin_montant'),
    ('financ_concatene', 'financ_concatene'),
    ('fr_nl', 'fr_nl'),
    ('fwb_dat', 'fwb_dat'),
    ('fwb_numdos', 'fwb_numdos'),
    ('geometry', 'GEOMETRY'),
    ('id_code_parcelle', 'id_code_parcelle'),
    ('id_commune', 'id_commune'),
    ('id_fwb_stade', 'id_fwb_stade'),
    ('id_mp_procedure', 'id_mp_procedure'),
    ('id_nom_ecole', 'id_nom_ecole'),
    ('id_projet', 'id_projet'),
    ('mdid', 'mdid'),
    ('mdnafn', 'mdnafn'),
    ('modul', 'modul'),
    ('mp_attr', 'mp_attr'),
    ('mp_compt', 'mp_compt'),
    ('mp_csc', 'mp_csc'),
    ('mp_jours', 'mp_jours'),
    ('mp_notif', 'mp_notif'),
    ('mp_type', 'mp_type'),
    ('nat2000', 'nat2000'),
    ('niv', 'niv'),
    ('niveau_spw', 'niveau_spw'),
    ('nom_projet', 'nom_projet'),
    ('non_suppl', 'non_suppl'),
    ('num_spw', 'num_spw'),
    ('num', 'num'),
    ('ogc_fid', 'OGC_FID'),
    ('ord_spec', 'ord_spec'),
    ('pe_accusrecep', 'pe_accusrecep'),
    ('pe_concerta', 'pe_concerta'),
    ('pe_datdos', 'pe_datdos'),
    ('pe_delivre', 'pe_delivre'),
    ('pe_faitgener', 'pe_faitgener'),
    ('pe_numdos', 'pe_numdos'),
    ('plac_debut', 'plac_debut'),
    ('plac_echeance', 'plac_echeance'),
    ('plac_fond_conso', 'plac_fond_conso'),
    ('plac_fondamental', 'plac_fondamental'),
    ('plac_hors_temp', 'plac_hors_temp'),
    ('plac_non_defini', 'plac_non_defini'),
    ('plac_non_sup', 'plac_non_sup'),
    ('plac_ouverture', 'plac_ouverture'),
    ('plac_remarq', 'plac_remarq'),
    ('plac_sec_conso', 'plac_sec_conso'),
    ('plac_secondaire', 'plac_secondaire'),
    ('plac_temp', 'plac_temp'),
    ('places_mat', 'places_mat'),
    ('places_prim', 'places_prim'),
    ('po', 'po'),
    ('pu_accus_incplet', 'pu_accus_incplet'),
    ('pu_accusrecep', 'pu_accusrecep'),
    ('pu_art191_dem', 'pu_art191_dem'),
    ('pu_concerta', 'pu_concerta'),
    ('pu_datdos', 'pu_datdos'),
    ('pu_delivre', 'pu_delivre'),
    ('pu_gestionnaire', 'pu_gestionnaire'),
    ('pu_numdos', 'pu_numdos'),
    ('pu_remarques', 'pu_remarques'),
    ('remarques', 'remarques'),
    ('reso', 'reso'),
    ('sdid', 'sdid'),
    ('sec', 'sec'),
    ('seveso', 'seveso'),
    ('siamu_avis', 'siamu_avis'),
    ('siamu_datavis', 'siamu_datavis'),
    ('siamu_datdos', 'siamu_datdos'),
    ('siamu_numdos', 'siamu_numdos'),
    ('sit_clas', 'sit_clas'),
    ('solpolu_inv', 'solpolu_inv'),
    ('solpolu_res', 'solpolu_res'),
    ('statut_spw', 'statut_spw'),
    ('statut', 'statut'),
    ('temp', 'temp'),
    ('typ_operation', 'typ_operation'),
    ('voirie_spw', 'voirie_spw'),
    ('voirie', 'voirie'),
    ('x', 'x'),
    ('y', 'y'),
)

CELL_NAMES = [(cell, model) for model, cell in UNITS]

cell_name = get_cell_name(UNITS)
val = get_val(UNITS)
label = get_label(UNITS)
text = get_text(UNITS)


def status(row):
    v = val(row, 'statut')
    if len(v) == 0:
        return UnitSkip()

    try:
        return MultiResult(
            UnitResult(status=dyn.DomainReg.term('status_ecole', v)))
    except Exception:
        return UnitError(cell_name('status'), 'no term {}'.format(v))


def construction_type(row):
    v = val(row, 'typ_operation')
    if len(v) == 0:
        return UnitSkip()
    mv = v.split('-')
    results = []
    for v in mv:
        if len(v.strip()) > 0:
            try:
                results.append(
                    UnitResult(construction_type=dyn.DomainReg.term(
                        'construction_type', v)))
            except Exception:
                return UnitError(cell_name('typ_operation'),
                                 'no term for {} '.format(v))
    return MultiResult(results)


SEAT_SQL = """
select p.places, a.annee 
from (select distinct id, places, idannee, idprojet from j_projets_places_{level}_suppl)   p 
   left join (select distinct id, annee from d_annees ) a on p.idannee = a.id  
where idprojet = {project};
"""

FIN_SQL = """
select f.financ, p.fin_montant, p.fin_an_prog
from (select distinct id, idfinanc, idprojet, fin_montant, fin_an_prog from j_projet_financ)  p 
   left join (select distinct id, financ from d_financ)  f on p.idfinanc = f.id  
where idprojet = {project};
"""

COMMUNE_SQL = """
select commune from d_commune where id = {id};
"""

UNKNOWN_YEAR = 1


def parse_year(s):
    if 'Inconnue' == s:
        return make_year(UNKNOWN_YEAR)  # yeah, I know, this aint pretty
    ss = s.split('-')
    if len(ss) == 1:
        return make_year(int(ss[0]))

    return make_year(int(ss[0]), int(ss[1]))


def seats(row, cursor):
    id = val(row, 'id_projet')

    # print(SEAT_SQL.format(project=id, level='fond'))

    fond = cursor.execute(SEAT_SQL.format(project=id, level='fond')).fetchall()
    sec = cursor.execute(SEAT_SQL.format(project=id, level='sec')).fetchall()

    ret = []
    for row in fond:
        ret.append(
            UnitResult(quantity=row[0],
                       school_level_basic=dyn.DomainReg.term(
                           'Niveau scolaire', 'Fondamental'),
                       opening=parse_year(row[1])))

    for row in sec:
        ret.append(
            UnitResult(quantity=row[0],
                       school_level_basic=dyn.DomainReg.term(
                           'Niveau scolaire', 'Secondaire'),
                       opening=parse_year(row[1])))

    if len(ret) == 0:
        return UnitSkip()

    return MultiResult(ret)


def get_funding_org(name):
    # print('get_funding_org', name)
    try:
        return ref.FundingOrg.find_by_name(name)
    except ref.FundingOrg.DoesNotExist:
        return ref.FundingOrg.objects.create(name=name)


def funding(row, cursor):
    id = val(row, 'id_projet')

    # print(FIN_SQL.format(project=id))
    fs = cursor.execute(FIN_SQL.format(project=id)).fetchall()

    ret = []
    for f in fs:
        name = f[0]
        if name is None:
            continue
        org = get_funding_org(name)
        try:
            amount = f[1] + 0.0
        except Exception:
            amount = 0.0
        try:
            y = make_year(int(f[2]))
        except Exception:
            y = make_year(UNKNOWN_YEAR)

        ret.append(UnitResult(org=org, grant_year=y, amount=amount))

    if len(ret) == 0:
        return UnitSkip()

    return MultiResult(ret)


def point(row):
    x = val(row, 'x')
    y = val(row, 'y')
    if x and y and (x > 0) and (y > 0):
        return UnitResult(geom='SRID=31370;MULTIPOINT (({} {}))'.format(x, y))
    return UnitSkip()


def school_site(row):
    p = point(row)
    if isinstance(p, UnitSkip):
        return p

    try:
        geom = GEOSGeometry(p.args['geom'])
        site = ref.SchoolSite.objects.filter(
            point__distance_lt=(geom, 32)).only('pk').first()
        return UnitResult(site=site.pk)
    except Exception as ex:
        return UnitError('school_site', '{}'.format(ex))


def school_info(row):
    p = point(row)
    if isinstance(p, UnitSkip):
        return p

    try:
        geom = GEOSGeometry(p.args['geom'])
        site = ref.SchoolSite.objects.filter(
            point__distance_lt=(geom, 32)).only(
                'lang', 'level', 'kind', 'reseau', 'point').first()

        # sectors = []
        # if site.general is not None:
        #     sectors.append('general')
        # if site.technique is not None:
        #     sectors.append('technique')
        # if site.technique_q is not None:
        #     sectors.append('technique (q)')
        # if site.profession is not None:
        #     sectors.append('professionel')
        # if site.artistique is not None:
        #     sectors.append('artistique')
        # if site.artistique_q is not None:
        #     sectors.append('artistique (q)')
        # if site.aso is not None:
        #     sectors.append('general')
        # if site.tso is not None:
        #     sectors.append('technique')
        # if site.bso is not None:
        #     sectors.append('professionel')
        # if site.kso is not None:
        #     sectors.append('artistique')

        # # print('sectors => ', sectors)

        # if len(sectors) > 0:
        #     sector = dyn.DomainReg.term('Filière', ' + '.join(sectors))
        # else:
        #     sector = dyn.DomainReg.term('Filière', 'non-precise')

        return UnitResult(
            lang=dyn.DomainReg.term('school lang', site.lang),
            level=dyn.DomainReg.term('school level', site.level),
            type=dyn.DomainReg.term('school type', site.kind),
            network=dyn.DomainReg.term('school network', site.reseau),
        )
    except Exception as ex:
        return UnitError('school_info', '{}'.format(ex))


type_po = ref.Term.objects.get(pk=379)


def school_po(row):
    p = point(row)
    if isinstance(p, UnitSkip):
        return p

    try:
        geom = GEOSGeometry(p.args['geom'])
        site = ref.SchoolSite.objects.filter(
            point__distance_lt=(geom,32)).only('organizer').first()
        contact = get_contact(site.organizer)
        return MultiResult(UnitResult(contact=contact, type=type_po))

    except Exception as ex:
        return UnitError('school_po', '{}'.format(ex))


TRACKS = {
    'DOA': ref.Term.objects.get(pk=396),
    'GEN': ref.Term.objects.get(pk=403),
    'TECH': ref.Term.objects.get(pk=391),
    'PROF': ref.Term.objects.get(pk=393),
    'ASO': ref.Term.objects.get(pk=403),
}


def school_track(row):
    tracks = val(row, 'sec').split('+')

    if len(tracks) == 0:
        return UnitSkip()

    return MultiResult(
        [UnitResult(track=TRACKS[t]) for t in tracks if t != ''])


def town(row):
    p = point(row)
    if isinstance(p, UnitSkip):
        return p

    try:
        geom = GEOSGeometry(p.args['geom'])
        site = ref.SchoolSite.objects.filter(
            point__distance_lt=(geom, 32)).only('locality').first()

        return UnitResult(name=dyn.DomainReg.term('Commune', site.locality))

    except Exception as ex:
        return UnitError('town', '{}'.format(ex))


note_type = ref.Term.objects.get(pk=49)


def remarques(row):
    d = val(row, 'remarques')
    if len(d) > 0:
        return MultiResult([UnitResult(type=note_type, body=d)])

    return UnitSkip()


def district(row):
    d = val(row, 'mdnafn')
    if len(d) > 0:
        try:
            return UnitResult(name=dyn.DomainReg.term('Quartier', d))
        except Exception as ex:
            return UnitError('mdnafn', 'could not term district {}'.format(d))

    return UnitSkip()


def temporary(row):
    v = val(row, 'temp')

    return UnitResult(value=len(v) > 0)


def school_creation(row):
    v = val(row, 'extens_creat').lower()

    if v.startswith('cr'):
        return UnitResult(creation=True)
    elif v.startswith('ex'):
        return UnitResult(creation=False)

    return UnitSkip()


def school_take_off(row):
    try:
        n = float(val(row, 'plac_non_sup'))
        if n > 0:
            # get year
            y = parse_year(val(row, 'plac_echeance'))
            return UnitResult(amount=n, opening=y)
        else:
            return UnitSkip()
    except Exception:
        return UnitSkip()


nova_re = re.compile(r'^\d+/\w+/(\d+)$')


def nova(row):
    v = val(row, 'pu_numdos')
    m = nova_re.match(v)
    if m is not None:
        try:
            gs = m.groups()
            return MultiResult(UnitResult(nova=gs[0]))
        except Exception as ex:
            return UnitError(
                cell_name('pu_numdos'),
                'Failed to extract a novaseq from {}:\n{}'.format(v, ex))

    return UnitSkip()


def address(row, cursor):
    # pc = val(row, 'post_code')
    cid = val(row, 'id_commune')
    sna = val(row, 'voirie')
    snu = val(row, 'num')
    try:
        row = cursor.execute(COMMUNE_SQL.format(id=cid)).fetchone()
        text = "{} {}\n{}".format(sna, snu, row[0])
        return UnitResult(address=dict(fr=text, nl=''))
    except Exception:
        return UnitSkip()


manager_type = ref.Term.objects.get(pk=336)
anne = User.objects.get(username='adujardin')


def manager(row):
    return MultiResult(UnitResult(context=manager_type, manager_user=anne))


class Command(ImportCommand):
    reader_type = ReaderType.SQLITE
    sqlite_tablename = 'projets_ecoles'
    source_id_field = 'id_projet'

    def process_row(self, row, create_unit):

        create_unit('name', label(row, 'nom_projet', 'name'))
        create_unit(
            'program',
            MultiResult([
                UnitResult(component=dyn.DomainReg.term('equipment', 'École'))
            ]))
        create_unit('status', status(row))
        create_unit('construction_type', construction_type(row))
        create_unit('school_seats', seats(row, self.cursor))
        create_unit('funding', funding(row, self.cursor))
        create_unit('temporary', temporary(row))
        create_unit('point', point(row))
        create_unit('school_site', school_site(row))
        create_unit('school_info', school_info(row))
        create_unit('actor', school_po(row))
        create_unit('town', town(row))
        create_unit('district', district(row))
        create_unit('note', remarques(row))
        create_unit('school_creation', school_creation(row))
        create_unit('school_take_off', school_take_off(row))
        create_unit('nova', nova(row))
        create_unit('address', address(row, self.cursor))
        create_unit('school_track', school_track(row))
        create_unit('manager', manager(row))
