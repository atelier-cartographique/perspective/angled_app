from pathlib import PosixPath

from angled import enums
from angled.models import project
from angled.models.audience import Audience
from angled.models.ref import Domain, Tag, Term, label, text
from angled.ui import UNITS
from django.apps import apps
from django.contrib.admin import ModelAdmin, site
from django.contrib.auth.models import User
from django.contrib.gis.db import models
from django.contrib.postgres.fields import ranges
from django.db.models import Q


def fk(field_name, target, **kwargs):
    return models.ForeignKey(
        target,
        on_delete=models.CASCADE,
        related_name="{}_{}".format(target, field_name).replace(".", "_").lower(),
        **kwargs
    )


class DomainReg:
    _inited = False
    _domain_cache = dict()
    _term_cache = dict()

    @classmethod
    def read_cache(cls, root_dir):
        if cls._inited:
            return
        cls._inited = True

        domain_path = PosixPath(root_dir).absolute().joinpath("domains.cache")
        term_path = PosixPath(root_dir).absolute().joinpath("terms.cache")
        try:
            with domain_path.open() as fdc:
                for line in fdc:
                    try:
                        pk, name = list(map(lambda x: x.strip(), line.split(":")))
                        domain = Domain.objects.get(pk=int(pk))
                        cls._domain_cache[name] = domain
                    except Exception:
                        pass
        except FileNotFoundError:
            print('"{}" not found, skipping'.format(domain_path))

        try:
            with term_path.open() as fdc:
                for line in fdc:
                    try:
                        pk, name = list(map(lambda x: x.strip(), line.split(":")))
                        term = Term.objects.get(pk=int(pk))
                        cls._term_cache[name] = term
                    except Exception:
                        pass
        except FileNotFoundError:
            print('"{}" not found, skipping'.format(term_path))

    @classmethod
    def write_cache(cls, root_dir):
        domain_path = PosixPath(root_dir).absolute().joinpath("domains.cache")
        term_path = PosixPath(root_dir).absolute().joinpath("terms.cache")
        domain_lines = [
            "{}:{}".format(domain.id, name)
            for name, domain in cls._domain_cache.items()
        ]
        term_lines = [
            "{}:{}".format(term.id, name) for name, term in cls._term_cache.items()
        ]

        nl = "\n"

        with domain_path.open("w") as fdc:
            fdc.write(nl.join(domain_lines))

        with term_path.open("w") as fdc:
            fdc.write(nl.join(term_lines))

    @classmethod
    def reg(cls, name):
        pass
        # if isinstance(name, str):
        #     DomainReg._impl_reg(name)
        # elif isinstance(name, list):
        #     for n in name:
        #         DomainReg._impl_reg(n)

    @classmethod
    def _get_term(cls, name):
        dom = cls._domain_cache.get(name)
        if dom is None:
            try:
                dom = Domain.find_by_name(name)
            except Domain.DoesNotExist:
                print('Domain "{}" does not exists'.format(name))
                print("pick from domains")
                for d in Domain.objects.all().order_by("name__fr"):
                    print(
                        "[{}]\t{}".format(d.id, d.name.get("fr", "--- not named ---"))
                    )
                print('Domain "{}" does not exists'.format(name))
                pk = int(input("Which one? "))
                dom = Domain.objects.get(pk=pk)

            cls._domain_cache[name] = dom

        def term(term_name):
            fullname = "{}.{}".format(name, term_name)
            t = cls._term_cache.get(fullname)
            if t is None:
                try:
                    t = Term.find_by_name(dom, term_name)
                except Term.DoesNotExist:
                    print(
                        'Term "{}" in Domain "{}" does not exists'.format(
                            term_name,
                            name,
                        )
                    )
                    for term in Term.objects.filter(domain=dom):
                        print(
                            "[{}] {}".format(
                                term.id, term.name.get("fr", "--- not named ---")
                            )
                        )
                    print(
                        'Term "{}" in Domain "{}" does not exists'.format(
                            term_name,
                            name,
                        )
                    )
                    pk = int(input("Which one? "))
                    t = Term.objects.get(pk=pk)

                cls._term_cache[fullname] = t

            return t

        return term

    @classmethod
    def term(cls, domain, term):
        return cls._get_term(domain)(term)


def term(field_name, domain):
    DomainReg.reg(domain)
    domain_name = domain
    if isinstance(domain, list):
        domain_name = domain[0]
    return models.ForeignKey(
        "angled.Term",
        on_delete=models.CASCADE,
        related_name="of_{}_{}".format(domain_name, field_name),
    )


def create_label(field_name):
    return label(field_name)


def create_text(field_name):
    return text(field_name)


def raw_text(
    field_name,
):
    return models.TextField()


def number(
    field_name,
):
    return models.IntegerField()


def decimal(field_name, precision=2):
    return models.DecimalField(max_digits=16, decimal_places=precision)


def month(
    field_name,
):
    return ranges.DateRangeField()


def year(
    field_name,
):
    return ranges.DateRangeField()


def date(
    field_name,
):
    return models.DateField()


def boolean(
    field_name,
):
    return models.BooleanField()


def enum(field_name, name):
    try:
        e = getattr(enums, name)
    except AttributeError:
        raise enums.MissingEnumeration(name)
    return models.CharField(max_length=32, choices=enums.choices_from_enum(e))


def varchar(field_name, length=128):
    return models.CharField(max_length=length)


def geometry(field_name, geometry_type):
    if "point" == geometry_type:
        return models.MultiPointField(srid=31370)
    elif "line" == geometry_type:
        return models.MultiLineStringField(srid=31370)
    elif "polygon" == geometry_type:
        return models.MultiPolygonField(srid=31370)
    raise ValueError(
        'Expected geometry type of point, line or polygon, got "{}"'.format(
            geometry_type
        )
    )


class Project(models.Model):
    id = models.AutoField(primary_key=True)
    created_at = models.DateTimeField(auto_now_add=True)
    objects = models.Manager()
    timeline = project.TimelineManager()
    full_loader = project.FullLoaderManager()

    def __str__(self):
        return "Project #{}".format(self.id)

    # def get_active_units(self, at=None):
    #     """
    #     get a list of units names which are in use for the current project
    #     """
    #     if at is None:
    #         at = datetime.now()
    #     _unique_sql = "SELECT '{unit_name}', COALESCE((SELECT true AS is_present FROM {unit_table} where project_id = {:d} and created_at <= to_timestamp('{date}') LIMIT 1), FALSE)"
    #     sqls = []
    #     for unit_name in UNITS_NAMES:
    #         model = get_unit_model(unit_name)
    #         sqls.append(
    #             _unique_sql.format(
    #                 self.id,
    #                 unit_name=unit_name,
    #                 unit_table=model._meta.db_table,
    #                 date=at.timestamp(),
    #             )
    #         )
    #     with connection.cursor() as cursor:
    #         cursor.execute(" UNION ".join(sqls))
    #         results = [row[0] for row in cursor.fetchall() if row[1]]
    #     return results


class Transaction(models.Model):
    id = models.AutoField(primary_key=True)
    created_at = models.DateTimeField(auto_now_add=True)
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return "Transaction #{}".format(self.id)


class BaseUnit(models.Model):
    """Base unit field"""

    STATUS_DELETED = "d"
    STATUS_ARCHIVED = "a"
    STATUS_CURRENT = "c"

    UNIT_STATUS = (
        (
            STATUS_DELETED,
            "deleted",
        ),
        (
            STATUS_ARCHIVED,
            "archived",
        ),
        (
            STATUS_CURRENT,
            "current",
        ),
    )

    class Meta:
        abstract = True
        indexes = [models.Index(fields=["project", "created_at"])]

    id = models.AutoField(primary_key=True)
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    transaction = models.ForeignKey(Transaction, on_delete=models.CASCADE)
    eol = models.DateTimeField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    unit_status = models.CharField(
        max_length=1, choices=UNIT_STATUS, blank=True, null=True
    )

    def __str__(self):
        return "{}({})".format(self._meta.verbose_name, self.project.id)


class ListIndex(models.Model):
    """CREATE TABLE "list_index" (
    "id"      serial PRIMARY KEY,
    "ts"      timestamp with timezone  not null
    "transaction" transaction_id       not null
    );
    """

    id = models.AutoField(primary_key=True)
    ts = models.DateTimeField(auto_now_add=True)
    unit = models.CharField(max_length=50, null=True)
    transaction = models.ForeignKey(Transaction, on_delete=models.CASCADE)


def make_multi_manager(unit_name, model):
    """
    Create a Manager adapted to the model of the UI
    """

    class MManager(models.Manager):
        """
        Manager for "multi" UI
        """

        def get_units_at(self, project, at):
            """
            Return the current "multi" for the given project.

            Current multi are the ones inserted / updated during the last manipulation
            The parameter `at` (datetime) get the current state at a given time.
            """
            if at is None:
                return self.get_units(project)

            instance = (
                self.filter(
                    index__unit=unit_name,
                    index__ts__lte=at,
                    unit__project=project,
                )
                .order_by("-index__ts")
                .first()
            )

            if instance is None:
                return []

            return [m.unit for m in self.filter(index=instance.index)]

        def get_units(self, project):
            # the set comes from units can be current *and* referenced in several genreations of the list
            return list(
                set(
                    [
                        m.unit
                        for m in self.filter(
                            unit__unit_status=BaseUnit.STATUS_CURRENT,
                            index__unit=unit_name,
                            unit__project=project,
                        )
                    ]
                )
            )

    return MManager


class BaseMulti(models.Model):
    """
    Multi

    CREATE TABLE "list_{name}" (
        "id"     serial PRIMARY KEY,
        "index"  references "list_index" (id) not null,
        "unit"   references "info_unit_{name}" (id) not null
    );
    """

    class Meta:
        abstract = True

    id = models.AutoField(primary_key=True)
    index = models.ForeignKey(ListIndex, on_delete=models.CASCADE, related_name="+")


FIELD_TYPES = {
    "fk": fk,
    "label": create_label,
    "text": create_text,
    "raw_text": raw_text,
    "number": number,
    "decimal": decimal,
    "month": month,
    "year": year,
    "date": date,
    "boolean": boolean,
    "enum": enum,
    "varchar": varchar,
    "term": term,
    "geometry": geometry,
}


def make_type(unit_name, name, fields):

    attrs = dict(
        Meta=type(
            "Meta",
            (),
            dict(
                verbose_name=name,
                verbose_name_plural=name,
                app_label="angled",
                ordering=["-created_at"],
            ),
        ),
        __module__="angled.models.dyn",
    )

    for field in fields:
        fn = field[0]
        fieldname = field[1]
        try:
            attrs[fieldname] = FIELD_TYPES[fn](*(field[1:]))
        except Exception as ex:
            print("Failed on field: {}".format(fieldname))
            print("\terror: {}".format(ex))
            raise ex

    cls = type(unit_name, (BaseUnit,), attrs)

    return cls


def make_multi(name, model):
    attrs = dict(
        Meta=type("Meta", (), dict(app_label="angled")),
        __module__="angled.models.dyn",
        unit=models.ForeignKey(model, on_delete=models.CASCADE, related_name="+"),
    )
    attrs["objects"] = make_multi_manager(name, model)()

    cls = type("multi_{}".format(name), (BaseMulti,), attrs)

    return cls


def tagged(unit_model):
    """Create a model to host tags for a unit"""

    unit_model_name = unit_model._meta.verbose_name
    attrs = dict(
        Meta=type(
            "Meta",
            (),
            dict(
                app_label="angled",
            ),
        ),
        __module__="angled.models.dyn",
    )

    class Tagged(models.Model):
        """Join table for tags"""

        class Meta:
            abstract = True
            unique_together = [["tag", "unit"]]

        id = models.AutoField(primary_key=True)
        tag = models.ForeignKey(
            Tag,
            on_delete=models.CASCADE,
            related_name="unit_tag_{}".format(unit_model_name),
        )
        unit = models.ForeignKey(
            unit_model,
            on_delete=models.CASCADE,
            related_name="tagged_{}".format(unit_model_name.lower()),
        )

    return type("Tagged_{}".format(unit_model_name), (Tagged,), attrs)


def reading(unit_model):
    """Create a model to host readings (permissions) for a unit"""

    unit_model_name = unit_model._meta.verbose_name
    attrs = dict(
        Meta=type(
            "Meta",
            (),
            dict(
                app_label="angled",
            ),
        ),
        __module__="angled.models.dyn",
    )

    class Reading(models.Model):
        """Join table for Readings"""

        class Meta:
            abstract = True
            unique_together = [["audience", "unit"]]

        id = models.AutoField(primary_key=True)
        audience = models.ForeignKey(
            Audience,
            on_delete=models.CASCADE,
            related_name="unit_audience_{}".format(unit_model_name),
            default=0,  # make migration easier
        )
        unit = models.ForeignKey(
            unit_model,
            on_delete=models.CASCADE,
            related_name="readings",
        )

    model = type("Reading_{}".format(unit_model_name), (Reading,), attrs)
    # site.register(model)
    return model


class UnitAdmin(ModelAdmin):
    list_display = ("id", "project", "user")
    # autocomplete_fields = ('project', 'transaction')


def register_admin(name, mod, fields):
    adm = type(
        "Admin_{}".format(name),
        (UnitAdmin,),
        dict(
            readonly_fields=["project", "transaction", "user"] + [f[1] for f in fields]
        ),
    )
    site.register(mod, adm)


def create_model(unit):
    name = unit["name"]

    unit_name = "info_unit_" + name
    multi = unit["multi"]
    fields = unit["fields"]
    model = make_type(unit_name, name, fields)
    if multi:
        make_multi(name, model)

    tagged(model)
    reading(model)

    # register_admin(name, model, fields)


def find_unit(u):
    for _, units in UNITS.items():
        for unit in units:
            if u == unit["name"]:
                return unit
    raise KeyError(u)


def is_multi(u):
    return find_unit(u)["multi"]


class MultiError(Exception):
    pass


def get_unit_model(unit):
    config = apps.get_app_config("angled")
    return config.get_model("info_unit_{}".format(unit))


def get_multi_model(unit):
    if not is_multi(unit):
        raise MultiError(unit)
    config = apps.get_app_config("angled")
    return config.get_model("multi_{}".format(unit))


def get_label_model(name):
    config = apps.get_app_config("angled")
    return config.get_model("{}".format(name))


def get_text_model(name):
    config = apps.get_app_config("angled")
    return config.get_model("{}".format(name))


def get_tag_model(unit):
    config = apps.get_app_config("angled")
    return config.get_model("Tagged_{}".format(unit))


def get_reading_model(unit):
    config = apps.get_app_config("angled")
    return config.get_model("Reading_{}".format(unit))


def unit_uni_label(name):
    model = get_label_model(name)

    def inner(label):
        return model.objects.create_message(
            fr=label,
            nl=label,
            en=label,
        )

    return inner


def unit_all_label(name):
    model = get_label_model(name)

    def inner(dict_lang_label):
        return model.objects.create_message(**dict_lang_label)

    return inner


def unit_uni_text(name):
    model = get_text_model(name)

    def inner(label):
        return model.objects.create_message(
            fr=label,
            nl=label,
            en=label,
        )

    return inner


# This is actually the critical piece of code
for cat, units in UNITS.items():
    for unit in units:
        create_model(unit)
