from datetime import datetime
from django.db import migrations
from angled.ui import UNITS_NAMES
from angled.models.dyn import BaseUnit, Project, is_multi

at_time = datetime.now()


def get_active_units(apps, project, connection):
    """
    get a list of units names which are in use for the current project
    """
    _unique_sql = "SELECT '{unit_name}', COALESCE((SELECT true AS is_present FROM {unit_table} where project_id = {:d} and created_at <= to_timestamp('{date}') LIMIT 1), FALSE)"
    sqls = []
    for unit_name in UNITS_NAMES:
        model = apps.get_model("angled", f"info_unit_{unit_name}")
        sqls.append(
            _unique_sql.format(
                project.id,
                unit_name=unit_name,
                unit_table=model._meta.db_table,
                date=at_time.timestamp(),
            )
        )

    with connection.cursor() as cursor:
        cursor.execute(" UNION ".join(sqls))
        results = [row[0] for row in cursor.fetchall() if row[1]]
    return results


def get_current(multi, unit_name, project):
    """
    Return the current "multi" for the given project.

    Current multi are the ones inserted / updated during the last manipulation
    The parameter `at` (datetime) get the current state at a given time.
    """

    query = """
    WITH ordered AS (SELECT 
        l.id, rank() OVER (PARTITION BY transaction.project_id ORDER BY ts DESC) AS ranking
        FROM angled_listindex AS l
        JOIN angled_transaction AS transaction ON transaction.id = l.transaction_id 
        WHERE transaction.project_id = %(pid)s and l.ts <= to_timestamp({timestamp}) and l.unit LIKE '{unit_name}'
        LIMIT 1)
    SELECT * FROM {multi_table_name} WHERE index_id IN (SELECT id FROM ordered)
    """.format(
        project.id,
        multi_table_name=multi._meta.db_table,
        unit_name=unit_name,
        timestamp=at_time.timestamp(),
    )

    return multi.objects.raw(query, params=dict(pid=project.id))


def process_project(apps, schema_editor, project: Project):
    for unit_name in get_active_units(apps, project, schema_editor.connection):
        if is_multi(unit_name):
            multi_model = apps.get_model("angled", f"multi_{unit_name}")
            multi_query = get_current(multi_model, unit_name, project)
            for multi in multi_query:
                multi.unit.unit_status = BaseUnit.STATUS_CURRENT
                multi.unit.save()
        else:
            unit_model = apps.get_model("angled", f"info_unit_{unit_name}")
            # here we do not *order_by* because the info_unit_* meta class already indicates `ordering=["-created_at"]`
            unit_instance = (
                unit_model.objects.filter(project__id=project.id)
                .filter(created_at__lte=at_time)
                .first()
            )
            if unit_instance:
                unit_instance.unit_status = BaseUnit.STATUS_CURRENT
                unit_instance.save()


def mark_unit_status(apps, schema_editor):

    # first we set everything to 'archive' status
    for unit_name in UNITS_NAMES:
        model = apps.get_model("angled", f"info_unit_{unit_name}")
        for instance in model.objects.all():
            instance.unit_status = BaseUnit.STATUS_ARCHIVED
            instance.save()

    # then we go for the marking of *current* units
    ProjectModel = apps.get_model("angled", "Project")
    for project in ProjectModel.objects.all():
        process_project(apps, schema_editor, project)


def unmark_unit_status(apps, schema_editor):
    for unit_name in UNITS_NAMES:
        model = apps.get_model("angled", f"info_unit_{unit_name}")
        for instance in model.objects.all():
            instance.unit_status = None
            instance.save()


class Migration(migrations.Migration):

    dependencies = [
        ("angled", "0061_auto_20220323_1345"),
    ]

    operations = [migrations.RunPython(mark_unit_status, unmark_unit_status)]
